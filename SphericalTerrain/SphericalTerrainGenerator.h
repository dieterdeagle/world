#ifndef SphericalTerrainGenerator_h
#define SphericalTerrainGenerator_h

#include <vector>

#include "SphericalTerrain.h"
#include "SphericalTerrainTriangle.h"

#include "util/noise/PerlinNoise.h"
#include "util/Vec3.h"

// ----------------------------------------------------------------------------

class SphericalTerrain;

// ----------------------------------------------------------------------------

class MeshGenerator
{
/// TODO
};

// ----------------------------------------------------------------------------

class SphericalTerrainGenerator
{
public:
  SphericalTerrainGenerator();
  ~SphericalTerrainGenerator();

  SphericalTerrain generateSphere( const Vec3& center, float radius, unsigned int nbIterations, int seed );

private:
  std::vector<SphericalTerrainTriangle> initSphereApprox( const Vec3& center, float radius );

  std::vector<SphericalTerrainTriangle> subdivideTriangle( const SphericalTerrainTriangle& triangle, const Vec3& center, float radius );

  std::vector<SphericalTerrainTriangle> subdivideTriangleLongestEdge( const SphericalTerrainTriangle& triangle, const Vec3& center, float radius );

  float offsetRadius( Vec3 normal, float radius );

  PerlinNoise _perlin;
};


// ----------------------------------------------------------------------------

#endif
