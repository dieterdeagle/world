#include "TerrainMeshCreator.h"

#include <iostream>

// ----------------------------------------------------------------------------

std::vector<SphericalTerrainTriangle> TerrainMeshCreator::createMesh( const SphericalTerrain& terrain, unsigned int nbIterations )
{
  std::cout << __FUNCTION__ << std::endl;
  std::vector<SphericalTerrainTriangle> surface = initMesh( terrain );

  for( unsigned int i = 0; i < nbIterations; ++i )
  {
//    std::cout << i << std::endl;
    std::vector<SphericalTerrainTriangle> finerSurface;
    for( auto it = surface.begin(); it != surface.end(); ++it )
    {
      std::vector<SphericalTerrainTriangle> newTriangles = subdivideTriangle( *it, terrain );
      finerSurface.insert( finerSurface.end(), newTriangles.begin(), newTriangles.end() );
    }
    surface = finerSurface;
  }
//std::cout << "jow" << std::endl;
  return surface;
}

// ----------------------------------------------------------------------------

std::vector<SphericalTerrainTriangle> TerrainMeshCreator::initMesh( const SphericalTerrain& terrain )
{
  Vec3 normalTop( 0.0,1.0,0.0 );
  Vec3 normalBottom( 0.0,-1.0,0.0);
  Vec3 normalLeft( -1.0,0.0,0.0 );
  Vec3 normalRight( 1.0,0.0,0.0 );
  Vec3 normalFront( 0.0,0.0,-1.0 );
  Vec3 normalBack( 0.0,0.0,1.0 );

  Vec3 top( terrain.getPosForNormal(normalTop) );
  Vec3 bottom( terrain.getPosForNormal( normalBottom ) );
  Vec3 left( terrain.getPosForNormal( normalLeft ) );
  Vec3 right( terrain.getPosForNormal( normalRight ) );
  Vec3 front( terrain.getPosForNormal( normalFront ) );
  Vec3 back( terrain.getPosForNormal( normalBack ) );

  SphericalTerrainTriangle topFrontLeft( top, front, left );
  SphericalTerrainTriangle topRightFront( top, right, front );
  SphericalTerrainTriangle bottomLeftFront( bottom, left, front );
  SphericalTerrainTriangle bottomFrontRight( bottom, front, right );

  SphericalTerrainTriangle topLeftBack( top, left, back );
  SphericalTerrainTriangle topBackRight( top, back, right );
  SphericalTerrainTriangle bottomLeftBack( bottom, left, back );
  SphericalTerrainTriangle bottomBackRight( bottom, right, back );

  std::vector<SphericalTerrainTriangle> approxSphere{ topFrontLeft, topRightFront, bottomLeftFront,
        bottomFrontRight, topLeftBack, topBackRight, bottomLeftBack, bottomBackRight };

  return approxSphere;
}

// ----------------------------------------------------------------------------

std::vector<SphericalTerrainTriangle> TerrainMeshCreator::subdivideTriangle( const SphericalTerrainTriangle& triangle, const SphericalTerrain& terrain )
{
  Vec3 A = triangle.getA();
  Vec3 B = triangle.getB();
  Vec3 C = triangle.getC();

  Vec3 halfAB = (A+B) / 2.f;
  Vec3 halfAC = (A+C) / 2.f;
  Vec3 halfBC = (B+C) / 2.f;

  std::vector<SphericalTerrainTriangle> newTriangles;

  newTriangles.push_back( SphericalTerrainTriangle( A, halfAB, halfAC) );
  newTriangles.push_back( SphericalTerrainTriangle( halfAB, B, halfBC) );
  newTriangles.push_back( SphericalTerrainTriangle( halfAB, halfBC, halfAC) );
  newTriangles.push_back( SphericalTerrainTriangle( halfBC, C, halfAC) );


  // displacement
  for( SphericalTerrainTriangle& currTriangle : newTriangles )
  {
    std::vector<Vec3> vertices = currTriangle.getVertices();
    for( std::size_t i = 0; i < vertices.size(); ++i )
    {
      Vec3 normal = ( vertices[i]-terrain.getCenter() ).normalized();
//      std::cout << "normal: " << normal << std::endl;
//      double noise = _perlin.noise( posOnSphere.getX(), posOnSphere.getY(), posOnSphere.getZ() );
      Vec3 newPos = terrain.getPosForNormal( normal );
//      std::cout << noise << std::endl;
      vertices[i] = newPos;
    }
    currTriangle = SphericalTerrainTriangle( vertices );
  }

  return newTriangles;
}

// ----------------------------------------------------------------------------
