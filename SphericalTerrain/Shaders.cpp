#include "Shaders.h"

#include <iostream>

osg::ref_ptr<osg::Program> createSphericalTerrainShader()
{
  std::cout << "Create spherical terrain shader.." << std::endl;
  osg::ref_ptr<osg::Program> pgm( new osg::Program );
  pgm->setName( "spherical terrain shader" );


  pgm->addShader( osg::Shader::readShaderFile( osg::Shader::VERTEX, "/home/ntr/code/cpp/World/src/SphericalTerrain/shaders/SphericalTerrain.vert" ) );
  pgm->addShader( osg::Shader::readShaderFile( osg::Shader::GEOMETRY, "/home/ntr/code/cpp/World/src/SphericalTerrain/shaders/SphericalTerrain.geom" ) );
  pgm->addShader( osg::Shader::readShaderFile( osg::Shader::FRAGMENT, "/home/ntr/code/cpp/World/src/SphericalTerrain/shaders/SphericalTerrain.frag" ) );

//  pgm->setParameter( GL_GEOMETRY_VERTICES_OUT_EXT, 2*8*3 );
//  pgm->setParameter( GL_GEOMETRY_INPUT_TYPE_EXT, GL_POINTS );
//  pgm->setParameter( GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLES );

  return pgm;
}
