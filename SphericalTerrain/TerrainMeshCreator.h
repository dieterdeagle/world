#ifndef TerrainMeshCreator_h
#define TerrainMeshCreator_h

#include <memory>

#include "SphericalTerrain.h"
#include "SphericalTerrainTriangle.h"

class TerrainMeshCreator
{
public:
  std::vector<SphericalTerrainTriangle> createMesh( const SphericalTerrain& terrain, unsigned int nbIterations );

private:
  std::vector<SphericalTerrainTriangle> initMesh( const SphericalTerrain& terrain );
  std::vector<SphericalTerrainTriangle> subdivideTriangle( const SphericalTerrainTriangle& triangle, const SphericalTerrain& terrain );
};

#endif
