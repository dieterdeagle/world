#version 330

in vec4 posE;
in vec3 dirFromCenter;
in float offset;
in float distToCenter;

in vec3 testNormal;

uniform vec3 center;
uniform float radius;

out vec4 FragColor;

vec3 l_dir = vec3( 1.0,0.0,1.0 );
float shininess = 2.0;
vec4 diffuse = vec4(0.2,0.2,0.2, 1.0);
vec4 ambient = vec4(0.1,0.1,0.1, 1.0);
vec4 specular = vec4(0.3,0.3,0.3, 1.0);

vec2 uvMapping( vec3 vector )
{
  float x = vector.x;
  float y = vector.y;
  float z = vector.z;

  return vec2( x / sqrt( x*x + y*y + z*z ) + 1.0,
               y / sqrt( x*x + y*y + z*z ) + 1.0 ) / 2.0;

  vec2 longitudeLatitude = vec2( 0.5 + (atan(z, x) / 3.1415926 ),
                                 0.5 - (asin(y) / 3.1415926) );

  return longitudeLatitude;
}

// -------------------

void main()
{
  vec4 spec = vec4(0.0);

  vec3 n = ( testNormal + 1.0 ) / 2.0;
  vec3 e = -posE.xyz;
  
  float intensity = max( dot(n,l_dir), 0.0 );

  if( intensity > 0.0 )
  {
    vec3 h = normalize( l_dir + e );
    float intSpec = max( dot(h,n), 0.0 );
    spec = specular * pow(intSpec,shininess);
  }

  vec4 color = max( intensity * diffuse + spec, ambient );
  color.xyz = pow( color.xyz, vec3(1.0/2.2) );
  FragColor = color;
//  FragColor = vec4( n, 1.0 );
//  FragColor = vec4( 0.0, 1.0, 0.0, 1.0 );
//  FragColor = vec4( vec3(distToCenter / 50.0), 1.0 );
}
