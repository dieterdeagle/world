#version 330

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;

out vec3 posW;
out vec3 v_normalW;

void main()
{
  posW = pos;
  v_normalW = normal;
  gl_Position = vec4( pos, 1.0 );
}
