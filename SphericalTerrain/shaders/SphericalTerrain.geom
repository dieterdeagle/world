#version 330 compatibility

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 posW[];
in vec3 v_normalW[];

uniform vec3 center;
uniform float radius;

out vec4 posE;
out vec3 dirFromCenter;
out float offset;
out float distToCenter;

out vec3 testNormal;


vec2 uvMapping( vec3 vector )
{
  float x = vector.x;
  float y = vector.y;
  float z = vector.z;

  return vec2( x / sqrt( x*x + y*y + z*z ) + 1.0,
               y / sqrt( x*x + y*y + z*z ) + 1.0 ) / 2.0;
}


float func1( float x )
{
  x = 7.9*(x + 2.34);
  return -0.3*sin(x-0.001)*sin(x+0.001)*sin(x)*sin(x)*sin(x)*sin(x)
            + sin(x*x)*sin(x*x)*sin(x*x)*sin(x*x)*sin(x*x)
         -2.7*sin(x)*sin(x)*sin(x)*sin(x)
            + sin(x-0.001)*sin(x+0.001)*sin(x);
}

float func2( float x )
{
  x = 5.37*(x-1.77);
  return 0.4*sin(x)*sin(x)*sin(x)*sin(x)*sin(x*x) - 2.1*(cos(x*x)*sin(x)*sin(x)*sin(x)*sin(x)) - 0.8*sin(x)*sin(x)*sin(x);
}

float func3( float x )
{
  x = 4.2*(x-2.11);
  return (0.2*sin(x)*sin(x*x)*cos(x)*sin(x)*sin(x) - 0.57*sin(x)*cos(x)*cos(x*x*x)*sin(x));
}



float calcOffset( vec3 dir )
{
  float x = 1.0;
  float y = 1.0;
  float z = dir.z*dir.z + 2.1;
  
  return 0.3*func1( dir.x ) + 0.3*func2( dir.y*dir.z ) + 0.3*func3( dir.z );
  return 0.6*(func1(x) / 2.1) + 0.4*(func2(y) / 2.1) + 2*func3(z);
  return 0.4*(-2.0*cos(113*z)*z*y*x*z*z*z*z*z + 0.3*x*x*x*x*x - 2.1*y*y*y*y - 5*z*z*z + 2.0*x*x - x + 2.0 + sin( x*y*11.0 ) );

}

vec3 computeNormal( vec3 A, vec3 B, vec3 C )
{
  vec3 U = B - A;
  vec3 V = C - A;

  vec3 normal = vec3( U.y*V.z - U.z*V.y,
                      U.z*V.x - U.x*V.z,
                      U.x*V.y - U.y*V.x );

  return normalize(normal);
}

vec3 computeOffsettedPosW( vec3 posWorld )
{
  vec3 dirFromCenterTmp = normalize( posWorld - center );
  float offsetTmp = calcOffset( dirFromCenterTmp );
  return center + (radius+offsetTmp)*dirFromCenterTmp;  
}


void main()
{
  vec3 Adir = normalize( posW[0] - center );
  vec3 Bdir = normalize( posW[1] - center );
  vec3 Cdir = normalize( posW[2] - center );

  vec3 A = posW[0];
  vec3 B = posW[1];
  vec3 C = posW[2];

  vec3 normalW;

  normalW = computeNormal(A,B,C);
  dirFromCenter = Adir;
  if( dot( normalW, dirFromCenter ) < 0.1 )
    normalW = -normalW;
  testNormal = gl_NormalMatrix * normalW;
  testNormal = gl_NormalMatrix * v_normalW[0];
  offset = calcOffset( dirFromCenter );
  distToCenter = length( center - A );
  posE = gl_ModelViewMatrix * vec4( A, 1.0 );
  gl_Position = gl_ProjectionMatrix * posE;
  EmitVertex();

  normalW = computeNormal(A,B,C);
  dirFromCenter = Bdir;
  if( dot( normalW, dirFromCenter ) < 0.1 )
    normalW = -normalW;
  testNormal = gl_NormalMatrix * normalW;
  testNormal = gl_NormalMatrix * v_normalW[1];
  offset = calcOffset( dirFromCenter );
  distToCenter = length( center - B );
  posE = gl_ModelViewMatrix * vec4( B, 1.0 );
  gl_Position = gl_ProjectionMatrix * posE;
  EmitVertex();

  normalW = computeNormal(A,B,C);
  dirFromCenter = Cdir;
  if( dot( normalW, dirFromCenter ) < 0.1 )
    normalW = -normalW;
  testNormal = gl_NormalMatrix * normalW;
  testNormal = gl_NormalMatrix * v_normalW[2];
  offset = calcOffset( dirFromCenter );
  distToCenter = length( center - C );
  posE = gl_ModelViewMatrix * vec4( C, 1.0 );
  gl_Position = gl_ProjectionMatrix * posE;
  EmitVertex();

  EndPrimitive();
}
