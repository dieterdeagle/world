#include "SphericalTerrain.h"

#include <iostream>
#include <limits>

#include "util/mathStuff.h"

// ----------------------------------------------------------------------------

SphericalTerrain::SphericalTerrain( const Vec3& center, float radius, float heightScaling,
                                    std::function<float(const Vec3&)> heightFunctor )
  : _center(center), _radius(radius), _heightScaling(heightScaling), _heightFunctor(heightFunctor)
{ 
}

// ----------------------------------------------------------------------------

float SphericalTerrain::getHeightAt( const Vec3& posW ) const
{
  auto normal = (posW - _center).normalized();
  return getHeightForNormal( normal );
}

// ----------------------------------------------------------------------------

float SphericalTerrain::getHeightForNormal( const Vec3& normal ) const
{
  Vec3 fixedNormal = fixDirection( normal );
  float height = _radius + _heightScaling * _radius * _heightFunctor( fixedNormal );

  return height;
}

// ----------------------------------------------------------------------------

Vec3 SphericalTerrain::getPosAt( const Vec3& posW ) const
{
  auto height = getHeightAt( posW );
  auto normal = (posW - _center).normalized();

  return _center + normal * height;
}

// ----------------------------------------------------------------------------

Vec3 SphericalTerrain::getPosForNormal( const Vec3& normal ) const
{
  Vec3 fixedNormal = fixDirection( normal );
  return _center + normal * getHeightForNormal(fixedNormal);
}

// ----------------------------------------------------------------------------
/// FIXME
Vec3 SphericalTerrain::getNormalAt( const Vec3& posW ) const
{
  Vec3 dir = ( posW - _center ).normalized();
//  return Vec3( 1.0,0.0,0.0);
  float eps = 4.f*std::numeric_limits<float>::epsilon();
  eps = 0.1f;

  Vec3 ortho0( 0,0,0 );
  if( dir.dot( Vec3(1,0,0) ) < 0.1 )
    ortho0 = dir.cross( Vec3(1,0,0) );
  else
    ortho0 = dir.cross( Vec3(0,1,0) );
  ortho0 = ortho0.normalized();
  Vec3 ortho1 = dir.cross( ortho0 );
  ortho1 = ortho1.normalized();

  Vec3 A = getPosAt( posW - eps*ortho0 - eps*ortho1 );
  Vec3 B = getPosAt( posW + eps*ortho0 - eps*ortho1 );
  Vec3 C = getPosAt( posW + eps*ortho1 );

  Vec3 U = B - A;
  Vec3 V = C - A;

  Vec3 normal = U.cross(V);

  return normal.normalized();
}

// ----------------------------------------------------------------------------

Vec3 SphericalTerrain::getCenter() const
{
  return _center;
}

// ----------------------------------------------------------------------------

float SphericalTerrain::getRadius() const
{
  return _radius;
}

Vec3 SphericalTerrain::fixDirection( const Vec3& dir ) const
{
  Vec3 result = dir;
  for( int i = 0; i < 3; ++i )
  {
    if( dir[i] > 1.0 )
      result[i] = 1.0 - (dir[i]-1.0);
    else if( dir[i] < -1.0 )
      result[i] = -1.0 + (-1.0-dir[i]);
  }

  return result;
}

// ----------------------------------------------------------------------------

//SphericalTerrain::SphericalTerrain( const PerlinNoise& perlin )
//  : _perlin(perlin)
//{
//}

//// ----------------------------------------------------------------------------

//SphericalTerrain::~SphericalTerrain()
//{

//}

//// ----------------------------------------------------------------------------

//void SphericalTerrain::setSurface( std::vector<SphericalTerrainTriangle> newSurface )
//{
//  _surface = newSurface;
//}

// ----------------------------------------------------------------------------

