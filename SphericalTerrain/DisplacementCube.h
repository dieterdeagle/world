#ifndef DisplacementCube_h
#define DisplacementCube_h

#include <vector>

#include "../util/Randomizer.h"
#include "../util/Vec3.h"

// ----------------------------------------------------------------------------

class DisplacementCube
{
public:
  friend class DisplacementCubeFactory;

  DisplacementCube();
  ~DisplacementCube();

  float getDisplacement( float x, float y, float z );

private:
  unsigned int _width; // is a cube -> width = height = depth

  std::vector< std::vector< std::vector< float > > > _data;
};

// ----------------------------------------------------------------------------

class DisplacementCubeFactory
{
  struct DisplacementNode
  {
    DisplacementNode( Vec3 pos, float displacement )
      : _pos(pos), _displacement(displacement){}

    Vec3 _pos;
    float _displacement;
  };

  DisplacementNode mateNodes( const DisplacementNode& node0, const DisplacementNode& node1, unsigned int currDepth );

public:
  DisplacementCubeFactory();
  ~DisplacementCubeFactory();

  DisplacementCube createDisplacementCube( unsigned int depth );

private:
  Randomizer _randomizer;
};

// ----------------------------------------------------------------------------

#endif
