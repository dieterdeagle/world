#include "SphericalTerrainTriangle.h"

// ----------------------------------------------------------------------------

SphericalTerrainTriangle::SphericalTerrainTriangle( const Vec3& a, const Vec3& b, const Vec3& c )
{
  _vertices = std::vector<Vec3>{ a, b, c };
}

SphericalTerrainTriangle::SphericalTerrainTriangle( const std::vector<Vec3>& points )
  : SphericalTerrainTriangle( points[0], points[1], points[2] )
{

}

// ----------------------------------------------------------------------------

SphericalTerrainTriangle::~SphericalTerrainTriangle()
{

}

// ----------------------------------------------------------------------------
