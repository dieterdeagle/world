#include "DisplacementCube.h"

#include <cmath>
#include <iostream>

// ----------------------------------------------------------------------------

DisplacementCube::DisplacementCube()
{

}

// ----------------------------------------------------------------------------

DisplacementCube::~DisplacementCube()
{

}

// ----------------------------------------------------------------------------

float DisplacementCube::getDisplacement( float x, float y, float z )
{
  // clamp
  x = std::min( 1.f, std::max( x, 0.f ) );
  y = std::min( 1.f, std::max( y, 0.f ) );
  z = std::min( 1.f, std::max( z, 0.f ) );

  /// TODO: http://en.wikipedia.org/wiki/Trilinear_interpolation

  int floorX = std::floor( x * (_width-1) );
  int floorY = std::floor( y * (_width-1) );
  int floorZ = std::floor( z * (_width-1) );

  return _data[floorX][floorY][floorZ];
}

// ----------------------------------------------------------------------------

////////////////////////////////////////
////////////////////////////////////////

// ----------------------------------------------------------------------------

DisplacementCubeFactory::DisplacementNode DisplacementCubeFactory::mateNodes( const DisplacementCubeFactory::DisplacementNode& node0,
                                                                              const DisplacementCubeFactory::DisplacementNode& node1,
                                                                              unsigned int currDepth )
{
  Vec3 meanPos = (node0._pos + node1._pos) / 2.f;
  float meanDisplacement = (node0._displacement + node1._displacement) / 2.f;
  float newDisplacement = meanDisplacement + _randomizer.getRandom() / static_cast<float>(currDepth);

  return DisplacementNode( meanPos, newDisplacement );
}

// ----------------------------------------------------------------------------

DisplacementCubeFactory::DisplacementCubeFactory()
{

}

// ----------------------------------------------------------------------------

DisplacementCubeFactory::~DisplacementCubeFactory()
{

}

// ----------------------------------------------------------------------------

DisplacementCube DisplacementCubeFactory::createDisplacementCube( unsigned int depth )
{
  std::vector<DisplacementNode> initialNodes{
        DisplacementNode( Vec3(0.f,0.f,0.f), _randomizer.getRandom() * 2.f - 1.f ),  DisplacementNode( Vec3(1.f,0.f,0.f), _randomizer.getRandom() * 2.f - 1.f ),
        DisplacementNode( Vec3(0.f,1.f,0.f), _randomizer.getRandom() * 2.f - 1.f ), DisplacementNode( Vec3(0.f,0.f,1.f), _randomizer.getRandom() * 2.f - 1.f ),
        DisplacementNode( Vec3(1.f,1.f,0.f), _randomizer.getRandom() * 2.f - 1.f ), DisplacementNode( Vec3(0.f,1.f,1.f), _randomizer.getRandom() * 2.f - 1.f ),
        DisplacementNode( Vec3(1.f,0.f,1.f), _randomizer.getRandom() * 2.f - 1.f ), DisplacementNode( Vec3(1.f,1.f,1.f), _randomizer.getRandom() * 2.f - 1.f )};


  std::vector<DisplacementNode> nodes = initialNodes;
  for( unsigned int d = 0; d < depth; ++d )
  {
    std::vector<DisplacementNode> newNodes;
    for( int i = 0; i < nodes.size(); ++i )
    {
      for( int j = i+1; j < nodes.size(); ++j )
      {
        newNodes.push_back( this->mateNodes( nodes[i], nodes[j], d+1 ) );
      }
    }
    nodes.insert( nodes.end(), newNodes.begin(), newNodes.end() );
  }

  for( int i = 0; i < nodes.size(); ++i )
  {
    std::cout << "pos: " << nodes[i]._pos << ": " << nodes[i]._displacement << std::endl;
  }
  std::cout << "nb of entries: " << nodes.size() << std::endl;

  return DisplacementCube();

}
