#ifndef SphericalTerrain_h
#define SphericalTerrain_h

#include <functional>

#include "util/noise/PerlinNoise.h"

#include "SphericalTerrainGenerator.h"
#include "SphericalTerrainTriangle.h"


// ----------------------------------------------------------------------------

//class SimpleHeightFunctor
//{
//public:
//  float heightAt( const Vec3& pos );
//};

//// ----------------------------------------------------------------------------

class SphericalTerrain
{
public:
  SphericalTerrain( const Vec3& center, float radius,
                    float heightScaling,
                    std::function<float(const Vec3&)> heightFunctor );

  /**
   * @brief getHeightAt returns height at world pos.
   * @param pos
   * @return
   */
  float getHeightAt( const Vec3& posW ) const;
  float getHeightForNormal( const Vec3& normal ) const;
  Vec3 getPosAt( const Vec3& posW ) const;
  Vec3 getPosForNormal( const Vec3& normal ) const;

  /// FIXME: a bit hacky..
  Vec3 getNormalAt( const Vec3& posW ) const;

  Vec3 getCenter() const;
  float getRadius() const;

  Vec3 fixDirection( const Vec3& dir ) const;

private:
  Vec3 _center;
  float _radius;

  float _heightScaling;

  std::function<float(const Vec3&)> _heightFunctor;

};

// ----------------------------------------------------------------------------


//class SphericalTerrain
//{
//  friend class SphericalTerrainGenerator;
//private:
//  SphericalTerrain( const PerlinNoise& perlin ); // use generator stuff

//public:
//  ~SphericalTerrain();

//  Vec3 getCenter() const { return _center; }
//  float getRadius() const { return _radius; }
//  std::vector<SphericalTerrainTriangle> getSurface() const { return _surface; }

//private:
//  /**
//   * @brief setSurface updates surface by replacing it. Should only be called by friends.
//   * @param newSurface
//   */
//  void setSurface( std::vector<SphericalTerrainTriangle> newSurface );

//private:
//  Vec3 _center = Vec3( 0.f,0.f,0.f );
//  float _radius = 0.f;

//  std::vector<SphericalTerrainTriangle> _surface;

//  PerlinNoise _perlin;
//};

// ----------------------------------------------------------------------------

#endif
