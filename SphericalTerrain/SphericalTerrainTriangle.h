#ifndef SphericalTerrainTriangle_h
#define SphericalTerrainTriangle_h

#include "util/Vec3.h"

#include <vector>

// ----------------------------------------------------------------------------

class SphericalTerrainTriangle
{
public:
  SphericalTerrainTriangle( const Vec3& a, const Vec3& b, const Vec3& c );
  SphericalTerrainTriangle( const std::vector<Vec3>& points );
  ~SphericalTerrainTriangle();

  std::vector<Vec3> getVertices() const { return _vertices; }
  Vec3 getA() const { return _vertices[0]; }
  Vec3 getB() const { return _vertices[1]; }
  Vec3 getC() const { return _vertices[2]; }

private:
  std::vector<Vec3> _vertices;
};

#endif
