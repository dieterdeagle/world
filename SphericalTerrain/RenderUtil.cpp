#include "RenderUtil.h"

#include <iostream>

#include <osg/Geometry>
#include <osg/Vec3>

#include <osgDB/WriteFile>
#include <osgDB/ReadFile>

#include "Shaders.h"
#include "TerrainMeshCreator.h"

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> getSphericalTerrainGeode( const SphericalTerrain& terrain, unsigned int nbIterations )
{
  std::cout << __FUNCTION__ << std::endl;
//  std::vector<SphericalTerrainTriangle> surface = terrain.getSurface();
  TerrainMeshCreator meshCreator;
  auto surface = meshCreator.createMesh( terrain, nbIterations );

  std::cout << "size of surface: " << surface.size() << std::endl;
  osg::ref_ptr<osg::Vec3Array> vertices( new osg::Vec3Array );

  for( const SphericalTerrainTriangle& triangle : surface )
  {
    std::vector<Vec3> currTriangleVertices = triangle.getVertices();
//    for( Vec3 currVec : currTriangleVertices )
//    {
//      std::cout << currVec << std::endl;
//    }
//    std::cout << std::endl;
    vertices->push_back( osg::Vec3( currTriangleVertices[0].getX(), currTriangleVertices[0].getY(), currTriangleVertices[0].getZ() ) );
    vertices->push_back( osg::Vec3( currTriangleVertices[1].getX(), currTriangleVertices[1].getY(), currTriangleVertices[1].getZ() ) );
    vertices->push_back( osg::Vec3( currTriangleVertices[2].getX(), currTriangleVertices[2].getY(), currTriangleVertices[2].getZ() ) );
  }

  std::cout << "size of vertices: " << vertices->size() << std::endl;

  osg::ref_ptr<osg::Geometry> geometry( new osg::Geometry );
  geometry->setVertexArray( vertices );
  geometry->addPrimitiveSet( new osg::DrawArrays( GL_TRIANGLES, 0, vertices->size() ) );


  Vec3 center = terrain.getCenter();
  osg::Vec3 centerOsg = osg::Vec3( center.getX(), center.getY(), center.getZ() );
  osg::Vec3Array* normals = new osg::Vec3Array;
  for( unsigned int i = 0; i < vertices->size(); ++i )
  {
    Vec3 pos( vertices->at(i) );
    Vec3 normal = terrain.getNormalAt( pos );

    normals->push_back( osg::Vec3( normal.getX(), normal.getY(), normal.getZ() ) );
  }
  geometry->setVertexAttribArray( 1, normals, osg::Vec3Array::BIND_PER_VERTEX );
////  geometry->setNormalArray( normals, osg::Vec3Array::BIND_PER_VERTEX );


  osg::ref_ptr<osg::Vec4Array> colors( new osg::Vec4Array );
  for( unsigned int i = 0; i < vertices->size(); ++i )
  {
//    colors->push_back( osg::Vec4( ((vertices->at(i)/vertices->at(i).length()) + osg::Vec3(1.f,1.f,1.f)) / 2.f, 1.f ) );
    colors->push_back( osg::Vec4( 1.f,0.0,0.0,1.0 ) );
  }
  geometry->setColorArray( colors, osg::Vec4Array::BIND_PER_VERTEX );

  osg::ref_ptr<osg::Geode> geode( new osg::Geode );
  geode->addDrawable( geometry );

  geode->getOrCreateStateSet()->setAttribute( createSphericalTerrainShader() );
  geode->getOrCreateStateSet()->addUniform( new osg::Uniform( "center", centerOsg ) );
  geode->getOrCreateStateSet()->addUniform( new osg::Uniform( "radius", terrain.getRadius() ) );

  return geode;
}

// ----------------------------------------------------------------------------
