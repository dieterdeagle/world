#include "SphericalTerrainGenerator.h"

#include <iostream>
#include <limits>
#include <cmath>

#include "../util/FractionalBrownianMotion.h"

// ----------------------------------------------------------------------------

SphericalTerrainGenerator::SphericalTerrainGenerator()
  : _perlin( 0 )
{

}

// ----------------------------------------------------------------------------

SphericalTerrainGenerator::~SphericalTerrainGenerator()
{

}

// ----------------------------------------------------------------------------

SphericalTerrain SphericalTerrainGenerator::generateSphere( const Vec3& center, float radius, unsigned int nbIterations, int seed )
{
  _perlin.setSeed( seed );

  std::vector<SphericalTerrainTriangle> surface = initSphereApprox( center, radius );

  for( unsigned int i = 0; i < nbIterations; ++i )
  {
    std::vector<SphericalTerrainTriangle> finerSurface;
    for( auto it = surface.begin(); it != surface.end(); ++it )
    {
      std::vector<SphericalTerrainTriangle> newTriangles = subdivideTriangle( *it, center, radius );
      finerSurface.insert( finerSurface.end(), newTriangles.begin(), newTriangles.end() );
    }
    surface = finerSurface;
  }

  SphericalTerrain terrain(_perlin);
  terrain.setSurface( surface );
  terrain._center = center;
  terrain._radius = radius;

  return terrain;
}

// ----------------------------------------------------------------------------

std::vector<SphericalTerrainTriangle> SphericalTerrainGenerator::initSphereApprox( const Vec3& center, float radius )
{
  Vec3 normalTop( 0.0,1.0,0.0 );
  Vec3 normalBottom( 0.0,-1.0,0.0);
  Vec3 normalLeft( -1.0,0.0,0.0 );
  Vec3 normalRight( 1.0,0.0,0.0 );
  Vec3 normalFront( 0.0,0.0,-1.0 );
  Vec3 normalBack( 0.0,0.0,1.0 );

  Vec3 top( center + Vec3(0,offsetRadius(normalTop,radius),0) );
  Vec3 bottom( center + Vec3(0,-offsetRadius(normalBottom,radius),0) );
  Vec3 left( center + Vec3(-offsetRadius(normalLeft,radius),0,0) );
  Vec3 right( center + Vec3(offsetRadius(normalRight,radius),0,0) );
  Vec3 front( center + Vec3(0,0,-offsetRadius(normalFront,radius)) );
  Vec3 back( center + Vec3(0,0,offsetRadius(normalBack,radius)) );

  SphericalTerrainTriangle topFrontLeft( top, front, left );
  SphericalTerrainTriangle topRightFront( top, right, front );
  SphericalTerrainTriangle bottomLeftFront( bottom, left, front );
  SphericalTerrainTriangle bottomFrontRight( bottom, front, right );

  SphericalTerrainTriangle topLeftBack( top, left, back );
  SphericalTerrainTriangle topBackRight( top, back, right );
  SphericalTerrainTriangle bottomLeftBack( bottom, left, back );
  SphericalTerrainTriangle bottomBackRight( bottom, right, back );

  std::vector<SphericalTerrainTriangle> approxSphere{ topFrontLeft, topRightFront, bottomLeftFront,
        bottomFrontRight, topLeftBack, topBackRight, bottomLeftBack, bottomBackRight };

  return approxSphere;
}

// ----------------------------------------------------------------------------

std::vector<SphericalTerrainTriangle> SphericalTerrainGenerator::subdivideTriangle( const SphericalTerrainTriangle& triangle, const Vec3& center, float radius )
{
  Vec3 A = triangle.getA();
  Vec3 B = triangle.getB();
  Vec3 C = triangle.getC();

  Vec3 halfAB = (A+B) / 2.f;
  Vec3 halfAC = (A+C) / 2.f;
  Vec3 halfBC = (B+C) / 2.f;

  Vec3 centroid = (A+B+C) / 3.f;

  std::vector<SphericalTerrainTriangle> newTriangles;

  newTriangles.push_back( SphericalTerrainTriangle( A, halfAB, halfAC) );
  newTriangles.push_back( SphericalTerrainTriangle( halfAB, B, halfBC) );
  newTriangles.push_back( SphericalTerrainTriangle( halfAB, halfBC, halfAC) );
  newTriangles.push_back( SphericalTerrainTriangle( halfBC, C, halfAC) );

//  newTriangles.push_back( SphericalTerrainTriangle( A, halfAB, centroid) );
//  newTriangles.push_back( SphericalTerrainTriangle( halfAB, B, centroid) );
//  newTriangles.push_back( SphericalTerrainTriangle( B, halfBC, centroid) );
//  newTriangles.push_back( SphericalTerrainTriangle( halfBC, C, centroid) );
//  newTriangles.push_back( SphericalTerrainTriangle( C, halfAC, centroid) );
//  newTriangles.push_back( SphericalTerrainTriangle( halfAC, A, centroid) );

  // displacement
  for( SphericalTerrainTriangle& currTriangle : newTriangles )
  {
    std::vector<Vec3> vertices = currTriangle.getVertices();
    for( int i = 0; i < vertices.size(); ++i )
    {
      Vec3 normal = (vertices[i]-center).normalized();
//      double noise = _perlin.noise( posOnSphere.getX(), posOnSphere.getY(), posOnSphere.getZ() );
      Vec3 newPos = center + normal * offsetRadius( normal, radius );
//      std::cout << noise << std::endl;
      vertices[i] = newPos;
    }
    currTriangle = SphericalTerrainTriangle( vertices );
  }

  return newTriangles;
}

// ----------------------------------------------------------------------------

std::vector<SphericalTerrainTriangle> SphericalTerrainGenerator::subdivideTriangleLongestEdge( const SphericalTerrainTriangle& triangle, const Vec3& center, float radius )
{
  Vec3 A = triangle.getA();
  Vec3 B = triangle.getB();
  Vec3 C = triangle.getC();

  Vec3 AB = B-A;
  Vec3 AC = C-A;
  Vec3 BC = C-B;

  std::vector<SphericalTerrainTriangle> newTriangles;

  if( AB.length() >= AC.length() && AB.length() >= BC.length() )
  {
    Vec3 halfAB = (A+B) / 2.f;
    newTriangles.push_back( SphericalTerrainTriangle( A, halfAB, C ) );
    newTriangles.push_back( SphericalTerrainTriangle( halfAB, B, C ) );
  }
  else if( BC.length() >= AB.length() && BC.length() >= AC.length() )
  {
    Vec3 halfBC = (B+C) / 2.f;
    newTriangles.push_back( SphericalTerrainTriangle( A, B, halfBC ) );
    newTriangles.push_back( SphericalTerrainTriangle( halfBC, C, A ) );
  }
  else if( AC.length() >= AB.length() && AC.length() >= BC.length() )
  {
    Vec3 halfAC = (A+C) / 2.f;
    newTriangles.push_back( SphericalTerrainTriangle( A, B, halfAC ) );
    newTriangles.push_back( SphericalTerrainTriangle( B, C, halfAC ) );
  }


  // displacement
  for( SphericalTerrainTriangle& currTriangle : newTriangles )
  {
    std::vector<Vec3> vertices = currTriangle.getVertices();
    for( int i = 0; i < vertices.size(); ++i )
    {
      Vec3 normal = (vertices[i]-center).normalized();
      Vec3 newPos = center + normal * radius;
      vertices[i] = newPos;
    }
    currTriangle = SphericalTerrainTriangle( vertices );
  }

  return newTriangles;
}

// ----------------------------------------------------------------------------

float SphericalTerrainGenerator::offsetRadius( Vec3 normal, float radius )
{
  double detail = 4.0;
//  double noise = _perlin.noise( normal * detail );
  double noise = fbm( normal * detail, 8, _perlin );
  return radius + noise*radius/10.0;
}

// ----------------------------------------------------------------------------
