#ifndef RenderUtil_SphericalTerrain
#define RenderUtil_SphericalTerrain

#include <osg/Geode>

#include "SphericalTerrain.h"

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> getSphericalTerrainGeode( const SphericalTerrain& terrain,
                                                   unsigned int nbIterations = 4 );

// ----------------------------------------------------------------------------

#endif
