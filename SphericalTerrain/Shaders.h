#ifndef SphericalTerrain_Shaders_h
#define SphericalTerrain_Shaders_h

#include <osg/Program>

osg::ref_ptr<osg::Program> createSphericalTerrainShader();

#endif
