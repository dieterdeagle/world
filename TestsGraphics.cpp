
#include <iostream>
#include <vector>

#include <osgViewer/Viewer>

#include <osgDB/WriteFile>

#include "graphics/ColorMap.hh"
#include "graphics/Hexagon.hh"
#include "graphics/NoiseTexture.hh"

//#include "util/noise/PerlinNoise.h"
#include "util/noise/FractionalBrownianMotion.h"

int main()
{
  ColorMap cmap;

  cmap.insert( 0.0, Color(100,0,0) );
  cmap.insert( 1.0, Color(0,100,0));
  std::cout << cmap << "\n";

  std::vector<double> testVals{ -0.5, 12.0, 0.5, 0.3, 0.7 };

  for( auto v : testVals )
    std::cout << v << ": \n" << cmap(v) << "\n";

  ColorMap fireCmap;
  fireCmap.insert( 0, Color(0,0,0,255) );
  fireCmap.insert( 0.25, Color(0,0,0,255) );
  fireCmap.insert( 0.45, Color(15,0,0,255) );
  fireCmap.insert( 0.65, Color(150,15,0,255) );
  fireCmap.insert( 0.85, Color(200,60,0,255) );
  fireCmap.insert( 1.0, Color(220,100,0,255) );
  fireCmap.setMin(-1);
  fireCmap.setMax(0.8);

  FbmFunctor<Vec3> fbm( 2, 256, 0, 8, 1 );
//  PerlinNoise perlin;
//  auto noise = std::bind( static_cast<double(PerlinNoise::*)(double,double,double)>(&PerlinNoise::noise), perlin, std::placeholders::_1, std::placeholders::_2,0 );
//  auto noise = [&]( double x, double y ){ return perlin.noise( x, y ); };
  std::size_t NB_IMGS = 5;
  for( std::size_t i = 0; i < NB_IMGS; ++i )
  {
    osg::ref_ptr<NoiseTexture> noiseTex( new NoiseTexture( 128,128, fbm, fireCmap, i ) );
    auto img = noiseTex->getImage();
    osgDB::writeImageFile( *img, "/tmp/fire" + std::to_string(i) + ".bmp" );
  }

  osg::ref_ptr<NoiseTexture> noiseTex( new NoiseTexture( 128,128, fbm, fireCmap, 1 ) );

  auto HEX_RADIUS = 1.0;
  auto HEX_RADIUS_INNEN = std::sqrt( HEX_RADIUS*HEX_RADIUS - (HEX_RADIUS*HEX_RADIUS) / 4.0 );
  auto HEX_RADIUS_TEX = 0.1;
  auto HEX_RADIUS_TEX_INNEN = std::sqrt( HEX_RADIUS_TEX*HEX_RADIUS_TEX - (HEX_RADIUS_TEX*HEX_RADIUS_TEX) / 4.0 );

  osg::ref_ptr<Hexagon> hexa1( new Hexagon( Vec3(0.0,0.0,0.0), HEX_RADIUS, Vec2(HEX_RADIUS_TEX,HEX_RADIUS_TEX), HEX_RADIUS_TEX, noiseTex ) );
  osg::ref_ptr<Hexagon> hexa2( new Hexagon( Vec3(1.5*HEX_RADIUS,HEX_RADIUS_INNEN,0.0), HEX_RADIUS, Vec2(2.5*HEX_RADIUS_TEX,HEX_RADIUS_TEX+HEX_RADIUS_TEX_INNEN), HEX_RADIUS_TEX, noiseTex ) );

  osg::ref_ptr<osg::Geode> geode( new osg::Geode );
  geode->addDrawable( hexa1 );
  geode->addDrawable( hexa2 );

  osgViewer::Viewer viewer;
  viewer.setSceneData( geode );

  viewer.run();

}
