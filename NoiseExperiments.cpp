
#include <osg/Image>
#include <osgDB/WriteFile>

#include "graphics/RenderUtil.h"

#include "PlaneTerrain/PlaneTerrain.h"

#include "util/noise/FractionalBrownianMotion.h"
#include "util/noise/PerlinNoise.h"
#include "util/noise/RidgedNoise.h"
#include "util/noise/SwissTurbulence.h"

// ----------------------------------------------------------------------------

int main( int argc, char* argv[] )
{
  int seed = atoi(argv[1]);
  int sizeI = atoi(argv[2]);

  int repeat = 4;
  PerlinNoise perlin( seed, repeat );

  Vec2 terrainSize( 40000,40000 );
  Vec2 sizePatch( 40000,40000 );
  Vec2 sizeContinent( 4000,4000 );
  Vec2 sizeCountry( 400,400 );
  Vec2 sizeRegion( 40,40 );
  Vec2 sizeParc( 4,4 );
  Vec2 sizeSiedlung( 0.4,0.4 );
  Vec2 sizeGarden( 0.04,0.04 );
  Vec2 sizeDirt( 0.004,0.004 );

  std::cout << "seed: " << seed << "\n";
  PlaneTerrain terrain( Vec2(0.0,0.0), terrainSize, 4.0, seed );

  unsigned int NB_SAMPLES = 256;
  std::vector< std::vector<double> > heightsTerrainWorld( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  std::vector< std::vector<double> > heightsTerrainContinent( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  std::vector< std::vector<double> > heightsTerrainCountry( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  std::vector< std::vector<double> > heightsTerrainRegion( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  std::vector< std::vector<double> > heightsTerrainParc( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  std::vector< std::vector<double> > heightsTerrainSiedlung( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  std::vector< std::vector<double> > heightsTerrainGarden( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  std::vector< std::vector<double> > heightsTerrainDirt( NB_SAMPLES, std::vector<double>(NB_SAMPLES,0.0) );
  for( unsigned int i = 0; i < NB_SAMPLES; ++i )
  {
    for( unsigned int j = 0; j < NB_SAMPLES; ++j )
    {
      auto currPosWorld = sizePatch * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                            static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainWorld[i][j] = terrain.getHeightAt( currPosWorld );

      auto currPosContinent = sizeContinent * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                                    static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainContinent[i][j] = terrain.getHeightAt( currPosContinent );

      auto currPosCountry = sizeCountry * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                                static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainCountry[i][j] = terrain.getHeightAt( currPosCountry );

      auto currPosRegion = sizeRegion * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                              static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainRegion[i][j] = terrain.getHeightAt( currPosRegion );

      auto currPosParc = sizeParc * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                          static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainParc[i][j] = terrain.getHeightAt( currPosParc );

      auto currPosSiedlung = sizeSiedlung * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                                  static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainSiedlung[i][j] = terrain.getHeightAt( currPosSiedlung );

      auto currPosGarden = sizeGarden * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                              static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainGarden[i][j] = terrain.getHeightAt( currPosGarden );

      auto currPosDirt = sizeDirt * Vec2( static_cast<double>(i) / (NB_SAMPLES-1),
                                          static_cast<double>(j) / (NB_SAMPLES-1) );
      heightsTerrainDirt[i][j] = terrain.getHeightAt( currPosDirt );
    }
  }

  auto imgWorld = osgImageFromVectorVector( heightsTerrainWorld );
  osgDB::writeImageFile( *imgWorld, "/tmp/testWorld.png" );

  auto imgTerrainContinent = osgImageFromVectorVector( heightsTerrainContinent );
  osgDB::writeImageFile( *imgTerrainContinent, "/tmp/testContinent.png" );

  auto imgTerrainCountry = osgImageFromVectorVector( heightsTerrainCountry );
  osgDB::writeImageFile( *imgTerrainCountry, "/tmp/testCountry.png" );

  auto imgTerrainRegion = osgImageFromVectorVector( heightsTerrainRegion );
  osgDB::writeImageFile( *imgTerrainRegion, "/tmp/testRegion.png" );

  auto imgTerrainParc = osgImageFromVectorVector( heightsTerrainParc );
  osgDB::writeImageFile( *imgTerrainParc, "/tmp/testParc.png" );

  auto imgTerrainSiedlung = osgImageFromVectorVector( heightsTerrainSiedlung );
  osgDB::writeImageFile( *imgTerrainSiedlung, "/tmp/testSiedlung.png" );

  auto imgTerrainGarden = osgImageFromVectorVector( heightsTerrainGarden );
  osgDB::writeImageFile( *imgTerrainGarden, "/tmp/testGarden.png" );

  auto imgTerrainDirt = osgImageFromVectorVector( heightsTerrainDirt );
  osgDB::writeImageFile( *imgTerrainDirt, "/tmp/testDirt.png" );

  return 0;

  // ------------------

  int oct = 16;
  RidgedNoiseFunctor<Vec2> ridgedNoise( perlin, oct );
  FbmFunctor<Vec2> fbmNoise( seed, sizeI, repeat, oct, 1/2.0, 0.5, 2.0 );
  SwissTurbulenceFunctor<Vec2> swiss( perlin, oct );


  Vec2 size( sizeI, sizeI );
  int samplesPerUnit = 32;
  int samplesX = size.getX() * samplesPerUnit;
  int samplesY = size.getY() * samplesPerUnit;
  std::vector< std::vector<double> > heightsPerlin( samplesX, std::vector<double>(samplesY,0.0) );
  std::vector< std::vector<double> > heightsRidged( samplesX, std::vector<double>(samplesY,0.0) );
  std::vector< std::vector<double> > heightsFbm( samplesX, std::vector<double>(samplesY,0.0) );
  std::vector< std::vector<double> > heightsSwiss( samplesX, std::vector<double>(samplesY,0.0) );
  for( int i = 0; i < samplesX; ++i )
  {
    for( int j = 0; j < samplesY; ++j )
    {
      Vec2 currVec( size.getX() * i / (samplesX-1),
                    size.getY() * j / (samplesY-1) );
//      std::cout << "curr vec: " << currVec << "\n";
      heightsPerlin[i][j] = perlin( currVec );
//      heightsRidged[i][j] = ridgedNoise( currVec );
      heightsFbm[i][j] = fbmNoise( currVec );
//      heightsSwiss[i][j] = swiss( currVec );
    }
  }
  auto imgPerlin = osgImageFromVectorVector( heightsPerlin );
  osgDB::writeImageFile( *imgPerlin, "/tmp/perlinTest.png" );
  auto imgRidged = osgImageFromVectorVector( heightsRidged );
  osgDB::writeImageFile( *imgRidged, "/tmp/ridgedTest.png" );
  auto imgFbm = osgImageFromVectorVector( heightsFbm );
  osgDB::writeImageFile( *imgFbm, "/tmp/fbmTest.png" );
  auto imgSwiss= osgImageFromVectorVector( heightsSwiss );
  osgDB::writeImageFile( *imgSwiss, "/tmp/swissTest.png" );
}
