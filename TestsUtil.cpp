
#include <chrono>
#include <iostream>
#include <random>

#include <osg/Image>
#include <osgDB/WriteFile>

#include "util/Array2d.hh"
#include "util/Convolution.hh"
#include "util/ReactionDiffusion.hh"
#include "util/Vec2.h"

// -----------------------

struct rgb
{
  unsigned char r = 0;
  unsigned char g = 0;
  unsigned char b = 0;
};

// --------------------------------

osg::ref_ptr<osg::Image> toImg( const Array2d<>& arr )
{
  osg::ref_ptr<osg::Image> img( new osg::Image );
  img->allocateImage( arr.width(), arr.height(), 1, GL_RGB, GL_UNSIGNED_BYTE );

  for( std::size_t j = 0; j < arr.height(); ++j )
  {
    for( std::size_t i = 0; i < arr.width(); ++i )
    {
      rgb col;
      col.r = arr(i,j)*255;
      col.g = arr(i,j)*255;
      col.b = arr(i,j)*255;
      *reinterpret_cast<rgb*>( img->data(i,j) ) = col;
    }
  }

  return img;
}

// --------------------------------

osg::ref_ptr<osg::Image> toImg( const Array2d<>& arrA, const Array2d<>& arrB )
{
  osg::ref_ptr<osg::Image> img( new osg::Image );
  img->allocateImage( arrA.width(), arrA.height(), 1, GL_RGB, GL_UNSIGNED_BYTE );

  for( std::size_t j = 0; j < arrA.height(); ++j )
  {
    for( std::size_t i = 0; i < arrA.width(); ++i )
    {
      rgb col;
//      col.r = std::max(0.0,std::min(255.0,arrA(i,j)*255));
//      col.g = std::max(0.0,std::min(255.0,arrA(i,j)*255));
//      col.b = std::max(0.0,std::min(255.0,arrA(i,j)*255));
      col.r = std::max(0.0,std::min(255.0,arrA(i,j)*255));
      col.g = 0;
      col.b = std::max(0.0,std::min(255.0,arrB(i,j)*255));
      *reinterpret_cast<rgb*>( img->data(i,j) ) = col;
    }
  }

  return img;
}

// --------------------------

int main()
{

  std::mt19937 twister;
  twister.seed( std::time(0) );
  std::uniform_real_distribution<double> distr( 0.0, 0.08 );

  std::size_t SIZE = 200;

  std::vector<double> dataA( SIZE*SIZE, 1.0 );
  Array2d<> arrA( SIZE,SIZE, dataA );

  Vec2 INIT_POS1( SIZE/4, SIZE/4 );
  double INIT_RADIUS = 15;
  std::vector<double> dataB( SIZE*SIZE, 0.0 );
//  for( int i = 0; i < SIZE; ++i )
//  {
//    for( int j = 0; j < SIZE; ++j )
//    {
//      if( INIT_POS1.distanceEuclidean( Vec2(i,j) ) < INIT_RADIUS )
//        dataB[ i + SIZE*j ] = 1.0;
////      dataB[ i + SIZE*j ] = 1.0;
//    }
//  }
  Vec2 INIT_POS2( 3*SIZE/4, 3*SIZE/4 );
  for( int i = -INIT_RADIUS; i < INIT_RADIUS; ++i )
  {
    for( int j = -INIT_RADIUS; j < INIT_RADIUS; ++j )
    {
      dataB[ (INIT_POS2.x() + i) + SIZE*(INIT_POS2.y() + j) ] = 1.0;
    }
  }

  Array2d<> arrB( SIZE,SIZE, dataB );

  Densities densities( arrA, arrB );
  auto imgDensity = toImg( densities._a, densities._b );
  osgDB::writeImageFile( *imgDensity, "/tmp/imgResult0.bmp" );

//  ReactionDiffusion reactionDiff( 1.0, 0.5, 0.055, 0.062 );
//  ReactionDiffusion reactionDiff( 1.0, 0.5, 0.0367, 0.0649 );
  ReactionDiffusion reactionDiff( 1.0, 0.5, 0.035, 0.064 );

//  auto randFeed = distr(twister);
//  auto randKill = distr(twister);
//  ReactionDiffusion reactionDiff( 1.0, 0.5, randFeed, randKill );
//  std::cout << "rand feed: " << randFeed << ", rand kill: " << randKill << "\n";

  auto t1 = std::chrono::high_resolution_clock::now();

  std::size_t NB_STEPS = 20000;
  for( int t = 1; t <= NB_STEPS; ++t )
  {
    densities = reactionDiff.calcNextStep( densities, 1.0 );
    if( t % 50 == 0 )
    {
      auto tempImg = toImg( densities._a, densities._b );
      osgDB::writeImageFile( *tempImg, "/tmp/imgResult" + std::to_string(t) + ".bmp" );
    }
  }

  auto imgResult = toImg( densities._a, densities._b );
  osgDB::writeImageFile( *imgResult, "/tmp/imgResult.bmp" );

  auto t2 = std::chrono::high_resolution_clock::now();
  std::cout << "reaction diffusion took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
            << " milliseconds\n";

  return 0;
}
