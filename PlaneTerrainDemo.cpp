
#include <iostream>
#include <memory>

#include <osg/Image>
#include <osgDB/WriteFile>
#include <osgGA/TrackballManipulator>
#include <osgViewer/Viewer>

#include "Game/InteractionHandler.h"
#include "Game/PatchManager.h"
#include "Game/Player.h"
#include "Game/PlayerNode.h"
#include "Game/PlayerUpdateCallback.h"
#include "Game/Physics.h"
#include "Game/World.h"
#include "Game/WorldMap.h"

#include "graphics/CameraManipulatorEgo.h"
#include "graphics/OsgGroupOfPatchGroups.h"
#include "graphics/OsgPatchGroup.h"
#include "graphics/RenderUtil.h"
#include "graphics/UpdateCallbackPatchesGroup.h"

#include "PlaneTerrain/PlaneTerrain.h"
#include "PlaneTerrain/PlaneTerrainMeshCreator.h"
//#include "PlaneTerrain/RenderUtil.h"

#include "TreeGen/Vegetation.h"
//#include "TreeGen/RenderUtil.h"

#include "util/noise/FractionalBrownianMotion.h"
#include "util/noise/PerlinNoise.h"

// ---------------------------------------

int main( int argc, char* argv[] )
{
  std::cout << "Start\n";
  double height = 4.0; // 4 km
  unsigned int nbSamples = 1000;

  std::cout << "argc: " << argc << "\n";
  int seed = 1;
  if( argc > 1 )
  {
    seed = atoi(argv[1]);
  }
  std::uniform_int_distribution<int> dist;
  std::mt19937 randomEngine( seed );
  int seedTerrain =  dist( randomEngine );
  int seedVeg =  dist( randomEngine );

  std::cout << "seed terrain: " << seedTerrain << ", seedVeg: " << seedVeg << "\n";

  Vec2 worldSize( 40000.0,40000.0 );
  /// World
  auto world = std::make_shared<World>( seedTerrain, seedVeg, worldSize, height );

  auto terrain = world->getTerrain();

//  Vec2 minPos( 0.9,0.9 );
//  Vec2 maxPos( 1.0, 1.0 );
  Vec2 startVec2( 100.1,100.1 );
  Vec3 start( startVec2, terrain->getHeightAt( startVec2 ) );
//  PlaneTerrainMeshCreator meshCreator( nbSamples );


  PlaneTerrainMeshCreator meshCreator( nbSamples );
  auto testMesh = meshCreator.createPatchMesh( *world->getTerrain(), startVec2, startVec2+Vec2(0.05,0.05) );
  auto testPatch = std::make_shared<Patch>( 0, testMesh, std::vector< std::shared_ptr<Tree> >{} );
  auto osgPatchGroup = createPatchGroup( testPatch );


  Vec2 patchSize( 0.025,0.025 );
//  auto patchMgr = std::make_shared<PatchManager>( world, patchSize, start.getXY(), nbSamples );

//  osg::ref_ptr<OsgGroupOfPatchGroups> groupOfPatches( new OsgGroupOfPatchGroups );

//  osg::ref_ptr<UpdateCallbackPatchesGroup> updateCallbackPatches( new UpdateCallbackPatchesGroup(patchMgr) );
//  groupOfPatches->addUpdateCallback( updateCallbackPatches );


  //-----------------
  std::cout << "Start render stuff\n";

  osg::ref_ptr<osg::Group> root( new osg::Group );

//  root->addChild( groupOfPatches );
  std::cout << "end render stuff\n";
  //-----------------


  // test
  root->addChild( osgPatchGroup );

  auto player = std::make_shared<Player>( start,
                                          0.0018, // 1.8m
                                          Vec3( 0,0,0 ) /* 0 km/h in each dir */ );

  osg::ref_ptr<PlayerNode> playerNode( new PlayerNode(player) );

  playerNode->addUpdateCallback( new PlayerUpdateCallback(player) );
  root->addChild( playerNode );

  auto physics = std::make_shared<Physics>( terrain, player );

  osgViewer::Viewer viewer;

  viewer.setSceneData( root );
  viewer.realize();
  viewer.addEventHandler( new InteractionHandler(physics) );
  osg::ref_ptr<osgGA::TrackballManipulator> manip( new osgGA::TrackballManipulator );
//  viewer.setCameraManipulator( manip );

  osg::ref_ptr<CameraManipulatorEgo> manipEgo( new CameraManipulatorEgo( player ) );
  viewer.setCameraManipulator( manipEgo );

//  PatchManager patchMgr( world, patchSize, nbSamples );

  WorldMap map( world, 256,256 );
  auto osgImg = osgImageFromVectorVector( map.getMap() );
  osgDB::writeImageFile( *osgImg, "/tmp/worldMap.png" );

  while( !viewer.done() )
  {
    physics->update();
//    patchMgr->update( player->getPos().getXY() );
    viewer.frame();
  }



  return 0;
}
