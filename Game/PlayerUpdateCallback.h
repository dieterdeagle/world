#ifndef Game_PlayerUpdateCallback_h
#define Game_PlayerUpdateCallback_h

#include <memory>
#include <vector>

#include <osg/Geode>
#include <osg/NodeCallback>

#include "Player.h"

// ----------------------------------------------------------------------------

class PlayerUpdateCallback : public osg::NodeCallback
{
public:
  PlayerUpdateCallback( const std::shared_ptr<Player>& player );

  virtual void operator()( osg::Node* node, osg::NodeVisitor* nv );

private:
  std::shared_ptr<Player> _player;
};


#endif
