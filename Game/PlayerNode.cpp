#include "PlayerNode.h"

#include <iostream>

// ----------------------------------------------------------------------------

PlayerNode::PlayerNode( const std::shared_ptr<Player>& player )
  : osg::Geode::Geode(), _player(player)
{

  _pos = osg::Vec3( _player->getPos().getX(), _player->getPos().getY(), _player->getPos().getZ() );
  _size = _player->getSize();

  osg::ref_ptr<osg::Vec3Array> vertices( new osg::Vec3Array );

  vertices->push_back( osg::Vec3( _pos[0]-_size/4, _pos[1]-_size/4, _pos[2] ) );
  vertices->push_back( osg::Vec3( _pos[0]-_size/4, _pos[1]+_size/4, _pos[2] ) );
  vertices->push_back( osg::Vec3( _pos[0]+_size/4, _pos[1]+_size/4, _pos[2] ) );
  vertices->push_back( osg::Vec3( _pos[0]+_size/4, _pos[1]-_size/4, _pos[2] ) );

  vertices->push_back( osg::Vec3( _pos[0]-_size/4, _pos[1]-_size/4, _pos[2]+_size ) );
  vertices->push_back( osg::Vec3( _pos[0]-_size/4, _pos[1]+_size/4, _pos[2]+_size ) );
  vertices->push_back( osg::Vec3( _pos[0]+_size/4, _pos[1]+_size/4, _pos[2]+_size ) );
  vertices->push_back( osg::Vec3( _pos[0]+_size/4, _pos[1]-_size/4, _pos[2]+_size ) );

  _geometry = new osg::Geometry;
  _geometry->setVertexArray( vertices );
//  _geometry->setNormalArray( normalsOsg );
//  _geometry->setNormalBinding( osg::Geometry::BIND_PER_VERTEX );

  osg::ref_ptr<osg::DrawElementsUInt> trianglesIds =
        new osg::DrawElementsUInt( osg::PrimitiveSet::TRIANGLES, 0);

  // bottom
  trianglesIds->push_back( 0 );
  trianglesIds->push_back( 1 );
  trianglesIds->push_back( 2 );
  trianglesIds->push_back( 0 );
  trianglesIds->push_back( 2 );
  trianglesIds->push_back( 3 );

  // top
  trianglesIds->push_back( 4 );
  trianglesIds->push_back( 5 );
  trianglesIds->push_back( 6 );
  trianglesIds->push_back( 4 );
  trianglesIds->push_back( 6 );
  trianglesIds->push_back( 7 );

  // side 1
  trianglesIds->push_back( 0 );
  trianglesIds->push_back( 1 );
  trianglesIds->push_back( 4 );
  trianglesIds->push_back( 1 );
  trianglesIds->push_back( 5 );
  trianglesIds->push_back( 4 );

  // side 2
  trianglesIds->push_back( 1 );
  trianglesIds->push_back( 2 );
  trianglesIds->push_back( 5 );
  trianglesIds->push_back( 2 );
  trianglesIds->push_back( 6 );
  trianglesIds->push_back( 5 );

  // side 3
  trianglesIds->push_back( 2 );
  trianglesIds->push_back( 3 );
  trianglesIds->push_back( 6 );
  trianglesIds->push_back( 3 );
  trianglesIds->push_back( 7 );
  trianglesIds->push_back( 6 );

  // side 4
  trianglesIds->push_back( 3 );
  trianglesIds->push_back( 0 );
  trianglesIds->push_back( 7 );
  trianglesIds->push_back( 0 );
  trianglesIds->push_back( 4 );
  trianglesIds->push_back( 7 );

  _geometry->addPrimitiveSet( trianglesIds );

  osg::ref_ptr<osg::Vec4Array> colors( new osg::Vec4Array );
  colors->push_back( osg::Vec4(0.8,0.1,0.1,1.0) );
  _geometry->setColorArray( colors );
  _geometry->setColorBinding( osg::Geometry::BIND_OVERALL );

  _geometry->setDataVariance( osg::Geometry::DataVariance::DYNAMIC );
  this->addDrawable( _geometry );
}

// ----------------------------------------------------------------------------

void PlayerNode::setPos( const osg::Vec3& pos )
{
  auto oldPos = _pos;
  auto diff = pos - oldPos;

  auto vertices = static_cast<osg::Vec3Array*>( _geometry->getVertexArray() );
  osg::ref_ptr<osg::Vec3Array> newVertices( new osg::Vec3Array );

  for( unsigned int i = 0; i < vertices->size(); ++i )
  {
    newVertices->push_back( vertices->at(i) + diff );
//    vertices->at(i) += diff;
  }
  _geometry->setVertexArray( newVertices );

  _pos = pos;

//  _geometry->dirtyDisplayList();
//  this->dirtyBound();

}

// ----------------------------------------------------------------------------
