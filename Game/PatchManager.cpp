#include "PatchManager.h"

#include <algorithm>
#include <chrono>
#include <iostream>

// ----------------------------------------------------------------------------

PatchManager::PatchManager( const std::shared_ptr<World>& world, const Vec2& patchSize,
                            Vec2 startingPos,
                            unsigned int meshSamplesPerUnit )
  : _world(world), _patchSize(patchSize), _meshCreator( meshSamplesPerUnit )
{
  _worldSize = _world->getSize();



  // we assume that world size is a multiple of patch size
  _nbPatchesX = std::ceil( _worldSize.getX() / _patchSize.getX() );
  _nbPatchesY = std::ceil( _worldSize.getY() / _patchSize.getY() );

  auto startingPatchId = this->getPatchId( startingPos );
  auto patch = this->createPatch( startingPatchId );

  this->addNewPatch( patch );
  _lastPosPatchId = startingPatchId;
  update( startingPos );
}

// ----------------------------------------------------------------------------

void PatchManager::update( const Vec2& currPos )
{
  auto currPatchId = this->getPatchId( currPos );

  // find patches that can be removed
  double MAX_DIST_TO_PATCHES = 0.1;
  for( auto p : _patches )
  {
    if( currPos.distanceEuclidean( (p.second->getMinPos() + p.second->getMaxPos()) / 2.0 ) > MAX_DIST_TO_PATCHES )
    {
      _idsOfPatchesToRemove.push_back( p.first );
      _hasNewIdsToRemove = true;
    }
  }
  // remove patches from _patches
  // sort descending
//  std::sort( _idsOfPatchesToRemove.begin(), _idsOfPatchesToRemove.end(), std::greater<unsigned int>() );
  for( auto i : _idsOfPatchesToRemove )
  {
    auto findIter = _patches.find( i );
    if( findIter != _patches.end() )
      _patches.erase( findIter );
  }

  if( (currPatchId != _lastPosPatchId) && ( _patches.find(currPatchId) == _patches.end() ) )
  {
    // do something
    auto ids = this->getNeighbouringIds( currPos );
    ids.push_front( currPatchId );

    for( auto i : ids )
    {
      if( _patches.find(i) == _patches.end() )
      {
        _futures.push_back( std::async( std::launch::async, &PatchManager::createPatch, this, i ) );
      }
    }
    _lastPosPatchId = currPatchId;
  }
  else
  {
    /// TODO?: check if position is near any border
  }

  std::deque<unsigned int> futureIdsToRemove;
  for( unsigned int id = 0; id < _futures.size(); ++id )
  {
//    std::cout << "fut " << id << std::endl;
    std::chrono::milliseconds span(0); // FIXME: span flexibel?
    if( _futures[id].wait_for(span)==std::future_status::timeout )
    {
    }
    else
    {
      auto patch = _futures[id].get();
      this->addNewPatch( patch );
      futureIdsToRemove.push_front( id );
    }
  }
  for( auto currId : futureIdsToRemove )
  {
    _futures.erase( _futures.begin() + currId );
  }
//  std::cout << "future size: " << _futures.size() << "\n";
}

// ----------------------------------------------------------------------------

std::shared_ptr<Patch> PatchManager::createPatch( unsigned int id )
{
  auto bounds = this->getBoundingOfPatch( id );
  auto patchMesh = _meshCreator.createPatchMesh( *_world->getTerrain(), bounds.first, bounds.second );
  auto patchPlants = _world->getVegetation()->createVegetationForPatch( bounds.first, bounds.second );

  return std::make_shared<Patch>( id, patchMesh, patchPlants );
}

// ----------------------------------------------------------------------------

unsigned int PatchManager::getPatchId( const Vec2& pos )
{
  auto posX = pos.getX();
  auto posY = pos.getY();

  // Repeat correction
  if( posX / _worldSize.getX() > 1.0 )
    posX -= std::floor( posX/_worldSize.getX() ) * _worldSize.getX();
  if( posY / _worldSize.getY() > 1.0 )
    posY -= std::floor( posY/_worldSize.getY() ) * _worldSize.getY();
  if( posX < 0.0 )
    posX += (std::floor( std::abs(posX/_worldSize.getX()) )+1) * _worldSize.getX();
  if( posY < 0.0 )
    posY += (std::floor( std::abs(posY/_worldSize.getY()) )+1) * _worldSize.getY();

  unsigned int idX = std::floor( posX / _patchSize.getX() );
  unsigned int idY = std::floor( posY / _patchSize.getY() );

  return idX + idY * _nbPatchesX;
}

// ----------------------------------------------------------------------------

std::deque<unsigned int> PatchManager::getNeighbouringIds( const Vec2& pos )
{
  int NB_PER_DIR = 2;

  std::deque<unsigned int> ids;

  for( int i = -NB_PER_DIR; i <= NB_PER_DIR; ++i )
  {
    for( int j = -NB_PER_DIR; j <= NB_PER_DIR; ++j )
    {
      if( i == 0 && j == 0 )
        continue;
      else
        ids.push_back( this->getPatchId( pos + Vec2(i*_patchSize.getX(),j*_patchSize.getY()) ) );
    }
  }


//  std::deque<unsigned int> ids{ this->getPatchId( pos + Vec2(-_patchSize.getX(),0) ),
//                                 this->getPatchId( pos + Vec2(-_patchSize.getX(),_patchSize.getY()) ),
//                                 this->getPatchId( pos + Vec2(0,_patchSize.getY()) ),
//                                 this->getPatchId( pos + Vec2(_patchSize.getX(),_patchSize.getY()) ),
//                                 this->getPatchId( pos + Vec2(_patchSize.getX(),0) ),
//                                 this->getPatchId( pos + Vec2(_patchSize.getX(),-_patchSize.getY()) ),
//                                 this->getPatchId( pos + Vec2(0,-_patchSize.getY()) ),
//                                 this->getPatchId( pos + Vec2(-_patchSize.getX(),-_patchSize.getY()) )
//                               };

  return ids;
}

// ----------------------------------------------------------------------------

std::pair<Vec2, Vec2> PatchManager::getBoundingOfPatch( unsigned int id )
{
  unsigned int idX = id % _nbPatchesX;
  unsigned int idY = std::floor( id / _nbPatchesX );
  Vec2 minPos( 0 + _patchSize.getX() * static_cast<double>(idX),
               0 + _patchSize.getY() * static_cast<double>(idY) );
  Vec2 maxPos( 0 + _patchSize.getX() * static_cast<double>(idX+1),
               0 + _patchSize.getY() * static_cast<double>(idY+1));

  return std::make_pair( minPos,maxPos );
}

// ----------------------------------------------------------------------------

std::vector< std::shared_ptr<Patch> > PatchManager::popNewPatches()
{
  std::vector< std::shared_ptr<Patch> > patches;
  patches.reserve( _idsOfNewPatches.size() );
  for( auto id : _idsOfNewPatches )
  {
    auto currPatchIter = _patches.find( id );
    if( currPatchIter != _patches.end() )
    {
      patches.push_back( currPatchIter->second );
    }
  }

  _hasNewPatches = false;
  _idsOfNewPatches.clear();

  return patches;
}

// ----------------------------------------------------------------------------

std::vector<unsigned int> PatchManager::popIdsToRemove()
{
  auto ids = _idsOfPatchesToRemove;
  _idsOfPatchesToRemove.clear();
  _hasNewIdsToRemove = false;

  return ids;
}

// ----------------------------------------------------------------------------

void PatchManager::addNewPatch( const std::shared_ptr<Patch>& patch )
{
  auto id = patch->getId();
  _patches.insert( std::make_pair(id,patch) );
  _idsOfNewPatches.push_back( id );
  _hasNewPatches = true;
}

// ----------------------------------------------------------------------------
