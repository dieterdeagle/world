#include "WorldMap.h"


WorldMap::WorldMap( const std::shared_ptr<World>& world,
                    unsigned int width,
                    unsigned int height )
  : _world(world), _width(width), _height(height)
{
  auto worldSize = _world->getSize();

  _map = std::vector< std::vector<double> >( _width, std::vector<double>(height,0.0) );

  double stepsizeX = worldSize.getX() / static_cast<double>(width); // static_casts just to be sure..
  double stepsizeY = worldSize.getY() / static_cast<double>(height);

  for( unsigned int i = 0; i < _width; ++i )
  {
    for( unsigned int j = 0; j < _height; ++j )
    {
      _map[i][j] = _world->getTerrain()->getHeightAt( Vec2( 0.0 + i*stepsizeX,
                                                            0.0 + j*stepsizeY ) );
    }
  }
}
