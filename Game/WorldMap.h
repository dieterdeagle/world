#ifndef PlaneTerrain_WorldMap_h
#define PlaneTerrain_WorldMap_h

#include <memory>
#include <vector>

#include "Game/World.h"

class WorldMap
{
public:
  WorldMap( const std::shared_ptr<World>& world,
            unsigned int width,
            unsigned int height );

  std::vector< std::vector<double> > getMap() const { return _map; }

private:
  std::shared_ptr<World> _world;
  unsigned int _width;
  unsigned int _height;

  std::vector< std::vector<double> > _map;
};

#endif
