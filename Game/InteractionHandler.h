#ifndef Game_InteractionHandler_h
#define Game_InteractionHandler_h

#include <memory>

#include <osgGA/GUIEventHandler>

#include "Physics.h"

class InteractionHandler : public osgGA::GUIEventHandler
{
public:
  InteractionHandler( const std::shared_ptr<Physics>& physics );

  virtual bool handle( const osgGA::GUIEventAdapter& ea,
                       osgGA::GUIActionAdapter& aa,
                       osg::Object*,
                       osg::NodeVisitor* );

private:
  std::shared_ptr<Physics> _physics;

  float _lastMouseX = -2.0;
  float _lastMouseY = -2.0;

  bool _requestedWarpPoint = false;
};

#endif
