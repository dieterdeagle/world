#ifndef Game_World_h
#define Game_World_h

#include "PlaneTerrain/PlaneTerrain.h"
#include "TreeGen/Vegetation.h"

#include "util/Vec2.h"

class World
{
public:
  World( int seedTerrain, int seedVegetation, // TODO: replace by one seed
         const Vec2& size,
         double heightScaling/*,
         unsigned int nbSamplesPerUnit*/ );

  std::shared_ptr<PlaneTerrain> getTerrain() const { return _terrain; }
  std::shared_ptr<Vegetation> getVegetation() const { return _vegetation; }

  Vec2 getSize() const { return _size; }

  unsigned int getNbSamplesPerUnit() const { return _nbSamplesPerUnit; }

private:
  std::shared_ptr<PlaneTerrain> _terrain;
  std::shared_ptr<Vegetation> _vegetation;

  Vec2 _size;

  unsigned int _nbSamplesPerUnit;
};

#endif
