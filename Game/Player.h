#ifndef Game_Player_h
#define Game_Player_h

#include <iostream>

#include <util/Vec3.h>

// ----------------------------------------------------------------------------

class Player
{
public:
  Player( const Vec3& pos,
          double size,
          const Vec3& velo = Vec3(1,0,0) );

  void setPos( const Vec3& pos ){ _pos = pos; /*std::cout << "pos: " << pos << "\n"; */}
  Vec3 getPos() const { return _pos; }

  void setVelocity( const Vec3& velo ){ _velocity = velo; }
  Vec3 getVelocity() const { return _velocity; }

  Vec3 getLookingDir() const { return _lookingDir; }
  void setLookingDir( const Vec3& lookingDir ){ _lookingDir = lookingDir; }

  Vec3 getLeft() const { return _left; }
  void setLeft( const Vec3& left ){ _left = left; }

  Vec3 getUp() const { return _up; }
  void setUp( const Vec3& up ){ _up = up; }

  double getSize() const { return _size; }

private:
  Vec3 _pos;
  double _size;
  Vec3 _velocity;

  Vec3 _lookingDir{1,1,0};
  Vec3 _left{-1,0,0};
  Vec3 _up{0,0,1};
};

// ----------------------------------------------------------------------------

#endif
