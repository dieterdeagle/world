#include "InteractionHandler.h"

#include <iostream>

// ----------------------------------------------------------------------------

InteractionHandler::InteractionHandler( const std::shared_ptr<Physics>& physics )
  : _physics(physics)
{

}

// ----------------------------------------------------------------------------

bool InteractionHandler::handle( const osgGA::GUIEventAdapter& ea,
                                 osgGA::GUIActionAdapter& aa,
                                 osg::Object*, osg::NodeVisitor* )
{

  if( _requestedWarpPoint )
  {
    _requestedWarpPoint = false;
    return false;
  }

  auto key = ea.getKey();

  auto currMouseX = ea.getXnormalized();
  auto currMouseY = ea.getYnormalized();

  std::cout << "currMouseX: " << currMouseX << "\n";

//  if( _lastMouseX < -1.0 )
//    _lastMouseX = currMouseX;
//  if( _lastMouseY < -1.0 )
//    _lastMouseY = currMouseY;

//  auto diffX = currMouseX - _lastMouseX;
//  auto diffY = currMouseY - _lastMouseY;

  auto diffX = currMouseX;
  auto diffY = currMouseY;


  _physics->requestPlayerRotate( Vec2(0.001*diffX,0.001*diffY) );

//  _lastMouseX = currMouseX;
//  _lastMouseY = currMouseY;

  std::cout << diffX << "," << diffY << "\n";

  // resets mouse
//  aa.requestWarpPointer( ea.getWindowWidth() / 2, ea.getWindowHeight() / 2 );
//  _requestedWarpPoint = true;


//  _player->

  switch( key )
  {
    case osgGA::GUIEventAdapter::KEY_A :
      _physics->requestPlayerAccel( Vec3(0,-1,0) );
      break;
    case osgGA::GUIEventAdapter::KEY_D:
      _physics->requestPlayerAccel( Vec3(0,1,0) );
      break;
    case osgGA::GUIEventAdapter::KEY_W:
      _physics->requestPlayerAccel( Vec3(1,0,0) );
      break;
    case osgGA::GUIEventAdapter::KEY_S:
      _physics->requestPlayerAccel( Vec3(-1,0,0) );
      break;
    case osgGA::GUIEventAdapter::KEY_E:
      _physics->requestPlayerStop();
      break;
  }


  return false;
}

// ----------------------------------------------------------------------------
