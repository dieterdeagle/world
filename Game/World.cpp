#include "World.h"

// ----------------------------------------------------------------------------

World::World( int seedTerrain, int seedVegetation,
              const Vec2& size, double heightScaling/*,
              unsigned int nbSamplesPerUnit */)
  : _size(size)/*, _nbSamplesPerUnit(nbSamplesPerUnit)*/
{
  /// FIXME: Para?
  unsigned int MESH_SAMPLESPERUNIT = 1000;
  unsigned int VEG_SAMPLESPERUNIT = 150;
  _terrain = std::make_shared<PlaneTerrain>( Vec2(0,0), _size,
                                             heightScaling,
                                             seedTerrain );
  _vegetation = std::make_shared<Vegetation>( seedVegetation, _terrain, VEG_SAMPLESPERUNIT );
}

// ----------------------------------------------------------------------------
