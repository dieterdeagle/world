#include "PlayerUpdateCallback.h"

#include <iostream>

#include "PlayerNode.h"

// ----------------------------------------------------------------------------

PlayerUpdateCallback::PlayerUpdateCallback( const std::shared_ptr<Player>& player )
  : _player(player)
{

}

// ----------------------------------------------------------------------------

void PlayerUpdateCallback::operator()( osg::Node* node, osg::NodeVisitor* nv )
{
  PlayerNode* playerNode = dynamic_cast<PlayerNode*>(node);

  if( playerNode )
  {
    auto currPos = _player->getPos();
    playerNode->setPos( osg::Vec3( currPos.getX(),currPos.getY(),currPos.getZ() ) );
  }
  traverse(node, nv);
}

// ----------------------------------------------------------------------------
