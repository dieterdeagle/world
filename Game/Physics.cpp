#include "Physics.h"

#include <chrono>
#include <iostream>

// ----------------------------------------------------------------------------

Physics::Physics( const std::shared_ptr<PlaneTerrain>& terrain, const std::shared_ptr<Player>& player )
  : _terrain(terrain), _player(player)
{
  auto now = std::chrono::high_resolution_clock::now();
  _lastUpdateTime = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count();
}

// ----------------------------------------------------------------------------

void Physics::requestPlayerAccel( const Vec3& accel )
{
  _player->setVelocity( _player->getVelocity() + accel );
}

// ----------------------------------------------------------------------------

void Physics::requestPlayerStop()
{
  _player->setVelocity( Vec3(0,0,0) );
}

// ----------------------------------------------------------------------------

void Physics::requestPlayerRotate( const Vec2& rot )
{
  std::cout << "rot: " << rot.getX() << "\n";

  auto lookingDir = _player->getLookingDir();
  osg::Vec3d lookingDirOsg( lookingDir.getX(),lookingDir.getY(),lookingDir.getZ() );

  auto left = _player->getLeft();
  osg::Vec3d leftOsg( left.getX(), left.getY(), left.getZ() );

  osg::Matrixd rotMatrixX;
  rotMatrixX.makeRotate( rot.getX(), osg::Vec3d(0.0,0.0,1.0) );

  osg::Matrixd rotMatrixY;
  rotMatrixY.makeRotate( rot.getY(), leftOsg );

  auto rotatedDir = rotMatrixX *  rotMatrixY * lookingDirOsg;
  _player->setLookingDir( Vec3(rotatedDir[0],rotatedDir[1],rotatedDir[2]) );

  auto rotatedLeft = rotMatrixX * leftOsg;
  _player->setLeft( Vec3(rotatedLeft[0], rotatedLeft[1], rotatedLeft[2]) );

//  auto up = _player->getUp();
//  osg::Vec3d upOsg( up.getX(), up.getY(), up.getZ() );
//  osg::Matrixd rotMatrixY;
//  rotMatrixY.makeRotate( rot.getY(), leftOsg );
//  auto rotatedUp = rotMatrixY * upOsg;
//  _player->setUp( Vec3(rotatedUp[0],rotatedUp[1],rotatedUp[2]) );
}

// ----------------------------------------------------------------------------

void Physics::requestSetPlayerLooksAt( float mouseX, float mouseY )
{

}

// ----------------------------------------------------------------------------

void Physics::update()
{
//	auto currTime = std::chrono::high_resolution_clock::now();
//	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currTime - _lastUpdateTime ).count();

  auto now = std::chrono::high_resolution_clock::now();
  int millis = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count();
  auto diff = millis - _lastUpdateTime;

  if( diff > 0 )
  {
    auto velo = _player->getVelocity();

    // velo km/h.
    // velo/60 km/m
    // velo/3600 km/s
    // velo/3600000 km/ms
    auto delta = velo / 3600000 * diff;

    auto dir = _player->getLookingDir();
    auto left = _player->getLeft();

    Vec3 newPos = _player->getPos() + delta.getX() * dir.normalized() + (-delta.getY()) * left.normalized();

    auto height = _terrain->getHeightAt( Vec2( newPos.getX(), newPos.getY() ) );
    _player->setPos( Vec3( newPos.getX(), newPos.getY(), height ) );

    _lastUpdateTime = millis;
  }
}

// ----------------------------------------------------------------------------
