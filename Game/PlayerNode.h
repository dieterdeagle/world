#ifndef Game_PlayerNode_h
#define Game_PlayerNode_h

#include <memory>

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Vec3>

#include "Player.h"

class PlayerNode : public osg::Geode
{
public:
  using osg::Geode::Geode;

  PlayerNode( const std::shared_ptr<Player>& player );

  void setPos( const osg::Vec3& pos );

private:
  std::shared_ptr<Player> _player;
  osg::Vec3 _pos;
  double _size;

  osg::ref_ptr<osg::Geometry> _geometry;
};

#endif
