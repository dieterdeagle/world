#ifndef Game_Physics_h
#define Game_Physics_h

#include <chrono>
#include <memory>
#include <vector>

#include <osg/Geode>
#include <osg/NodeCallback>

#include "PlaneTerrain/PlaneTerrain.h"

#include "Player.h"

// ----------------------------------------------------------------------------

namespace PhysicsRequests
{


} // end namespace PhysicsRequests

// ----------------------------------------------------------------------------

class Physics
{
public:
  Physics( const std::shared_ptr<PlaneTerrain>& terrain,
           const std::shared_ptr<Player>& player );

  void requestPlayerAccel( const Vec3& accel );
  void requestPlayerStop();

  void requestPlayerRotate( const Vec2& rot );
  void requestSetPlayerLooksAt( float mouseX, float mouseY );

  void update();

private:
  std::shared_ptr<PlaneTerrain> _terrain;
  std::shared_ptr<Player> _player;

  int _lastUpdateTime;
//  std::chrono::time_point<std::chrono::high_resolution_clock,std::chrono::duration<int> > _lastUpdateTime;
};

#endif
