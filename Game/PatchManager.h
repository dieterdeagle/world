#ifndef Game_PatchManager_h
#define Game_PatchManager_h

#include <deque>
#include <future>
#include <map>
#include <memory>
#include <vector>

#include "PlaneTerrain/PlaneTerrainMeshCreator.h"
#include "PlaneTerrain/Patch.h"

#include "util/Vec2.h"

#include "World.h"

/**
 * @brief The PatchManager class
 */
class PatchManager
{
public:
  PatchManager( const std::shared_ptr<World>& world,
                const Vec2& patchSize,
                Vec2 startingPos,
                unsigned int meshSamplesPerUnit = 1000 /* 1 sample each meter*/);

  /**
   * @brief update checks if updates are needed and if so updates patches.
   * @param currPos typically position of player
   */
  void update( const Vec2& currPos );

  std::shared_ptr<Patch> createPatch( unsigned int id );

  unsigned int getPatchId( const Vec2& pos );
  std::deque<unsigned int> getNeighbouringIds( const Vec2& pos );

  std::pair<Vec2,Vec2> getBoundingOfPatch( unsigned int id );

  bool hasNewPatches() const { return _hasNewPatches; }
  bool hasNewIdsToRemove() const { return _hasNewIdsToRemove; }

  std::vector<std::shared_ptr<Patch> > popNewPatches();

  std::vector<unsigned int> popIdsToRemove();

private:
  void addNewPatch( const std::shared_ptr<Patch>& patch );

private:
  std::shared_ptr<World> _world;

  Vec2 _worldSize{0,0};
  Vec2 _patchSize;

  PlaneTerrainMeshCreator _meshCreator;

  unsigned int _nbPatchesX;
  unsigned int _nbPatchesY;

  std::map<unsigned int, std::shared_ptr<Patch> > _patches;

  bool _hasNewPatches = false;
  std::vector<unsigned int> _idsOfNewPatches;

  bool _hasNewIdsToRemove = false;
  std::vector<unsigned int> _idsOfPatchesToRemove;

  unsigned int _lastPosPatchId = -1; /// FIXME -1 not so nice

  std::vector< std::future<std::shared_ptr<Patch> > > _futures;
};

#endif
