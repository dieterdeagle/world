#ifndef geometry_LineSegment_h
#define geometry_LineSegment_h

#include <ostream>

#include "util/Vec2.h"

class LineSegment
{
public:
  LineSegment( const Vec2& p1, const Vec2& p2 );

  double length() const;

  Vec2 p1() const { return _p1; }
  Vec2 p2() const { return _p2; }

  void setP1( const Vec2& p1 ){ _p1 = p1; }
  void setP2( const Vec2& p2 ){ _p2 = p2; }

private:
  Vec2 _p1;
  Vec2 _p2;
};

std::ostream& operator<<( std::ostream& os, const LineSegment& lineSeg );

// ----------------------------------------------------------------------------

namespace LineUtility
{
  double distance( const LineSegment& lineSeg,
                   const Vec2& pt );

  double distance( const LineSegment& lineSeg1,
                   const LineSegment& lineSeg2 );

  bool intersecting( const LineSegment& lineSeg1,
                     const LineSegment& lineSeg2 );

  /**
   * @brief findLineIntersection Obacht! Intersection between _lines_ defined by the line segments
   *   is returned. No sanity checks.
   * @param lineSeg1
   * @param lineSeg2
   * @return
   */
  Vec2 findLineIntersection( const LineSegment& lineSeg1,
                             const LineSegment& lineSeg2 );
}

#endif
