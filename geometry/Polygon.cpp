#include "Polygon.h"

#include <iostream>

// ----------------------------------------------------------------------------

std::vector<Vec2> PolygonUtil::createEllipseFromLineSegs( const LineSegment& mainAxis,
                                                          const Vec2& sideDir,
                                                          unsigned int nbSamples )
{
  auto stepsizeMain = (mainAxis.length() / 2.0) / static_cast<double>(nbSamples-1);
  auto stepsizeSide = sideDir.length() / static_cast<double>(nbSamples-1);

  std::cout << "stepsizes: " << stepsizeMain << ", " << stepsizeSide << "\n";
  auto mainAxisMiddle = ( mainAxis.p1() + mainAxis.p2() ) / 2.0;


  std::vector<Vec2> points;
  points.reserve( 4*nbSamples );

  points.push_back( mainAxis.p1() );


  // first quadrant
  {
    LineSegment side1( mainAxis.p1(), mainAxis.p1() + sideDir );
    for( unsigned int i = 0; i < nbSamples; ++i )
    {
      auto mainSample = mainAxis.p1() + (i+1) * stepsizeMain * ( (mainAxis.p2()-mainAxis.p1()).normalized() );
      LineSegment mainSegment( mainAxisMiddle - sideDir, mainSample );

      auto sideSample = side1.p1() + (i+1) * stepsizeSide * ( (side1.p2()-side1.p1()).normalized() );
      LineSegment sideSegment( mainAxisMiddle + sideDir, sideSample );

      auto pt = LineUtility::findLineIntersection( mainSegment, sideSegment );
      points.push_back( pt );
    }
  }

  // second quadrant
  {
    LineSegment side2( mainAxis.p2() + sideDir, mainAxis.p2() );
    for( unsigned int i = 0; i < nbSamples; ++i )
    {
      auto mainSample = mainAxisMiddle + (i+1) * stepsizeMain * ( (mainAxis.p2()-mainAxis.p1()).normalized() );
      LineSegment mainSegment( mainAxisMiddle - sideDir, mainSample );

      auto sideSample = side2.p1() + (i+1) * stepsizeSide * ( (side2.p2()-side2.p1()).normalized() );
      LineSegment sideSegment( mainAxisMiddle + sideDir, sideSample );

      auto pt = LineUtility::findLineIntersection( mainSegment, sideSegment );
      points.push_back( pt );
    }
  }

  // third quadrant
  {
    LineSegment side3( mainAxis.p2(), mainAxis.p2() - sideDir );
    for( unsigned int i = 0; i < nbSamples; ++i )
    {
      auto mainSample = mainAxis.p2() - (i+1) * stepsizeMain * ( (mainAxis.p2()-mainAxis.p1()).normalized() );
      LineSegment mainSegment( mainAxisMiddle + sideDir, mainSample );

      auto sideSample = side3.p1() + (i+1) * stepsizeSide * ( (side3.p2()-side3.p1()).normalized() );
      LineSegment sideSegment( mainAxisMiddle - sideDir, sideSample );

      auto pt = LineUtility::findLineIntersection( mainSegment, sideSegment );
      points.push_back( pt );
    }
  }

    // fourth quadrant
    {
      LineSegment side4(  mainAxis.p1() - sideDir, mainAxis.p1() );
      for( unsigned int i = 0; i < nbSamples; ++i )
      {
        auto mainSample = mainAxisMiddle - (i+1) * stepsizeMain * ( (mainAxis.p2()-mainAxis.p1()).normalized() );
        LineSegment mainSegment( mainAxisMiddle + sideDir, mainSample );

        auto sideSample = side4.p1() + (i+1) * stepsizeSide * ( (side4.p2()-side4.p1()).normalized() );
        LineSegment sideSegment( mainAxisMiddle - sideDir, sideSample );

        auto pt = LineUtility::findLineIntersection( mainSegment, sideSegment );
        points.push_back( pt );
      }
    }

  return points;
}

// ----------------------------------------------------------------------------
