#ifndef geometry_Polygon_h
#define geometry_Polygon_h

#include <deque>
#include <limits>

#include "geometry/LineSegment.h"

// T is Vec2 or Vec3
template <typename T>
class Polygon
{
public:
  template <typename ITER>
  Polygon( ITER begin, ITER end )
  {
    _points = std::deque<T>( begin, end );
  }

  T operator[]( std::size_t id )
  {
    return _points[id];
  }

private:
  std::deque<T> _points;
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

namespace PolygonUtil
{
  std::vector<Vec2> createEllipseFromLineSegs( const LineSegment& mainAxis, const Vec2& sideDir,
                                     unsigned int nbSamples );

  template <typename ITER>
  bool polygonContainsPoint( ITER begin, ITER end, const Vec2& pt )
  {
    std::vector<LineSegment> segments;
    segments.reserve( std::distance(begin,end) );

    std::vector<Vec2> polygon;
    polygon.reserve( std::distance(begin,end) );
    auto maxX = std::numeric_limits<double>::lowest();
    for( auto it = begin; it != end; ++it )
    {
      polygon.push_back( *it );
      maxX = std::max( maxX, it->x() );
    }

    for( std::size_t i = 1; i < polygon.size(); ++i )
    {
      segments.push_back( LineSegment(polygon[i-1], polygon[i]) );
    }
    segments.push_back( LineSegment( polygon.back(),polygon.front() ) );

    LineSegment testSeg( pt, Vec2(4*maxX,0) );

    unsigned int counter = 0;
    for( const auto& seg: segments )
    {
      if( LineUtility::intersecting( seg, testSeg ) )
        counter++;
    }

    if( counter % 2 == 0 )
      return false;
    return true;

    // Todo: on any line segment?
  }
}

#endif
