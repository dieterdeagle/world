#include "LineSegment.h"

#include <iostream>
#include <limits>

// ----------------------------------------------------------------------------

LineSegment::LineSegment( const Vec2& p1, const Vec2& p2 )
  : _p1(p1), _p2(p2)
{

}

// ----------------------------------------------------------------------------

double LineSegment::length() const
{
  return _p1.distanceEuclidean(_p2);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool LineUtility::intersecting( const LineSegment& lineSeg1,
                                const LineSegment& lineSeg2 )
{
  auto P1 = lineSeg1.p1();
  auto P2 = lineSeg1.p2();

  auto P3 = lineSeg2.p1();
  auto P4 = lineSeg2.p2();

  auto A = P2 - P1;
  auto B = P3 - P4;
  auto C = P1 - P3;

  auto EPS = std::numeric_limits<double>::epsilon();

  double denom = (A[1]*B[0] - A[0]*B[1]);

  if( std::abs(denom) < EPS )
  {
    if( distance(lineSeg1,P3) < EPS || distance(lineSeg1,P4) < EPS )
      return true;
    if( distance(lineSeg2,P1) < EPS || distance(lineSeg2,P2) < EPS )
      return true;
    return false;
  }

  double alphaNum = (B[1]*C[0] - B[0]*C[1]);

  if( denom > 0.0 )
  {
    if( alphaNum < 0.0 || alphaNum > denom )
      return false;
  }
  else if( alphaNum > 0.0 || alphaNum < denom )
    return false;

  double betaNum = (A[0]*C[1] - A[1]*C[0]);

  if( denom > 0.0 )
  {
    if( betaNum < 0.0 || betaNum > denom )
      return false;
  }
  else if( betaNum > 0.0 || betaNum < denom )
    return false;

  return true;
}

// ----------------------------------------------------------------------------

double LineUtility::distance( const LineSegment& lineSeg,
                              const Vec2& pt )
{
  // http://paulbourke.net/geometry/pointlineplane/
  auto p1 = lineSeg.p1();
  auto p2 = lineSeg.p2();

  auto x1 = p1.getX();
  auto y1 = p1.getY();
  auto x2 = p2.getX();
  auto y2 = p2.getY();

  auto x3 = pt.getX();
  auto y3 = pt.getY();

  auto u = ( (x3-x1)*(x2-x1) + (y3-y1)*(y2-y1) ) / (p1.distanceEuclidean(p2)*p1.distanceEuclidean(p2) );

  // if not on line segment but only on line defined by line segment: choose min dist to line segment pts.
  if( u < 0.0 || u > 1.0 )
  {
    return std::min( pt.distanceEuclidean(lineSeg.p1()), pt.distanceEuclidean(lineSeg.p2()) );
  }
  else
  {
    auto x = x1 + u*(x2-x1);
    auto y = y1 + u*(y2-y1);

    return pt.distanceEuclidean( Vec2(x,y) );
  }
}

// ----------------------------------------------------------------------------

std::ostream& operator<<( std::ostream& os, const LineSegment& lineSeg )
{
  os << "[" << lineSeg.p1() << "," << lineSeg.p2() << "]";
  return os;
}

// ----------------------------------------------------------------------------

double LineUtility::distance( const LineSegment& lineSeg1,
                              const LineSegment& lineSeg2 )
{
  return std::min( distance(lineSeg1,lineSeg2.p1()),
                        std::min( distance(lineSeg1,lineSeg2.p2()),
                                  std::min( distance(lineSeg2,lineSeg1.p1()),
                                            distance(lineSeg2,lineSeg1.p2()) ) ) );
}

// ----------------------------------------------------------------------------

Vec2 LineUtility::findLineIntersection( const LineSegment& lineSeg1,
                                        const LineSegment& lineSeg2 )
{
  auto P1 = lineSeg1.p1();
  auto P2 = lineSeg1.p2();

  auto P3 = lineSeg2.p1();
  auto P4 = lineSeg2.p2();

  auto A = P2 - P1;
  auto B = P3 - P4;
  auto C = P1 - P3;

  auto EPS = std::numeric_limits<double>::epsilon();

  double denom = (A[1]*B[0] - A[0]*B[1]);

  if( std::abs(denom) < EPS )
  {
    if( distance(lineSeg1,P3) < EPS )
      return P3;
    else if( distance(lineSeg1,P4) < EPS )
      return P4;
    else if( distance(lineSeg2,P1) < EPS )
      return P1;
    else if( distance(lineSeg2,P2) < EPS )
      return P2;

//    return false;
  }

  double alphaNum = (B[1]*C[0] - B[0]*C[1]);

//  if( denom > 0.0 )
//  {
//    if( alphaNum < 0.0 || alphaNum > denom )
//      return false;
//  }
//  else if( alphaNum > 0.0 || alphaNum < denom )
//    return false;

//  double betaNum = (A[0]*C[1] - A[1]*C[0]);

//  if( denom > 0.0 )
//  {
//    if( betaNum < 0.0 || betaNum > denom )
//      return false;
//  }
//  else if( betaNum > 0.0 || betaNum < denom )
//    return false;

//  return true;

  return P1 + (alphaNum / denom) * ( P2 - P1 );
}

// ----------------------------------------------------------------------------
