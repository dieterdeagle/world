#ifndef geometry_BoundingBox_h
#define geometry_BoundingBox_h

#include "util/Vec2.h"

template <typename V>
class BoundingBox
{
public:
  BoundingBox( const V& min, const V& max )
    : _min(min), _max(max)
  {
  }

  // ----------------------------------

  bool contains( const V& pt )
  {
    auto vals = pt.getValues();
    for( std::size_t i = 0; i < vals.size(); ++i )
    {
      if( vals[i] < _min[i] || vals[i] > _max[i] )
        return false;
    }
    return true;
  }

  // ----------------------------------

private:
  V _min;
  V _max;

};

#endif

