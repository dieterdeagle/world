
#include <chrono>
#include <iostream>
#include <random>
#include <limits>

#include "util/NeuralNetwork/NeuralNetwork.hh"

// the smaller the error, the better. only for 1 input and 1 output
double fitness( NeuralNetwork& nnw, const std::vector<double>& inputs, const std::vector<double> results )
{
  double errorSum = 0.0;
  for( std::size_t i = 0; i < inputs.size(); ++i )
  {
    auto currResult = nnw.run( std::vector<double>{ inputs[i] } );
    errorSum += std::abs( currResult[0] - results[i] );
  }

  if( std::abs(errorSum) < std::numeric_limits<double>::epsilon() )
    return std::numeric_limits<double>::max();

  return 1.0 / errorSum;
}

// ---------------------------

std::vector<NeuralNetwork> evolve( std::vector<NeuralNetwork> initPop, std::size_t nbRounds )
{
  auto currPop = initPop;

  for( std::size_t r = 0; r < nbRounds; ++r )
  {

  }
}

// ---------------------------

int main()
{
  NeuralNetwork nnw( std::vector<unsigned int>{ 1,3,3,1 } );
  nnw.initRandom();

  auto result( nnw.run( std::vector<double>{ 1.0 } ) );

  for( auto r : result )
    std::cout << r << ",";
  std::cout << "\n";


  std::size_t NB_SAMPLES = 10;

  std::mt19937 twister;
  twister.seed( std::time(0) );
  std::uniform_real_distribution<double> distr( -static_cast<int>(NB_SAMPLES), NB_SAMPLES );

  std::vector<double> inSamples;
  std::vector<double> outSamples;
  auto func = [](double in){ return in*in; };

  for( std::size_t i = 0; i < NB_SAMPLES; ++i )
  {
    inSamples.push_back( distr(twister) );
    outSamples.push_back( func( inSamples.back() ) );

    std::cout << "  " << inSamples.back() << " -> " << outSamples.back() << "\n";
  }

  std::cout << "fitness: " << fitness( nnw, inSamples, outSamples ) << "\n";


  std::cout << "DNA\n";
  auto dna = nnw.getDNA();
  for( auto d : dna )
  {
    std::cout << d << ", ";
  }
  std::cout << "\n";

  NeuralNetwork nnw2( dna );
  std::cout << "oy\n";
  NeuralNetwork nnw3( std::vector<double>{2,4,0.5,0.3} );

}
