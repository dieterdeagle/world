#include "OsgGroupOfPatchGroups.h"

#include <iostream>

// ----------------------------------------------------------------------------

OsgGroupOfPatchGroups::OsgGroupOfPatchGroups()
  : osg::Group::Group()
{
  this->setDataVariance( osg::Group::DataVariance::DYNAMIC );
}

// -----------------------

void OsgGroupOfPatchGroups::addPatch( const osg::ref_ptr<OsgPatchGroup>& patch )
{
  _patches.insert( std::make_pair( patch->getId(), patch ) );
  this->addChild( patch );
}

// -----------------------

void OsgGroupOfPatchGroups::removePatch( unsigned int id )
{
  auto findIter = _patches.find( id );
  if( findIter != _patches.end() )
  {
    this->removeChild( findIter->second );
    _patches.erase( findIter );
  }
}

// ----------------------------------------------------------------------------
