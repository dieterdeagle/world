#ifndef graphics_OsgGroupOfPatchGroups_h
#define graphics_OsgGroupOfPatchGroups_h

#include <map>

#include <osg/Group>

#include "OsgPatchGroup.h"

class OsgGroupOfPatchGroups : public osg::Group
{
public:
  using osg::Group::Group;

  OsgGroupOfPatchGroups();

  void addPatch( const osg::ref_ptr<OsgPatchGroup>& patch );
  void removePatch( unsigned int id );

private:
  std::map< unsigned int, osg::ref_ptr<OsgPatchGroup> > _patches;
};

#endif
