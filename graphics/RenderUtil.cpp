#include "RenderUtil.h"

#include <osg/Geometry>
#include <osg/Image>
#include <osg/Texture2D>

#include <osg/PrimitiveSet>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

/////////////////////////////////////////////////////////
///////////////// General ///////////////////////////////
/////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------

osg::ref_ptr<OsgPatchGroup> createPatchGroup( const std::shared_ptr<Patch>& patch )
{
  auto meshGeode = createMeshPatchGeode( patch->getMesh() );
  auto forestGeode = getForestGeode( patch->getPlants() );

  osg::ref_ptr<OsgPatchGroup> patchGroup( new OsgPatchGroup( patch->getId(),
                                                             meshGeode,
                                                             forestGeode ) );
  return patchGroup;
}

//-----------------------------------------------------------------------------




/////////////////////////////////////////////////////////
///////////////// PlaneTerrain //////////////////////////
/////////////////////////////////////////////////////////

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> createMeshPatchGeode( const PlaneMesh& mesh )
{
  return createMeshPatchGeode( mesh.getMesh(), mesh.getNormals() );
}

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> createMeshPatchGeode( const std::vector<std::vector<Vec3> >& mesh,
                                               const std::vector<std::vector<Vec3> >& normals )
{
  osg::ref_ptr<osg::Vec3Array> vertices( new osg::Vec3Array );
  osg::ref_ptr<osg::Vec3Array> normalsOsg( new osg::Vec3Array );

  for( std::size_t i = 0; i < mesh.size(); ++i )
  {
    for( std::size_t j = 0; j < mesh[i].size(); ++j )
    {
      osg::Vec3 currVert( mesh[i][j].getX(), mesh[i][j].getY(), mesh[i][j].getZ() );
      vertices->push_back( currVert );

      osg::Vec3 currNormal( normals[i][j].getX(), normals[i][j].getY(), normals[i][j].getZ() );
      normalsOsg->push_back( currNormal );
    }
  }

  osg::ref_ptr<osg::Geometry> geometry( new osg::Geometry );
  geometry->setVertexArray( vertices );
  geometry->setNormalArray( normalsOsg );
  geometry->setNormalBinding( osg::Geometry::BIND_PER_VERTEX );

  osg::ref_ptr<osg::DrawElementsUInt> trianglesIds =
        new osg::DrawElementsUInt( osg::PrimitiveSet::TRIANGLES, 0);
  for( std::size_t i = 0; i < mesh.size()-1; ++i )
  {
    for( std::size_t j = 0; j < mesh[i].size()-1; ++j )
    {
      trianglesIds->push_back( (i+0) + (j+0)*mesh.size() );
      trianglesIds->push_back( (i+1) + (j+0)*mesh.size() );
      trianglesIds->push_back( (i+0) + (j+1)*mesh.size() );

      trianglesIds->push_back( (i+0) + (j+1)*mesh.size() );
      trianglesIds->push_back( (i+1) + (j+0)*mesh.size() );
      trianglesIds->push_back( (i+1) + (j+1)*mesh.size() );
    }
  }
  geometry->addPrimitiveSet( trianglesIds );

  osg::ref_ptr<osg::Geode> geode( new osg::Geode );
  geode->addDrawable( geometry );

  return geode;
}

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> createMeshPatchGeode( const std::vector<std::vector<Vec3> >& mesh,
                                               const osg::ref_ptr<osg::Texture2D>& normalMapTexture )
{
  /// TODO!
}

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Texture2D> toNormalMapTexture( const std::vector<std::vector<Vec3> >& normals )
{
  std::size_t width = normals.size();
  std::size_t height = normals[0].size();

  osg::ref_ptr<osg::Image> img( new osg::Image );
  img->allocateImage( width, height, 1, GL_RGB, GL_UNSIGNED_BYTE );

  for( std::size_t i = 0; i < width; ++i )
  {
    for( std::size_t j = 0; j < height; ++j )
    {
      *(reinterpret_cast<unsigned char*>(img->data(i,j)+0)) = (normals[i][j].getX() + 1.0) / 2.0 * 255;
      *(reinterpret_cast<unsigned char*>(img->data(i,j)+1)) = (normals[i][j].getY() + 1.0) / 2.0 * 255;
      *(reinterpret_cast<unsigned char*>(img->data(i,j)+2)) = (normals[i][j].getZ() + 1.0) / 2.0 * 255;
    }
  }

  osg::ref_ptr<osg::Texture2D> texture( new osg::Texture2D );
  texture->setImage( img );

  return texture;
}

// ----------------------------------------------------------------------------

/////////////////////////////////////////////////////////
///////////////// TreeGen ///////////////////////////////
/////////////////////////////////////////////////////////


//-----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> getTreeGeode( const std::shared_ptr<Tree>& tree )
{
  osg::ref_ptr<osg::Geometry> geom = getTreeGeometry( tree );

  osg::ref_ptr<osg::Geode> treeGeode = new osg::Geode;
  treeGeode->addDrawable( geom );

  return treeGeode;
}

//-----------------------------------------------------------------------------

void renderForest( const std::vector< std::shared_ptr<Tree> >& forest )
{

//  osg::Group* forestGroup = getForestGroup( forest );
  osg::ref_ptr<osg::Geode> forestGroup = getForestGeode( forest );

  osgViewer::Viewer viewer;
  viewer.setSceneData( forestGroup );
  std::vector<osgViewer::View*> views;
  viewer.getViews(views);
  for( auto view : views )
  {
    view->addEventHandler( new osgViewer::StatsHandler );
  }

  viewer.run();
}

//-----------------------------------------------------------------------------

osg::ref_ptr<osg::Group> getForestGroup( const std::vector<std::shared_ptr<Tree> >& forest )
{
  osg::ref_ptr<osg::Group> group( new osg::Group );

  for( auto tree : forest )
  {
    group->addChild( getTreeGeode(tree) );
  }

  return group;
}

//-----------------------------------------------------------------------------

osg::ref_ptr<osg::Program> setupShaders()
{
  osg::ref_ptr<osg::Program> pgm( new osg::Program );

  osg::ref_ptr<osg::Shader> vertShader( osg::Shader::readShaderFile( osg::Shader::VERTEX, "/home/ntr/code/cpp/World/src/TreeGen/shaders/BranchSegment.vert" ) );
  osg::ref_ptr<osg::Shader> geomShader( osg::Shader::readShaderFile( osg::Shader::GEOMETRY, "/home/ntr/code/cpp/World/src/TreeGen/shaders/BranchSegment.geom" ) );
  osg::ref_ptr<osg::Shader> fragShader( osg::Shader::readShaderFile( osg::Shader::FRAGMENT, "/home/ntr/code/cpp/World/src/TreeGen/shaders/BranchSegment.frag" ) );

  pgm->addShader( vertShader );
  pgm->addShader( geomShader );
  pgm->addShader( fragShader );

  return pgm;
}

//-----------------------------------------------------------------------------

osg::ref_ptr<osg::Geometry> getTreeGeometry( const std::shared_ptr<Tree>& tree )
{
  std::vector< std::shared_ptr<Branch> > branchStack;
  for( auto branch : tree->_branches )
  {
    branchStack.push_back( branch );
  }

  osg::ref_ptr<osg::Vec3Array> vertices( new osg::Vec3Array );
  osg::ref_ptr<osg::FloatArray> thicknesses( new osg::FloatArray );

  osg::ref_ptr<osg::Vec3Array> prevDirection( new osg::Vec3Array );

  while( !branchStack.empty() )
  {
    std::shared_ptr<Branch> currBranch = branchStack.back();
    branchStack.pop_back();

    vec3f tempDir(0,0,0);
    if( ! currBranch->_segments.empty() )
    {
      std::shared_ptr<BranchSegment> firstSeg = currBranch->_segments.front();
      tempDir = (firstSeg->_end - firstSeg->_start).normalized();


      // ----------------
      // leaves

      auto leaves = currBranch->_leaves;
      if( leaves )
        {
        osg::ref_ptr<osg::Geometry> leavesGeom( new osg::Geometry );

        osg::ref_ptr<osg::Vec3Array> leavesVertices( new osg::Vec3Array );

        for( const auto& v : leaves->_triangleStrip )
        {
          leavesVertices->push_back( osg::Vec3( v.x(),v.y(),v.z() ) );
        }
        leavesGeom->setVertexArray( leavesVertices );

        osg::ref_ptr<osg::Vec2Array> leavesTexCoords( new osg::Vec2Array );
        for( const auto& t : leaves->_texCoords )
        {
          leavesTexCoords->push_back( osg::Vec2(t.x(),t.y() ) );
        }
        leavesGeom->setTexCoordArray( 0, leavesTexCoords );

        osg::ref_ptr<osg::Texture2D> texture( new osg::Texture2D );
        texture->setImage( leaves->_img);

        leavesGeom->getOrCreateStateSet()->setTextureAttributeAndModes( 0, texture, osg::StateAttribute::ON );
      }


      //-----------------

      for( auto seg : currBranch->_segments )
      {
        prevDirection->push_back( osg::Vec3f( tempDir.getX(), tempDir.getY(), tempDir.getZ() ) );
        prevDirection->push_back( osg::Vec3f( tempDir.getX(), tempDir.getY(), tempDir.getZ() ) );
        vec3f start = seg->_start;
        vertices->push_back( osg::Vec3f( start.getX(), start.getY(), start.getZ() ) );
        vec3f end = seg->_end;

        vertices->push_back( osg::Vec3f( end.getX(), end.getY(), end.getZ() ) );
        thicknesses->push_back( seg->_thickness );
        thicknesses->push_back( seg->_thickness );
        tempDir = (seg->_end - seg->_start).normalized();
      }
    }

    for( auto child : currBranch->_children )
    {
      branchStack.push_back( child );
    }
  }

  osg::ref_ptr<osg::DrawArrays> drawArray( new osg::DrawArrays( osg::DrawArrays::PrimitiveSet::LINES, 0, vertices->size() ) );

  osg::ref_ptr<osg::Geometry> geom( new osg::Geometry );
  geom->setVertexArray( vertices );
  geom->setVertexAttribArray( 1, thicknesses );
  geom->setVertexAttribBinding( 1, osg::Geometry::BIND_PER_VERTEX );
  geom->setVertexAttribArray( 2, prevDirection );
  geom->setVertexAttribBinding( 2, osg::Geometry::BIND_PER_VERTEX );
  geom->addPrimitiveSet( drawArray );


  return geom;
}

//-----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> getForestGeode( const std::vector<std::shared_ptr<Tree> >& forest )
{
  osg::ref_ptr<osg::Geode> geode( new osg::Geode );

  for( auto tree : forest )
  {
    geode->addDrawable( getTreeGeometry(tree) );
  }

  geode->getOrCreateStateSet()->setMode( GL_LIGHTING, osg::StateAttribute::OFF );

  geode->getOrCreateStateSet()->setAttribute( setupShaders() );

  return geode;
}

//-----------------------------------------------------------------------------

osg::ref_ptr<osg::Image> osgImageFromVectorVector( const std::vector<std::vector<double> >& vectorVector )
{
  // find min and max
  double min = std::numeric_limits<double>::max();
  double max = std::numeric_limits<double>::lowest();

  for( const auto& row : vectorVector )
  {
    for( const auto& el : row )
    {
      min = std::min( min, el );
      max = std::max( max, el );
    }
  }

  auto range = max - min;

  std::cout << "min: " << min << ", max: " << max << "\n";

  Vec3 color( 255,255,255 );

  osg::ref_ptr<osg::Image> osgImg( new osg::Image );
  osgImg->allocateImage( vectorVector.size(), vectorVector[0].size(), 1, GL_RGB, GL_UNSIGNED_BYTE );

  for( std::size_t i = 0; i < vectorVector.size(); ++i )
  {
    for( std::size_t j = 0; j < vectorVector[i].size(); ++j )
    {
      auto factor = (vectorVector[i][j] - min) / range;
      // Hack! Correct: use color map
      if( factor < 0.0 )
      {
        *(reinterpret_cast<unsigned char*>( osgImg->data(i,j) + 0 ) ) = 0;
        *(reinterpret_cast<unsigned char*>( osgImg->data(i,j) + 1 ) ) = 0;
        *(reinterpret_cast<unsigned char*>( osgImg->data(i,j) + 2 ) ) = 127+factor*255;
      }
      else
      {
        *(reinterpret_cast<unsigned char*>( osgImg->data(i,j) + 0 ) ) = color.getX() * factor;
        *(reinterpret_cast<unsigned char*>( osgImg->data(i,j) + 1 ) ) = color.getY() * factor;
        *(reinterpret_cast<unsigned char*>( osgImg->data(i,j) + 2 ) ) = color.getZ() * factor;
      }
    }
  }

  return osgImg;
}

//-----------------------------------------------------------------------------
