#ifndef graphics_NoiseTexture_hh
#define graphics_NoiseTexture_hh

#include <functional>

#include <osg/Texture2D>

#include "graphics/ColorMap.hh"

#include "util/Vec3.h"

class NoiseTexture : public osg::Texture2D
{
public:
  NoiseTexture( std::size_t width,
                std::size_t height,
                const std::function<double(const Vec3&)>& noise,
                const ColorMap& cmap,
                std::size_t z = 0 );
};

#endif
