#ifndef graphics_CameraManipulatorEgo_h
#define graphics_CameraManipulatorEgo_h

#include <memory>

#include <osgGA/CameraManipulator>

#include "Game/Player.h"

class CameraManipulatorEgo : public osgGA::CameraManipulator
{
public:
  CameraManipulatorEgo( const std::shared_ptr<Player>& player );

  /** set the position of the matrix manipulator using a 4x4 Matrix.*/
  virtual void setByMatrix( const osg::Matrixd& matrix );

  /** set the position of the matrix manipulator using a 4x4 Matrix.*/
  virtual void setByInverseMatrix( const osg::Matrixd& matrix );

  /** get the position of the manipulator as 4x4 Matrix.*/
  virtual osg::Matrixd getMatrix() const;

  /** get the position of the manipulator as a inverse matrix of the manipulator, typically used as a model view matrix.*/
  virtual osg::Matrixd getInverseMatrix() const;

private:
  std::shared_ptr<Player> _player;

  mutable osg::Matrixd _matrix;
  mutable osg::Matrixd _matrixInv;
};

#endif
