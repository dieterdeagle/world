#include "OsgPatchGroup.h"

#include <iostream>

// ----------------------------------------------------------------------------

OsgPatchGroup::OsgPatchGroup( unsigned int id,
                              const osg::ref_ptr<osg::Geode>& terrainGeode,
                              const osg::ref_ptr<osg::Geode>& vegetationGeode )
  : osg::Group::Group(), _id(id), _terrainGeode(terrainGeode), _vegetationGeode(vegetationGeode)
{
  this->addChild( _terrainGeode );
  this->addChild( _vegetationGeode );
}

// ----------------------------------------------------------------------------
