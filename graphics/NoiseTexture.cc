#include "NoiseTexture.hh"

#include <iostream>

#include <osg/Image>

NoiseTexture::NoiseTexture( std::size_t width,
                            std::size_t height,
                            const std::function<double (const Vec3&)>& noise,
                            const ColorMap& cmap,
                            std::size_t z )
{

  osg::ref_ptr<osg::Image> img( new osg::Image );
  img->allocateImage( width, height, 1, GL_RGB, GL_UNSIGNED_BYTE );

  for( std::size_t j = 0; j < height; ++j )
  {
    for( std::size_t i = 0; i < width; ++i )
    {
      auto val = noise( Vec3(i/10.0, j/10.0, z/5.0) );
//      std::cout << " noise val: " << val << "\n";
      *reinterpret_cast<Color*>( img->data( i,j ) ) = cmap( val );
    }
  }

  this->setImage( img );
}
