#ifndef graphics_ImageManipulation_h
#define graphics_ImageManipulation_h

#include <iostream>
#include <limits>

#include <osg/Image>

#include "geometry/Polygon.h"
#include "graphics/Color.h"

#include "util/Vec2.h"

// --------------------------------------------------

void drawLine( const osg::ref_ptr<osg::Image>& img,
               const Vec2& pos1, const Vec2& pos2,
               const Color& color = Color(0,0,0), double thickness = 1.0 );

// --------------------------------------------------

void drawPoint( const osg::ref_ptr<osg::Image>& img,
                const Vec2& pos1,
                const Color& color );

// --------------------------------------------------

Color getPixel( const osg::ref_ptr<osg::Image>& img,
                const Vec2& pos );

// --------------------------------------------------

/**
 * @brief fillImg covers complete img with color.
 * @param img
 * @param color
 */
void fillImg( const osg::ref_ptr<osg::Image>& img,
              const Color& color );

// --------------------------------------------------

struct uc4
{
  unsigned char r = 0;
  unsigned char g = 0;
  unsigned char b = 0;
  unsigned char a = 0;
};

void clearImg( const osg::ref_ptr<osg::Image>& img );

// --------------------------------------------------

/**
 * @brief overlay
 * @param imgBackground
 * @param imgOverlay
 * @todo add blending/mixing
 * @todo positioning of overlay
 */
void overlay( const osg::ref_ptr<osg::Image>& imgBackground,
              const osg::ref_ptr<osg::Image>& imgOverlay );

// --------------------------------------------------

template <typename ITER>
void drawPolygon( const osg::ref_ptr<osg::Image>& img,
                  ITER begin, ITER end,
                  const Color& color,
                  double thickness = 1.0 )
{
  auto last = begin;
  for( auto it = begin+1; it != end; ++it )
  {
    drawLine( img, *last, *it, color, thickness );
    last = it;
  }
  drawLine( img, *(end-1), *begin, color, thickness );
}

// --------------------------------------------------

template <typename ITER>
void drawPolygonFilled( const osg::ref_ptr<osg::Image>& img,
                        ITER begin, ITER end,
                        const Color& colorOutline,
                        double thickness = 1.0,
                        const Color& colorFill = Color(50,120,5) )
{
  // find bounding box
  Vec2 min( std::numeric_limits<double>::max(), std::numeric_limits<double>::max() );
  Vec2 max( std::numeric_limits<double>::lowest(), std::numeric_limits<double>::lowest() );
  for( auto it = begin; it != end; ++it )
  {
    min[0] = std::min( min.x(), it->x() );
    min[1] = std::min( min.y(), it->y() );
    max[0] = std::max( max.x(), it->x() );
    max[1] = std::max( max.y(), it->y() );
  }

  for( unsigned int i = std::floor(min.x()); i < std::ceil(max.x()); ++i )
  {
    for( unsigned int j = std::floor(min.y()); j < std::ceil(max.y()); ++j )
    {
      if( PolygonUtil::polygonContainsPoint( begin,end,Vec2(i,j) ) )
      {
        drawPoint( img, Vec2(i,j), colorFill );
      }
    }
  }

  drawPolygon( img, begin, end, colorOutline, thickness );
}

// --------------------------------------------------

void fillAroundPixel( const osg::ref_ptr<osg::Image>& img,
                      const Vec2& pt,
                      const Color& color );

// --------------------------------------------------

bool isSanePosition( const Vec2& pt,
                     const osg::ref_ptr<osg::Image>& img );

// --------------------------------------------------

#endif
