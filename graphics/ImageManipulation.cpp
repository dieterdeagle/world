#include "ImageManipulation.h"

#include <deque>
#include <iostream>

// ----------------------------------------------------------------------------

void drawLine( const osg::ref_ptr<osg::Image>& img,
               const Vec2& pos1, const Vec2& pos2,
               const Color& color, double thickness )
{
  unsigned int STEPS_PER_PIXEL = 2;
  auto dir = (pos2 - pos1);
  dir = dir.normalized();
  Vec2 ortho( -dir[1], dir[0] );
  double length = (pos2-pos1).length();

  unsigned int nbSteps = std::ceil(length * STEPS_PER_PIXEL);
  double stepsize = length / nbSteps;

  auto currPos = pos1;
  for( unsigned int i = 0; i < nbSteps; ++i )
  {
    for( int j = - std::floor(thickness) / 2; j <= std::floor(thickness)/2; ++j )
    {
      drawPoint( img, currPos + ortho * stepsize * j, color );
    }

    currPos += dir * stepsize;
  }

}

// ----------------------------------------------------------------------------

void drawPoint( const osg::ref_ptr<osg::Image>& img,
                const Vec2& pos,
                const Color& color )
{
  auto x = std::floor( pos.x() );
  auto y = std::floor( pos.y() );

  if( x < 0 || x >= img->s() || y < 0 || y >= img->t() )
    return;

  // Todo: alpha blending / color mixing
  *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 0 ) = color.r();
  *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 1 ) = color.g();
  *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 2 ) = color.b();
  *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 3 ) = color.a();
}

// ----------------------------------------------------------------------------

void fillAroundPixel( const osg::ref_ptr<osg::Image>& img,
                      const Vec2& pt,
                      const Color& color )
{
std::cout << " - - fill around: " << pt << "\n";

  if( !isSanePosition(pt,img) )
    return;

  auto colorToOverwrite = getPixel( img, pt );

  std::deque<Vec2> points{ pt };

  while( !points.empty() )
  {
    auto currPt = points.front();
    points.pop_front();
    if(  currPt[0] < 0 || currPt[0] >= img->s()
      || currPt[1] < 0 || currPt[1] >= img->t()
      || getPixel( img, currPt ) == color )
    {
//      std::cout << "out of bounds or already visited\n";
      continue;
    }

    // draw pt
    auto currCol = getPixel( img, currPt );

    if( currCol == colorToOverwrite )
    {
      drawPoint( img, currPt, color );
      points.push_back( currPt + Vec2(-1.0, 0.0) );
      points.push_back( currPt + Vec2( 1.0, 0.0) );
      points.push_back( currPt + Vec2( 0.0,-1.0) );
      points.push_back( currPt + Vec2( 0.0, 1.0) );
    }
  }
}

// ----------------------------------------------------------------------------

Color getPixel( const osg::ref_ptr<osg::Image>& img,
                const Vec2& pos )
{
  auto x = std::floor( pos.x() );
  auto y = std::floor( pos.y() );
  return Color( *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 0 ),
                *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 1 ),
                *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 2 ),
                *( reinterpret_cast<unsigned char*>( img->data(x,y) ) + 3 ) );
}

// ----------------------------------------------------------------------------

void overlay( const osg::ref_ptr<osg::Image>& imgBackground,
              const osg::ref_ptr<osg::Image>& imgOverlay )
{
  if( imgBackground->s() != imgOverlay->s() || imgBackground->t() != imgOverlay->t() )
  {
    std::cerr << "ERR! img manipulation: overlay. image sizes differ! Doing nothing.\n";
    return;
  }

  for( int i = 0; i < imgBackground->s()*imgBackground->t(); ++i )
  {
    uc4 d = *reinterpret_cast<uc4*>(imgOverlay->data(i));
    if( d.a > 0 )
      *reinterpret_cast<uc4*>(imgBackground->data(i)) = d;
  }

}

// ----------------------------------------------------------------------------

void fillImg( const osg::ref_ptr<osg::Image>& img,
              const Color& color )
{
  for( int i = 0; i < img->s(); ++i )
  {
    for( int j = 0; j < img->t(); ++j )
    {
      drawPoint( img, Vec2(i,j), color );
    }
  }
}

// ----------------------------------------------------------------------------

bool isSanePosition(const Vec2& pt, const osg::ref_ptr<osg::Image>& img)
{
  auto x = std::floor( pt.x() );
  auto y = std::floor( pt.y() );
  if( x > 0 && x < img->s() && y > 0 && y < img->t() )
    return true;
  return false;
}

// ----------------------------------------------------------------------------

void clearImg( const osg::ref_ptr<osg::Image>& img )
{
  std::fill( reinterpret_cast<uc4*>(img->data()), reinterpret_cast<uc4*>(img->data()) + img->s()*img->t()*img->r(), uc4()  );
}

// ----------------------------------------------------------------------------
