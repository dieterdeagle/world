#ifndef graphics_Hexagon_hh
#define graphics_Hexagon_hh

#include <osg/Geometry>
#include <osg/Texture2D> // Todo: Maybe 3d?

#include "util/Vec2.h"
#include "util/Vec3.h"

class Hexagon : public osg::Geometry
{
public:
  Hexagon( const Vec3& center, double radius,
           const Vec2& centerTex, double radiusTex,
           const osg::ref_ptr<osg::Texture2D>& texture );

private:
  Vec3 _center;
  double _radius;

  Vec2 _centerTex;
  double _radiusTex;
};

#endif
