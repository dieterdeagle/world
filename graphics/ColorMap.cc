#include "ColorMap.hh"

#include <iostream>

// ----------------------------------------------------------------------------

void ColorMap::insert( double val, const Color& color )
{
  _map.insert( std::make_pair( val, color ) );
}

// ----------------------------------------------------------------------------

Color ColorMap::operator()( double val ) const
{
//  std::cout << " val in: " << val <<"\n";
  val = (val - _min) / (_max-_min);
//  std::cout << "    val mapped: " << val << "\n";

  // clamping
  if( val <= _map.begin()->first )
    return _map.begin()->second;
//  else if( val >= _map.rbegin()->first )
//    return _map.rbegin()->second;

  auto lbIter = _map.lower_bound( val );
  auto ubIter = _map.upper_bound( val );

  if( lbIter == _map.end() ) // clamp end
    return _map.rbegin()->second;

  if( ubIter == _map.end() ) // clamp begin
    return _map.rbegin()->second;

  if( lbIter == ubIter )
    --lbIter;

  auto valLb = lbIter->first;
  auto valUb = ubIter->first;

  auto distLb = val - valLb;
  auto distUb = valUb - val;

  auto wLb = distUb / (distLb+distUb);
  auto wUb = distLb / (distLb+distUb);

  return (lbIter->second * wLb) + (ubIter->second * wUb);
}

// ----------------------------------------------------------------------------

std::map<double,Color>::const_iterator ColorMap::begin() const
{
  return _map.begin();
}

// ----------------------------------------------------------------------------

std::map<double,Color>::const_iterator ColorMap::end() const
{
  return _map.end();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

std::ostream& operator<<( std::ostream& out, const ColorMap& cmap )
{
  for( auto iter = cmap.begin(); iter != cmap.end(); ++iter )
  {
    out << iter->first << ": " << iter->second << "\n";
  }

  return out;
}
