#ifndef graphics_RenderUtil_h
#define graphics_RenderUtil_h

#include <vector>
#include <memory>

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Image>
#include <osg/Program>
#include <osg/Texture2D>

#include "PlaneTerrain/PlaneMesh.h"
#include "PlaneTerrain/Patch.h"

#include "TreeGen/Tree.h"

#include "util/Vec3.h"

#include "OsgPatchGroup.h"

///////////////////////////////////////////////////
/// General RenderUtil
///////////////////////////////////////////////////

osg::ref_ptr<OsgPatchGroup> createPatchGroup( const std::shared_ptr<Patch>& patch );

osg::ref_ptr<osg::Image> osgImageFromVectorVector( const std::vector< std::vector<double> >& vectorVector );

///////////////////////////////////////////////////
/// PlaneTerrain RenderUtil
///////////////////////////////////////////////////

osg::ref_ptr<osg::Geode> createMeshPatchGeode( const PlaneMesh& mesh );

osg::ref_ptr<osg::Geode> createMeshPatchGeode( const std::vector< std::vector<Vec3> >& mesh,
                                               const std::vector< std::vector<Vec3> >& normals );

/// TODO!
osg::ref_ptr<osg::Geode> createMeshPatchGeode( const std::vector< std::vector<Vec3> >& mesh,
                                               const osg::ref_ptr<osg::Texture2D>& normalMapTexture );

osg::ref_ptr<osg::Texture2D> toNormalMapTexture( const std::vector< std::vector<Vec3> >& normals );




///////////////////////////////////////////////////
/// TreeGen RenderUtil
///////////////////////////////////////////////////

osg::ref_ptr<osg::Program> setupShaders();

osg::ref_ptr<osg::Geometry> getTreeGeometry( const std::shared_ptr<Tree>& tree );
osg::ref_ptr<osg::Geode> getForestGeode( const std::vector< std::shared_ptr<Tree> >& forest );

osg::ref_ptr<osg::Geode> getTreeGeode( const std::shared_ptr<Tree>& tree );

osg::ref_ptr<osg::Group> getForestGroup( const std::vector< std::shared_ptr<Tree> >& forest );

//void render( std::shared_ptr<Tree> tree );
void renderForest( const std::vector<std::shared_ptr<Tree> >& tree );

#endif
