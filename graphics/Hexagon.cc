#include "Hexagon.hh"


Hexagon::Hexagon( const Vec3& center, double radius,
                  const Vec2& centerTex, double radiusTex,
                  const osg::ref_ptr<osg::Texture2D>& texture )
  : _center(center), _radius(radius), _centerTex(centerTex), _radiusTex(radiusTex)
{

  auto radius2 = _radius*_radius;
  auto diagVal = std::sqrt( radius2 - (radius2)/4.0 );

  osg::ref_ptr<osg::Vec3Array> vertices( new osg::Vec3Array );
  // center
  vertices->push_back( osg::Vec3(center) );
  // left
  vertices->push_back( osg::Vec3( center + Vec3(-_radius,     0,      0) ) );
  // left top
  vertices->push_back( osg::Vec3( center + Vec3(-_radius/2.0, diagVal,0) ) );
  // right top
  vertices->push_back( osg::Vec3( center + Vec3( _radius/2.0, diagVal,0) ) );
  // right
  vertices->push_back( osg::Vec3( center + Vec3( _radius,     0,      0) ) );
  // right bottom
  vertices->push_back( osg::Vec3( center + Vec3( _radius/2.0, -diagVal,   0) ) );
  // left bottom
  vertices->push_back( osg::Vec3( center + Vec3(-_radius/2.0, -diagVal,   0) ) );
  // left
  vertices->push_back( osg::Vec3( center + Vec3(-_radius,     0,      0) ) );

  this->setVertexArray( vertices );
  this->addPrimitiveSet( new osg::DrawArrays(osg::PrimitiveSet::TRIANGLE_FAN,0,8) );


  // --- Tex stuff --------
  auto radius2Tex = _radiusTex * _radiusTex;
  auto diagValTex = std::sqrt( radius2Tex - (radius2Tex)/4.0 );

  osg::ref_ptr<osg::Vec2Array> texCoords( new osg::Vec2Array );
  //center
  texCoords->push_back( osg::Vec2(centerTex) );
  // left
  texCoords->push_back( osg::Vec2( centerTex + Vec2(-_radiusTex,     0) ) );
  // left top
  texCoords->push_back( osg::Vec2( centerTex + Vec2(-_radiusTex/2.0, diagValTex) ) );
  // right top
  texCoords->push_back( osg::Vec2( centerTex + Vec2( _radiusTex/2.0, diagValTex) ) );
  // right
  texCoords->push_back( osg::Vec2( centerTex + Vec2( _radiusTex,     0 ) ) );
  // right bottom
  texCoords->push_back( osg::Vec2( centerTex + Vec2( _radiusTex/2.0, -diagValTex) ) );
  // left bottom
  texCoords->push_back( osg::Vec2( centerTex + Vec2(-_radiusTex/2.0, -diagValTex) ) );
  // left
  texCoords->push_back( osg::Vec2( centerTex + Vec2(-_radiusTex,     0 ) ) );

  this->setTexCoordArray( 0, texCoords );

//  osg::ref_ptr<osg::Vec4Array> colors( new osg::Vec4Array );
//  colors->push_back( osg::Vec4( 0,1,0,0 ) );
//  this->setColorArray( colors, osg::Vec4Array::BIND_OVERALL );

  this->getOrCreateStateSet()->setTextureAttributeAndModes( 0, texture, osg::StateAttribute::ON );

//  this->getOrCreateStateSet()->set
}
