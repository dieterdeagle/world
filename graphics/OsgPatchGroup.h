#ifndef graphics_OsgPatchGroup_h
#define graphics_OsgPatchGroup_h

#include <osg/Geode>
#include <osg/Group>

class OsgPatchGroup : public osg::Group
{
public:
  using osg::Group::Group;

  OsgPatchGroup( unsigned int id,
                 const osg::ref_ptr<osg::Geode>& terrainGeode,
                 const osg::ref_ptr<osg::Geode>& vegetationGeode );

  unsigned int getId() const { return _id; }

private:
  unsigned int _id;
  osg::ref_ptr<osg::Geode> _terrainGeode;
  osg::ref_ptr<osg::Geode> _vegetationGeode;
};

#endif
