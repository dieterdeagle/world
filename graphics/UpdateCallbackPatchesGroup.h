#ifndef Game_UpdateCallbackPatchesGroup_h
#define Game_UpdateCallbackPatchesGroup_h

#include <memory>
#include <vector>

#include <osg/Geode>
#include <osg/NodeCallback>

#include "Game/PatchManager.h"

// ----------------------------------------------------------------------------

class UpdateCallbackPatchesGroup : public osg::NodeCallback
{
public:
  UpdateCallbackPatchesGroup( const std::shared_ptr<PatchManager>& patchMgr );

  virtual void operator()( osg::Node* node, osg::NodeVisitor* nv );

private:
  std::shared_ptr<PatchManager> _patchMgr;

};

#endif
