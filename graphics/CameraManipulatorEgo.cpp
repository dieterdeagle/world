#include "CameraManipulatorEgo.h"

// ----------------------------------------------------------------------------

CameraManipulatorEgo::CameraManipulatorEgo( const std::shared_ptr<Player>& player )
  : _player(player)
{

}

// ----------------------------------------------------------------------------

void CameraManipulatorEgo::setByMatrix( const osg::Matrixd& matrix )
{
  _matrix = matrix;
  _matrixInv.inverse(matrix);
}

// ----------------------------------------------------------------------------

void CameraManipulatorEgo::setByInverseMatrix( const osg::Matrixd& matrix )
{
  _matrix.inverse(matrix);
  _matrixInv = matrix;
}

// ----------------------------------------------------------------------------

osg::Matrixd CameraManipulatorEgo::getMatrix() const
{
  auto tempMatrix = getMatrix();
  osg::Matrixd tempInvMatrix;
  tempInvMatrix.invert(tempMatrix);
  return tempInvMatrix;
}

// ----------------------------------------------------------------------------

osg::Matrixd CameraManipulatorEgo::getInverseMatrix() const
{
  auto playerPos = _player->getPos() + Vec3( 0.00025,0.0,_player->getSize()+0.0002 );
  osg::Vec3d playerPosOsg( playerPos.getX(), playerPos.getY(), playerPos.getZ() );

  auto playerLookingDir = _player->getLookingDir();
  osg::Vec3d lookingDirOsg( playerLookingDir.getX(), playerLookingDir.getY(), playerLookingDir.getZ() );

  auto playerUp = _player->getUp();
  osg::Vec3d playerUpOsg( playerUp.getX(), playerUp.getY(), playerUp.getZ() );

  _matrix.makeLookAt( playerPosOsg,
                      playerPosOsg + lookingDirOsg,
                      playerUpOsg );
  return _matrix;
}

// ----------------------------------------------------------------------------
