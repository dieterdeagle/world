#include "Color.h"

// ----------------------------------------------------------------------------

Color::Color( unsigned char r, unsigned char g, unsigned char b, unsigned char a )
  : _r(r), _g(g), _b(b), _a(a)
{

}

// ----------------------------------------------------------------------------

Color::Color( const osg::Vec4& vec4 )
  : _r(vec4.r()*255), _g(vec4.g()*255), _b(vec4.b()*255), _a(vec4.a()*255)
{

}

// ----------------------------------------------------------------------------

Color::Color( const osg::Vec3& vec3 )
  : _r(vec3.x()*255), _g(vec3.y()*255), _b(vec3.z()*255)
{

}

// ----------------------------------------------------------------------------

bool Color::operator==( const Color& rhs )
{
  return ( _r == rhs.r() && _g == rhs.g() && _b == rhs.g() && _a == rhs.a() );
}

// ----------------------------------------------------------------------------

bool Color::operator!=( const Color& rhs )
{
  return !(*this == rhs);
}

// ----------------------------------------------------------------------------

Color& Color::operator+=( const Color& rhs )
{
  _r += rhs.r();
  _g += rhs.g();
  _b += rhs.b();
  _a += rhs.a();

  return *this;
}

// ----------------------------------------------------------------------------

Color& Color::operator*=( double d )
{
  _r *= d;
  _g *= d;
  _b *= d;
  _a *= d;

  return *this;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

std::ostream& operator<<( std::ostream& out, const Color& color )
{
  out << "(" << static_cast<int>(color.r()) << "," << static_cast<int>(color.g())
      << "," << static_cast<int>(color.b()) << "," << static_cast<int>(color.a()) << ")";

  return out;
}

// ----------------------------------------------------------------------------

Color operator*( const Color& color, double d )
{
  auto result = color;
  return result *= d;
}

// ----------------------------------------------------------------------------

Color operator+( const Color& lhs, const Color& rhs )
{
  auto result = lhs;
  return result += rhs;
}
