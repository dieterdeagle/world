#ifndef graphics_Color_h
#define graphics_Color_h

#include <ostream>

#include <osg/Vec3>
#include <osg/Vec4>

class Color
{
public:
  Color( unsigned char r,
         unsigned char g,
         unsigned char b,
         unsigned char a = 255 );

  Color( const osg::Vec4& vec4 );
  Color( const osg::Vec3& vec3 );

  // --- getter ----
  unsigned char r() const { return _r; }
  unsigned char g() const { return _g; }
  unsigned char b() const { return _b; }
  unsigned char a() const { return _a; }

  // --- setter ----
  void setR( unsigned char r ){ _r = r; }
  void setG( unsigned char g ){ _g = g; }
  void setB( unsigned char b ){ _b = b; }
  void setA( unsigned char a ){ _a = a; }

  bool operator==( const Color& rhs );
  bool operator!=( const Color& rhs );

  Color& operator+=( const Color& rhs );
  Color& operator*=( double d );

private:
  unsigned char _r = 0;
  unsigned char _g = 0;
  unsigned char _b = 0;
  unsigned char _a = 255;
};

// --------------------------

Color operator+( const Color& lhs, const Color& rhs );
Color operator*( const Color& color, double d );

std::ostream& operator<<( std::ostream& out, const Color& color );

#endif
