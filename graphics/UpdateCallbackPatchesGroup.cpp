#include "UpdateCallbackPatchesGroup.h"

#include "graphics/RenderUtil.h"

#include "OsgGroupOfPatchGroups.h"

// ----------------------------------------------------------------------------

UpdateCallbackPatchesGroup::UpdateCallbackPatchesGroup( const std::shared_ptr<PatchManager>& patchMgr )
  : _patchMgr(patchMgr)
{

}

// ----------------------------------------------------------------------------

void UpdateCallbackPatchesGroup::operator()( osg::Node* node, osg::NodeVisitor* nv )
{
  auto groupOfPatchesGroups = dynamic_cast<OsgGroupOfPatchGroups*>(node);

  if( groupOfPatchesGroups )
  {
    if( _patchMgr->hasNewPatches() )
    {
      auto patches = _patchMgr->popNewPatches();
      for( const auto& p : patches )
      {
        auto osgPatch = createPatchGroup( p );
        groupOfPatchesGroups->addPatch( osgPatch );
//        groupOfPatchesGroups->dirtyBound();
      }
    }
    if( _patchMgr->hasNewIdsToRemove() )
    {
      auto ids = _patchMgr->popIdsToRemove();
      for( auto i : ids )
      {
        std::cout << "remove: " << i << "\n";
        groupOfPatchesGroups->removePatch( i );
      }
    }
  }

  traverse(node, nv);
}

// ----------------------------------------------------------------------------
