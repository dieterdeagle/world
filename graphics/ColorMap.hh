#ifndef graphics_ColorMap_hh
#define graphics_ColorMap_hh

#include <ostream>
#include <map>

#include "Color.h"

class ColorMap
{
public:
  void insert( double val, const Color& color );

  Color operator()( double val ) const;

  std::map<double,Color>::const_iterator begin() const;
  std::map<double,Color>::const_iterator end() const;

  void setMin( double min ){ _min = min; }
  void setMax( double max ){ _max = max; }

private:
  std::map<double,Color> _map;

  double _min = 0.0;
  double _max = 1.0;
};

// -----------------------------------

std::ostream& operator<<( std::ostream& out, const ColorMap& cmap );

#endif
