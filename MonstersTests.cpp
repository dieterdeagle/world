
#include <iostream>
#include <map>
#include <string>

#include <osg/Image>
#include <osgDB/WriteFile>
#include <osgViewer/Viewer>

#include "graphics/Color.h"
#include "graphics/Hexagon.hh"
#include "graphics/NoiseTexture.hh"

#include "util/noise/FractionalBrownianMotion.h"

#include "Monsters/DecisionMaker.h"
#include "Monsters/Evolution.h"
#include "Monsters/FightCoordinator.h"
#include "Monsters/MonsterAttack.h"
#include "Monsters/MonsterBase.h"
#include "Monsters/MonsterGenerator.h"
#include "Monsters/MonsterWorld2d.h"
#include "Monsters/NameGenerator.h"

#include "util/Histogram.h"

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> createHexMap( const std::shared_ptr<MonsterWorld>& world, double radius )
{

  std::size_t TEX_SIZE = 512;

  Vec2 worldCenter( world->getWidth() / 2.0, world->getHeight() / 2.0 );
  worldCenter *=  1.5 * radius;
  auto worldRadius = world->getWidth() / 2.0 * 1.5 * radius;

  // -- fire ---------
  ColorMap fireCmap;
  fireCmap.insert( 0, Color(0,0,0,255) );
  fireCmap.insert( 0.25, Color(0,0,0,255) );
  fireCmap.insert( 0.45, Color(15,0,0,255) );
  fireCmap.insert( 0.65, Color(150,15,0,255) );
  fireCmap.insert( 0.85, Color(200,60,0,255) );
  fireCmap.insert( 1.0, Color(220,100,0,255) );
  fireCmap.setMin(-1);
  fireCmap.setMax(0.8);

  FbmFunctor<Vec3> fbmFire( 2, 256, 0, 8, 1 );
  osg::ref_ptr<NoiseTexture> texFire( new NoiseTexture( TEX_SIZE,TEX_SIZE, fbmFire, fireCmap, 0 ) );
  osgDB::writeImageFile( *texFire->getImage(), "/tmp/fireTex.bmp" );

  // -- grass (placeholder) -----
  ColorMap cmapGrass;
  cmapGrass.insert( 0, Color(20,200,5,255) );
  cmapGrass.insert( 1.0, Color(40,180,10,255) );

  FbmFunctor<Vec3> fbmGrass( 3, 256, 0, 8, 1 );
  osg::ref_ptr<NoiseTexture> texGrass( new NoiseTexture( TEX_SIZE,TEX_SIZE, fbmGrass, cmapGrass, 0 ) );
  osgDB::writeImageFile( *texGrass->getImage(), "/tmp/grassTex.bmp" );


  // -- water (todo) -----
  ColorMap cmapWater;
  cmapWater.insert( -1.0, Color( 10,40,80, 255) );
  cmapWater.insert( 0.0, Color( 10,40,100, 255) );
  cmapWater.insert( 0.5, Color( 10,40,150, 255) );
  cmapWater.insert( 1.0, Color( 10,10,210, 255) );
  FbmFunctor<Vec3> fbmWater( 4, 256, 0, 8, 1 );
  osg::ref_ptr<NoiseTexture> texWater( new NoiseTexture( TEX_SIZE,TEX_SIZE, fbmWater, cmapWater, 0 ) );
  osgDB::writeImageFile( *texWater->getImage(), "/tmp/waterTex.bmp" );

  // -- forest (todo) -----
  ColorMap cmapForest;
  cmapForest.insert( 0.0, Color( 20,100,20, 255 ) );
  cmapForest.insert( 0.5, Color( 30,120,20, 255 ) );
  cmapForest.insert( 0.8, Color( 20,140,30, 255 ) );
  cmapForest.insert( 1.0, Color( 60,180,80, 255 ) );
  FbmFunctor<Vec3> fbmForest( 5, 256, 0, 4, 1 );
  osg::ref_ptr<NoiseTexture> texForest( new NoiseTexture( TEX_SIZE,TEX_SIZE, fbmForest, cmapForest, 0 ) );
  osgDB::writeImageFile( *texForest->getImage(), "/tmp/forestTex.bmp" );

  // -- stone (todo) -----
  ColorMap cmapStone;
  cmapStone.insert( -1.0, Color( 0,0,0, 255 ) );
  cmapStone.insert( 0.0, Color( 40,40,40, 255 ) );
  cmapStone.insert( 0.2, Color( 80,60,60, 255 ) );
  cmapStone.insert( 0.4, Color( 100,80,80, 255 ) );
  cmapStone.insert( 0.6, Color( 100,100,100, 255 ) );
  cmapStone.insert( 0.8, Color( 150,150,150, 255 ) );
  cmapStone.insert( 0.9, Color( 180,180,180, 255 ) );
  cmapStone.insert( 1.0, Color( 240,240,240, 255 ) );
  FbmFunctor<Vec3> fbmStone( 6, 256,0, 5, 1 );
  osg::ref_ptr<NoiseTexture> texStone( new NoiseTexture( TEX_SIZE, TEX_SIZE, fbmStone, cmapStone, 0 ) );
  osgDB::writeImageFile( *texStone->getImage(), "/tmp/stoneTex.bmp" );


  // -----------------------------

  auto radius2 = radius*radius;
  auto diagVal = std::sqrt( radius2 - radius2/4.0 );

  std::cout << "Tex size: " << TEX_SIZE << ", world width: " << world->getWidth() << "\n";
  auto texToWorldRatio = TEX_SIZE / static_cast<double>(world->getWidth() );
  auto worldToTexRatio = world->getWidth() / static_cast<double>(TEX_SIZE);
  auto TEST = 1.0 + worldToTexRatio;


//    auto RADIUS_TEX = radius / (1.5*TEX_SIZE);
//  auto RADIUS_TEX = ( radius / ( 0.6*TEX_SIZE/TEST ) );
  auto RADIUS_TEX = 1.0 / static_cast<double>( world->getWidth() );
  RADIUS_TEX /= 1.5;

  auto radius2Tex = RADIUS_TEX * RADIUS_TEX;
  auto diagValTex = std::sqrt( radius2Tex - radius2Tex/4.0 );

  using Type = MonsterWorld::TileType;

  osg::ref_ptr<osg::Geode> geode( new osg::Geode );
  for( unsigned int j = 0; j < world->getHeight(); ++j )
  {
    for( unsigned int i = 0; i < world->getWidth(); ++i )
    {
      auto tile = world->getTileType( i,j );
      osg::ref_ptr<NoiseTexture> currTex = texGrass;

      if( tile == Type::Fire )
        currTex = texFire;
      else if( tile == Type::Water )
        currTex = texWater;
      else if( tile == Type::Forest )
        currTex = texForest;
      else if( tile == Type::Stone )
        currTex = texStone;

      Vec3 currCenter( i*1.5*radius, 2*j*diagVal, 0 );
      std::cout << "currcenter: " << currCenter << "\n";
      std::cout << "  center of world: " << worldCenter << "\n";

      if( worldCenter.distanceEuclidean( Vec2( currCenter.x(), currCenter.y() ) ) < worldRadius )
      {
        Vec2 worldPosRatio( i / static_cast<double>(world->getWidth() - 1),
                            j / static_cast<double>(world->getHeight() -1) );


        if( i % 2 == 0 )
        {
          currCenter += Vec3( 0, diagVal, 0 );
        }
//        Vec2 currCenterTex = worldPosRatio;
        Vec2 currCenterTex( RADIUS_TEX + i*1.5*RADIUS_TEX, RADIUS_TEX + 2*j*diagValTex );
        if( i % 2 == 0 )
        {
          currCenterTex += Vec2( 0, diagValTex );
        }
        std::cout << "currCenterTex: " << currCenterTex << ". radius: " << RADIUS_TEX << "\n";
        osg::ref_ptr<Hexagon> hex( new Hexagon( currCenter, radius, currCenterTex, RADIUS_TEX, currTex ) );
        geode->addDrawable( hex );
      }
    }
  }

  std::cout << "TEST: " << TEST << "\n";

  return geode;
}

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Image> drawWorldToImage( const std::shared_ptr<MonsterWorld>& world )
{
  using Type = MonsterWorld::TileType;
  std::map< Type, Color > colorMap{ std::make_pair( Type::EMPTY, Color(0,0,0) ),
                                    std::make_pair( Type::Grass, Color(25,125,5) ),
                                    std::make_pair( Type::Forest, Color(20,80,5) ),
                                    std::make_pair( Type::Fire, Color(180,10,10) ),
                                    std::make_pair( Type::Water, Color(0,0,220) ),
                                    std::make_pair( Type::Stone, Color(125,125,125) ) };


  osg::ref_ptr<osg::Image> img( new osg::Image );
  img->allocateImage( world->getWidth(), world->getHeight(), 1, GL_RGBA, GL_UNSIGNED_BYTE );

  for( unsigned int j = 0; j < world->getHeight(); ++j )
  {
    for( unsigned int i = 0; i < world->getWidth(); ++i )
    {
      auto tile = world->getTileType( i,j );
      *reinterpret_cast<Color*>( img->data(i,j) ) = colorMap.at( tile );
    }
  }

  return img;
}

// ----------------------------------------------------------------------------

void drawWorldToTerminal( const MonsterWorld& world )
{
  using Type = MonsterWorld::TileType;
  std::map< Type, std::string > symbolMap{ std::make_pair( Type::EMPTY, "E" ),
                                           std::make_pair( Type::Grass, "G" ),
                                           std::make_pair( Type::Forest, "T" ),
                                           std::make_pair( Type::Fire, "F" ),
                                           std::make_pair( Type::Water, "W" ),
                                           std::make_pair( Type::Stone, "S" ) };

  for( unsigned int i = 0; i < world.getWidth() + 1; ++i )
  {
    std::cout << "- ";
  }
  std::cout << "\n";

  for( unsigned int j = 0; j < world.getHeight(); ++j )
  {
    std::cout << "|";
    for( unsigned int i = 0; i < world.getWidth(); ++i )
    {
      auto tile = world.getTileType( i,j );
      std::cout << symbolMap.at( tile ) << " ";
    }
    std::cout << "|\n";
  }

  for( unsigned int i = 0; i < world.getWidth() + 1; ++i )
  {
    std::cout << "- ";
  }
  std::cout << "\n";
}

// ----------------------------------------------------------------------------

std::string findAndPrintPopInfo( std::vector< std::shared_ptr<MonsterBase> > pop )
{
  std::vector<double> mins( 3, std::numeric_limits<double>::max() );
  std::vector<double> maxs( 3, std::numeric_limits<double>::lowest() );
  std::vector<double> sums( 3, 0 );

  std::vector< std::vector<double> > vals( 3, std::vector<double>(pop.size(),0.0) );

  for( std::size_t j = 0; j < pop.size(); ++j )
  {
    auto mon = pop[j];
    auto attribs = mon->getAttributes();
    for( std::size_t i = 0; i < mins.size(); ++i )
    {
      vals[i][j] = attribs[i];
      mins[i] = std::min( mins[i], attribs[i] );
      maxs[i] = std::max( maxs[i], attribs[i] );
      sums[i] += attribs[i];
    }
  }


  std::stringstream format;
//  std::string out;
  auto idNameMap = MonsterDetail::idNameMap();
  for( const auto& p : idNameMap )
  {
    std::cout << "map: " << p.first << ", " << p.second << "\n";
    format << p.second << ": [" << mins[p.first] << "," << maxs[p.first] << "], " << sums[p.first]/pop.size() << "\n";
//    Histogram<double> histo( 10 );
//    auto valsTemp = vals[p.first];
//    auto valsTemp = std::vector<double>{0.1,0.5,0.8};
//    histo.insert( valsTemp );
//    format << histo << "\n";
    /// Todo: Parallele Koordinaten
  }

  return format.str();
}

// ----------------------------------------------------------------------------

int main()
{

  std::vector<double> histoTestData{ 0.0, 0.1, 0.1, 0.8, 0.4, 0.3, 0.9, 0.101, 0.102, 0.5 };
  Histogram<double> histoTest( 2 );
  histoTest.insert( histoTestData );
  std::cout << histoTest << "\n";

  NameGenerator nameGen;
  auto nameGenSimple = std::bind( &NameGenerator::createSimpleName, nameGen );

  MonsterGenerator< std::normal_distribution<double> > monGen( 15.0, 12.5, nameGenSimple );

  auto mon1 = monGen.generate();
  std::cout << *mon1 << "\n";
  auto mon2 = monGen.generate();

  std::cout << *mon2 << "\n";

  auto DeciderRand = std::make_shared<AIRandom>();
  FightCoordinator fightCoord;
  fightCoord.fight( mon1, DeciderRand, mon2, DeciderRand );


  std::size_t NB_INIT_POP = 20;
  std::vector< std::shared_ptr<MonsterBase> > initPop1;
  std::vector< std::shared_ptr<MonsterBase> > initPop2;
  initPop1.reserve( NB_INIT_POP );
  initPop2.reserve( NB_INIT_POP );
  for( std::size_t i = 0; i < NB_INIT_POP; ++i )
  {
    initPop1.push_back( monGen.generate() );
    initPop2.push_back( monGen.generate() );
  }

  auto infoPre1 = findAndPrintPopInfo( initPop1 );
  auto infoPre2 = findAndPrintPopInfo( initPop2 );


  {
  Evolution evo( nameGenSimple );
  auto monsters1 = evo.evolve( initPop1, 5 );
  auto monsters2 = evo.evolve( initPop2, 5 );

  auto infoPost1 = findAndPrintPopInfo( monsters1 );
  auto infoPost2 = findAndPrintPopInfo( monsters2 );

  std::cout << "Info pre:\n" << infoPre1 << "\n" << infoPre2 << "\n";
  std::cout << "Info post:\n" << infoPost1 << "\n" << infoPost2 << "\n";
  }

  initPop1.clear();
  initPop2.clear();



  NameGenerator fire( std::vector<std::string>{ "Fire flames glow Magma Lava Vulcano Heat Hot melt blaze red pyro smoke Sun solar blast", "ashes warm", "burn cremation" } );

  int nbNames = 20;
  for( int i = 0; i < nbNames; ++i )
  {
//    std::cout << fire.generateName() << "\n";
  }


  std::vector< std::pair<MonsterWorld::TileType,double> > worldSeedingProbs{ std::make_pair( MonsterWorld::TileType::Grass, 1.0 ),
                                                                             std::make_pair( MonsterWorld::TileType::Forest, 0.4 ),
                                                                             std::make_pair( MonsterWorld::TileType::Fire, 0.05 ),
                                                                             std::make_pair( MonsterWorld::TileType::Water, 0.2 ),
                                                                             std::make_pair( MonsterWorld::TileType::Stone, 0.2 ) };


  auto nbSeeds = 1024;
  MonsterWorldUtility::Generator worldGen( worldSeedingProbs, nbSeeds );
  auto w = 128;
  auto h = 128;

  auto world = worldGen.generate( w, h );

  auto hexMap = createHexMap( world, 1.0 );

  osgViewer::Viewer viewer;
  viewer.setSceneData( hexMap );


//  while( !viewer.done() )
//  {
//    viewer.frame();
//  }
  viewer.run();

  auto img = drawWorldToImage( world );
  osgDB::writeImageFile( *img, "/tmp/monsterWorld.png" );

  std::cout << "end of pgm\n";

  return 0;
}

