#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>

using namespace cl;
using namespace std;

/**
 * @brief main NOT WORKING!
 * @param argc
 * @param argv
 * @return
 */


int main( int argc, char* argv[] ) {
//    Context(CL_DEVICE_TYPE_DEFAULT);

  std::cout << "HEEEE 0\n";
  cl_platform_id platform;
  cl_device_id device;
  cl_context context;
  cl_uint refCount;

  std::cout << "HEEEE 1\n";

  // get first available platform
  clGetPlatformIDs(1, &platform, NULL);

  std::cout << "HEEEE 2\n";
  // get first available gpu device
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);

  std::cout << "HEEEE 3\n";
  // create context
  context = clCreateContext(NULL, 1, &device, NULL, NULL, NULL);

  std::cout << "HEEEE 4\n";

    static const unsigned elements = 1000;
    vector<float> data(elements, 5);
    Buffer a(begin(data), end(data), true, false);
    Buffer b(begin(data), end(data), true, false);
    Buffer c(CL_MEM_READ_WRITE, elements * sizeof(float));

    Program addProg(R"d(
        kernel
        void add(   global const float * restrict const a,
                    global const float * restrict const b,
                    global       float * restrict const c) {
            unsigned idx = get_global_id(0);
            c[idx] = a[idx] + b[idx];
        }
    )d", true);

    auto add = make_kernel<Buffer, Buffer, Buffer>(addProg, "add");
    add(EnqueueArgs(elements), a, b, c);

    vector<float> result(elements);
    cl::copy(c, begin(result), end(result));

    std::copy(begin(result), end(result), ostream_iterator<float>(cout, ", "));
}
