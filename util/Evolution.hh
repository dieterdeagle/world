#ifndef util_Evolution_hh
#define util_Evolution_hh

#include <functional>
#include <vector>

template <typename T>
class Evolution
{
public:

  using FitnessFunc = std::function< std::vector<double>(const std::vector<T>&) >;
  std::vector<T> evolve( std::vector<T> initPopulation, std::size_t nbRounds );

private:

};

#endif
