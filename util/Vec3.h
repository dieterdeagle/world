#ifndef util_Vec3_h
#define util_Vec3_h

#include <ostream>
#include <vector>

#include <osg/Vec3f>

#include "Vec2.h"

class Vec3
{
public:
  using ValueType = double;

  Vec3( ValueType x, ValueType y, ValueType z );
  Vec3( const Vec3& rhs);
  Vec3( const Vec2& rhsXY, ValueType rhsZ );
  Vec3( const osg::Vec3f& osgVec3 );

  ~Vec3();

  ValueType& operator[]( unsigned int i );
  ValueType operator[]( unsigned int i ) const;

  Vec3& operator+=( const Vec3& rhs );
  Vec3& operator-=( const Vec3& rhs );
  Vec3& operator/=( ValueType rhs );
  Vec3& operator*=( ValueType rhs );

  Vec3& operator=( const Vec3& rhs );

  ValueType getX() const { return this->x(); }
  ValueType getY() const { return this->y(); }
  ValueType getZ() const { return this->z(); }

  ValueType x() const { return _values[0]; }
  ValueType y() const { return _values[1]; }
  ValueType z() const { return _values[2]; }

  Vec2 getXY() const { return Vec2( getX(), getY() ); }

  explicit operator osg::Vec3f() const;

  ValueType distanceEuclidean( const Vec3& rhs ) const;

  Vec3 normalized() const;

  ValueType length() const;

  ValueType dot( const Vec3& rhs ) const;
  Vec3 cross( const Vec3& rhs ) const;

  Vec3 findPerpendicularVector() const;

private:
  std::vector<ValueType> _values;
};

const Vec3 operator+( const Vec3& lhs, const Vec3& rhs );
const Vec3 operator-( const Vec3& lhs, const Vec3& rhs );
const Vec3 operator/( const Vec3& lhs, Vec3::ValueType rhs );
const Vec3 operator*( const Vec3& lhs, Vec3::ValueType rhs );
const Vec3 operator*( Vec3::ValueType lhs, const Vec3& rhs );

std::ostream& operator<<( std::ostream& stream, const Vec3& rhs );

//osg::Vec3f& operator=( osg::Vec3f& out, const Vec3& in );

#endif
