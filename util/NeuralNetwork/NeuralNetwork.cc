#include "NeuralNetwork.hh"

#include <algorithm>
#include <iostream>


// ----------------------------------------------------------------------------

NeuralNetwork::NeuralNetwork( const std::vector<unsigned int>& topology )
  : _topology(topology)
{

}

// ----------------------------------------------------------------------------

NeuralNetwork::NeuralNetwork( const std::vector<double>& dna )
{
  auto nbLayers = std::ceil( dna.front() ); // or floor? Todo: check what happens
  _topology.reserve( nbLayers );
  std::size_t currId;
  for( currId = 1; currId < nbLayers+1; ++currId )
  {
    _topology.push_back( std::ceil( dna[ currId % dna.size() ] ) );
  }

  // first layer: each neuron one input
  Layer startingLayer;
  startingLayer.reserve( _topology.front() );
  for( std::size_t i = 0; i < _topology.front(); ++i, ++currId )
  {
    startingLayer.push_back( Neuron( std::vector<double>( 1, dna[ currId % dna.size() ] ), _nextId++ ) );
  }
  _layers.push_back( startingLayer );

  // rest of layers
  for( unsigned int layerId = 1; layerId < _topology.size(); ++layerId )
  {
    auto currNbOfNeurons = _topology[layerId];
    Layer currLayer;
    currLayer.reserve( currNbOfNeurons );

    for( unsigned int i = 0; i < currNbOfNeurons; ++i )
    {
      auto prevNbOfNeurons = _layers[ layerId - 1 ].size();
      // nb of inputs equals nb of Neurons in prev layer
      std::vector<double> rands;
      rands.reserve( prevNbOfNeurons );
      for( std::size_t k = 0; k < prevNbOfNeurons; ++k, ++currId )
      {
        rands.push_back( dna[ currId % dna.size() ]);
      }

      Neuron currNeuron( rands, _nextId++ );
      currLayer.push_back( currNeuron );
    }

    _layers.push_back( currLayer );
  }

}

// ----------------------------------------------------------------------------

std::vector<double> NeuralNetwork::getDNA() const
{
  std::vector<double> dna;
  // the first value gives the nb of layers l
  dna.push_back( _topology.size() );

  // the next l values give the nb of neurons per layer
  for( auto t : _topology )
    dna.push_back( t );

  for( auto l : _layers )
  {
    for( auto n : l )
    {
      auto weights = n.getWeights();
      for( auto w : weights )
      {
        dna.push_back( w );
      }
    }
  }

  return dna;
}

// ----------------------------------------------------------------------------

void NeuralNetwork::initRandom()
{
  _layers.clear();
  _layers.reserve( _topology.size() );

  std::mt19937 twister;
  twister.seed( std::time(0) );

  // weights between 0 and 1
  std::uniform_real_distribution<double> distr( 0.0, 1.0 );


  Layer startingLayer;
  startingLayer.reserve( _topology.front() );
  for( std::size_t i = 0; i < _topology.front(); ++i )
  {
    startingLayer.push_back( Neuron( std::vector<double>( 1, distr(twister) ), _nextId++ ) );
  }
  _layers.push_back( startingLayer );

  for( unsigned int layerId = 1; layerId < _topology.size(); ++layerId )
  {
    auto currNbOfNeurons = _topology[layerId];
    Layer currLayer;
    currLayer.reserve( currNbOfNeurons );

    for( unsigned int i = 0; i < currNbOfNeurons; ++i )
    {
      auto prevNbOfNeurons = _layers[ layerId - 1 ].size();
      // nb of inputs equals nb of Neurons in prev layer
      std::vector<double> rands;
      rands.reserve( prevNbOfNeurons );
      for( std::size_t k = 0; k < prevNbOfNeurons; ++k )
        rands.push_back( distr( twister ) );

      Neuron currNeuron( rands, _nextId++ );
      currLayer.push_back( currNeuron );
    }

    _layers.push_back( currLayer );
  }

}

// ----------------------------------------------------------------------------

std::vector<double> NeuralNetwork::run( const std::vector<double>& inputs )
{
  // set first inputs of first layer:
  for( std::size_t i = 0; i < inputs.size(); ++i )
  {
    _layers[0][i].setInput( 0, inputs[i] );
//    auto nextLayer = _layers[1];
//    auto currResult = _layers[0][i].process();
//    for( auto& nextLayerNeuron : nextLayer )
//    {
//      nextLayerNeuron.setInput( i, currResult );
//    }
  }

  for( std::size_t idLayer = 0; idLayer < _layers.size() - 1; ++ idLayer )
  {
    auto& currLayer = _layers[ idLayer ];
    auto& nextLayer = _layers[ idLayer + 1 ];
    for( std::size_t idNeuron = 0; idNeuron < currLayer.size(); ++idNeuron )
    {
      auto& currNeuron = currLayer[ idNeuron ];
      auto currResult = currNeuron.process();

      for( auto& nextLayerNeuron : nextLayer )
      {
        nextLayerNeuron.setInput( idNeuron, currResult );
      }
    }
  }

  auto lastLayer = _layers.back();
  std::vector<double> result;
  result.reserve( lastLayer.size() );

  for( auto& currNeuron : lastLayer )
  {
    result.push_back( currNeuron.process() );
  }

  return result;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

Neuron::Neuron( const std::vector<double>& weights, unsigned int id )
  : _inputWeights(weights), _id(id)
{
  // set inputs to 0.0 by default
  _inputs.resize( weights.size(), 0.0 );

  std::cout << "Neuron with:\n";
  for( auto w : weights )
    std::cout << w << ", ";
  std::cout << "\n";
}

// ----------------------------------------------------------------------------

void Neuron::setInput( std::size_t inputId, double input )
{
  auto weightedIn = _inputWeights[ inputId ] * input;
  std::cout << " neuron " << _id << ": set input " << inputId << " to " << weightedIn << "\n";

  _inputs[ inputId ] = weightedIn;

  std::cout <<  "  input is now: " << _inputs[ inputId ] << "\n";
}

// ----------------------------------------------------------------------------

double Neuron::process()
{
  std::cout << " neuron " << _id << ": in:\n";
  for( auto in : _inputs )
    std::cout << in << ", ";
  std::cout << "\n";
  auto result = _func( _inputs.begin(), _inputs.end() );
  std::cout << "  neuron result: " << result << "\n";
  return result;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

NeuralNetworkDetail::Mater::Mater()
{
  _twister.seed( std::time(0) );
}

// ----------------------------------------------------------------------------

NeuralNetwork NeuralNetworkDetail::Mater::mate( const NeuralNetwork& nnw1, const NeuralNetwork& nnw2 )
{
  auto dna1 = nnw1.getDNA();
  auto dna2 = nnw2.getDNA();

  std::vector<double> childDna;

  auto childDnaSize = dna2.size();
  if( _distrOneOrTwo( _twister ) == 0 )
    childDnaSize = dna1.size();
  if( _distrUniReal( _twister ) < _probOfMutation )
  {
    if( _distrMutationIntensity( _twister ) < 0 )
      childDnaSize -= 1;
    else
      childDnaSize += 1;
  }

  for( std::size_t i = 0; i < childDnaSize; ++i )
  {
    if( _distrOneOrTwo( _twister ) == 0 )
      childDna.push_back( dna1[ i%dna1.size() ] );
    else
      childDna.push_back( dna2[ i%dna2.size() ] );
  }

  return NeuralNetwork( childDna );

  // TODO: work in progress

//  auto layers1 = nnw1.getLayers();
//  auto layers2 = nnw2.getLayers();

//  std::vector<NeuralNetwork::Layer> resultLayers;

//  for( std::size_t layerId = 0; layerId < layers1.size(); ++layerId )
//  {
//    auto currLayer1 = layers1[ layerId ];
//    auto currLayer2 = layers2[ layerId ];

//    NeuralNetwork::Layer resultCurrLayer;

//    for( std::size_t neuronId = 0; neuronId < currLayer1.size(); ++ neuronId )
//    {
//      auto randNnwId = _distrOneOrTwo( _twister );
//      std::cout << " randNnwId: " << randNnwId << "\n";
//      std::vector<double> resultCurrWeights = currLayer1[neuronId].getWeights();
//      if( randNnwId == 1 )
//      {
//        resultCurrWeights = currLayer2[neuronId].getWeights();
//      }

//      // mutate
//      for( auto& w : resultCurrWeights )
//      {
//        auto randMutationVal = _distrUniReal( _twister );
//        std::cout << " rand mutation val: " << randMutationVal << "\n";
//        if( randMutationVal < _probOfMutation )
//        {
//          // mutate

//        }
//      }
//    }
//  }
}
