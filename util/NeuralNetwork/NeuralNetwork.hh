#ifndef util_NeuralNetwork_h
#define util_NeuralNetwork_h

#include <chrono>
#include <functional>
#include <numeric>
#include <random>
#include <vector>

class Neuron;

class NeuralNetwork
{
public:
  using Layer = std::vector<Neuron>;

  NeuralNetwork( const std::vector<unsigned int>& topology );

  /**
   * @brief NeuralNetwork create from dna
   * @param dna
   */
  NeuralNetwork( const std::vector<double>& dna );

  /**
   * @brief getDNA
   * @return
   */
  std::vector<double> getDNA() const;

  void initRandom();

  std::vector<double> run( const std::vector<double>& inputs );

  std::vector<Layer> getLayers() const { return _layers; }

private:
  std::vector<unsigned int> _topology;

  std::vector<Layer> _layers;

  unsigned int _nextId = 0;
};

// ---------------------------

namespace NeuralNetworkDetail
{
  class Mater
  {
  public:
    Mater();

    /**
     * @brief mate assuming same topologies for the moment.
     * @param nnw1
     * @param nnw2
     * @return
     */
    NeuralNetwork mate( const NeuralNetwork& nnw1, const NeuralNetwork& nnw2 );

  private:
    std::mt19937 _twister;
    std::uniform_int_distribution<std::size_t> _distrOneOrTwo{ 0, 1 };
    std::uniform_real_distribution<double> _distrUniReal;

    // default: 3 percent
    double _probOfMutation = 0.03;

    // Todo: Think about intensity values
    std::uniform_real_distribution<double> _distrMutationIntensity{ -1.0, 1.0 };
  };

} // end namespace NeuralNetworkDetail

// -----------------

class Neuron
{
public:
  Neuron( const std::vector<double>& weights, unsigned int id );

  std::vector<double> getWeights() const { return _inputWeights; }

  void setInput( std::size_t inputId, double input );

  double process();

private:
  std::vector<double> _inputs;

  std::vector<double> _inputWeights;

  std::function<double( const std::vector<double>::iterator&,
                        const std::vector<double>::iterator& )> _func = std::bind( std::accumulate<std::vector<double>::iterator, double>,
                                                                                   std::placeholders::_1, std::placeholders::_2,
                                                                                   0.0 );

  unsigned int _id;
};

#endif
