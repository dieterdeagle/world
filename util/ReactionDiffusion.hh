#ifndef util_ReactionDiffusion_hh
#define util_ReactionDiffusion_hh

#include "Array2d.hh"

struct Densities
{
  Densities( const Array2d<>& a, const Array2d<>& b )
    : _a(a), _b(b)
  {}

  Array2d<> _a;
  Array2d<> _b;
};

// -------------------------------

class ReactionDiffusion
{
public:
  ReactionDiffusion( double dA, double dB, double feed, double kill );

  Densities calcNextStep( Densities& densities, double timeDelta );

//  Densities calcNextStepCL( Densities& densities, double timeDelta );

private:
  double _dA;
  double _dB;
  double _feed;
  double _kill;

};

#endif
