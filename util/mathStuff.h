#ifndef World_util_math_h
#define World_util_math_h

#include "Vec3.h"

const static float PI = 3.14159265359;

// ----------------------------------------------------------------------------

Vec3 computeNormalTriangle( const Vec3& A, const Vec3& B, const Vec3& C );

// ----------------------------------------------------------------------------

Vec3 rotate( const Vec3& input, float degreeX, float degreeY, float degreeZ );

// ----------------------------------------------------------------------------

Vec2 rotate( const Vec2& in/*, const Vec2& center*/, double degree );

// ----------------------------------------------------------------------------

#endif
