#include "ReactionDiffusion.hh"

#include <CL/cl.hpp>

#include "util/Convolution.hh"

// -------------------------------------

ReactionDiffusion::ReactionDiffusion( double dA, double dB, double feed, double kill )
  : _dA(dA), _dB(dB), _feed(feed), _kill(kill)
{

}

// -------------------------------------

Densities ReactionDiffusion::calcNextStep( Densities& densities, double timeDelta )
{
  auto arrA = densities._a;
  auto arrB = densities._b;

  Densities result = densities;

  Array2d<> filter( 3,3, std::vector<double>{ 0.05,0.2,0.05,
                                              0.2  ,-1 ,0.2,
                                              0.05,0.2,0.05 } );

  auto aConv = convolution3x3( arrA, filter );
  auto bConv = convolution3x3( arrB, filter );

  // not working
//  Array2d<> filter1( 3,1, std::vector<double>{ 0.25, 1, 0.25 } );
//  Array2d<> filter2( 1,3, std::vector<double>{ 0.2,  1, 0.2 } );
//  auto aConv = convolution3x3Seperated( arrA, filter1, filter2 );
//  auto bConv = convolution3x3Seperated( arrB, filter1, filter2 );

  for( std::size_t i = 1; i < densities._a.width()-1; ++i )
  {
    for( std::size_t j = 1; j < densities._a.height()-1; ++j )
    {
      auto a = densities._a(i,j);
      auto b = densities._b(i,j);
//      std::cout << "a: " << a << ", b: " << b << "\n";
      result._a(i,j) += ( _dA * aConv(i,j) - a*b*b + _feed * (1.0-a) ) * timeDelta;
      result._b(i,j) += ( _dB * bConv(i,j) + a*b*b - (_kill+_feed)*b ) * timeDelta;
    }
  }

  return result;
}

// -------------------------------------

//Densities ReactionDiffusion::calcNextStepCL( const Densities& densities, double timeDelta )
//{

//}

// -------------------------------------
