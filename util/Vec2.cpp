#include "Vec2.h"

#include <cmath>
#include <iostream>
#include <limits>

// ----------------------------------------------------------------------------

Vec2::Vec2( double x, double y )
{
  _values = std::vector<double>{ x, y };
}

// ----------------------------------------------------------------------------

Vec2::Vec2( const Vec2& rhs )
{
  _values = rhs._values;
}

// ----------------------------------------------------------------------------

Vec2::Vec2( const osg::Vec2d& osgVec2 )
{
  _values = std::vector<double>{ osgVec2.x(), osgVec2.y() };
}

// ----------------------------------------------------------------------------

Vec2::Vec2( const osg::Vec2f& osgVec2 )
{
  _values = std::vector<double>{ osgVec2.x(), osgVec2.y() };
}

// ----------------------------------------------------------------------------

Vec2::~Vec2()
{

}

// ----------------------------------------------------------------------------

double& Vec2::operator[]( unsigned int i )
{
  return _values[i];
}

// ----------------------------------------------------------------------------

double Vec2::operator[]( unsigned int i ) const
{
  return _values[i];
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator+=( const Vec2& rhs )
{
  _values[0] += rhs.getX();
  _values[1] += rhs.getY();

  return *this;
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator+=( Vec2::ValueType rhs )
{
  _values[0] += rhs;
  _values[1] += rhs;

  return *this;
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator-=( Vec2::ValueType rhs )
{
  _values[0] -= rhs;
  _values[1] -= rhs;

  return *this;
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator-=( const Vec2& rhs )
{
  _values[0] -= rhs.getX();
  _values[1] -= rhs.getY();

  return *this;
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator/=( const Vec2& rhs )
{
  _values[0] /= rhs._values[0];
  _values[1] /= rhs._values[1];

  return *this;
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator/=( Vec2::ValueType rhs )
{
  _values[0] /= rhs;
  _values[1] /= rhs;

  return *this;
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator*=( Vec2::ValueType rhs )
{
  _values[0] *= rhs;
  _values[1] *= rhs;

  return *this;
}

// ----------------------------------------------------------------------------

Vec2& Vec2::operator*=( const Vec2& rhs )
{
  _values[0] *= rhs._values[0];
  _values[1] *= rhs._values[1];

  return *this;
}

// ----------------------------------------------------------------------------

Vec2::operator osg::Vec2f() const
{
  return osg::Vec2d( this->x(), this->y() );
}

// ----------------------------------------------------------------------------

double Vec2::distanceEuclidean( const Vec2& rhs ) const
{
  Vec2 tmp = *this - rhs;

  return tmp.length();
}

// ----------------------------------------------------------------------------

Vec2 Vec2::normalized() const
{
  if( this->length() < std::numeric_limits<Vec2::ValueType>::epsilon() )
    return *this;

  Vec2 tmp = *this / this->length();

  return tmp;
}

// ----------------------------------------------------------------------------

double Vec2::length() const
{
  double summedQuadraticDistance =
        ( this->getX() * this->getX() )
      + ( this->getY() * this->getY() );

  return std::sqrt( summedQuadraticDistance );
}

// ----------------------------------------------------------------------------

double Vec2::dot( const Vec2& rhs ) const
{
  return this->getX()*rhs.getX() + this->getY()*rhs.getY();
}

// ----------------------------------------------------------------------------

/// ---------------------------------------------------------------------------
/// ---------------------------------------------------------------------------

// ----------------------------------------------------------------------------

const Vec2 operator+( const Vec2& lhs, const Vec2& rhs )
{
  Vec2 tmp( lhs );
  return tmp += rhs;
}

// ----------------------------------------------------------------------------

const Vec2 operator-( const Vec2& lhs, const Vec2& rhs )
{
  Vec2 tmp( lhs );
  return tmp -= rhs;
}

// ----------------------------------------------------------------------------

const Vec2 operator/(const Vec2& lhs, double rhs)
{
  Vec2 tmp( lhs );
  return tmp /= rhs;
}

// ----------------------------------------------------------------------------

const Vec2 operator*(const Vec2& lhs, double rhs)
{
  Vec2 tmp( lhs );
  return tmp *= rhs;
}

// ----------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& stream, const Vec2& rhs)
{
  return stream << "(" << rhs.getX() << "," << rhs.getY() << ")";
}

// ----------------------------------------------------------------------------

const Vec2 operator*( Vec2::ValueType lhs, const Vec2& rhs )
{
  return rhs * lhs;
}

// ----------------------------------------------------------------------------

Vec2 floor( const Vec2& in )
{
  return Vec2( std::floor(in.getX()), std::floor(in.getY()) );
}

// ----------------------------------------------------------------------------

Vec2 ceil( const Vec2& in )
{
  return Vec2( std::ceil(in.getX()), std::ceil(in.getY()) );
}

// ----------------------------------------------------------------------------

const Vec2 operator/( const Vec2& lhs, const Vec2& rhs )
{
  return Vec2( lhs.getX() / rhs.getX(), lhs.getY() / rhs.getY() );
}

// ----------------------------------------------------------------------------

const Vec2 operator+( const Vec2& lhs, double rhs )
{
  return Vec2(lhs) += rhs;
}

// ----------------------------------------------------------------------------

const Vec2 operator+( double lhs, const Vec2& rhs )
{
  return rhs + lhs;
}

// ----------------------------------------------------------------------------

const Vec2 operator*( const Vec2& lhs, const Vec2& rhs )
{
  return Vec2(lhs) *= rhs;
}

// ----------------------------------------------------------------------------

const Vec2 operator-( const Vec2& lhs, double rhs )
{
  return Vec2(lhs) -= rhs;
}

// ----------------------------------------------------------------------------

const Vec2 operator-( double lhs, const Vec2& rhs )
{
  return rhs - lhs;
}

// ----------------------------------------------------------------------------
