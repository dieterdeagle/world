#ifndef FractionalBrownianMotion_h
#define FractionalBrownianMotion_h

#include <iostream>

#include "util/noise/PerlinNoise.h"
#include "util/Vec3.h"

/// http://www.iquilezles.org/www/articles/morenoise/morenoise.htm

double fbm( Vec3 pos, int n, const PerlinNoise& perlin, double w = 0.5, double s = 2.0 );


#include <functional>
#include <cmath>

// ----------------------------------------------------------------------------

template <typename T>
class FbmFunctor
{
public:
  FbmFunctor( std::function<double(const T&,double)> noise,
              int octaves = 8,
              double detail = 1.0,
              double w = 0.5,
              double s = 2.0 )
    : _noise(noise), _octaves(octaves), _detail(detail), _w(w), _s(s)
  {
  }

  // ---------------------------------------

  FbmFunctor( int seed,
              unsigned int size,
              int repeat,
              int octaves = 8,
              double detail = 1.0,
              double w = 0.5,
              double s = 2.0 )
    : _size(size), _octaves(octaves), _detail(detail), _w(w), _s(s)
  {
    auto repeatN = size * detail;
//    repeatN = size; // test
    std::cout << "FbmFunctor seed: " << seed << "\n";
    std::cout << "  size: " << size << ", detail: " << detail << ", repeat: " << repeatN << "\n";
    _noise = PerlinNoise( seed, size );
  }

  // ---------------------------------------

  double operator()( const T& pos ) const
  {
//    double detail = 0.1;
    double sum = 0.0;
    for( int i = 0; i < _octaves; ++i )
    {
      auto repeat = _size * _detail *  std::pow(_s,i);
//      std::cout << "i = " << i << "\n";
//      std::cout << "  pos: " << pos << " * detail " << _detail << "\n";
//      std::cout << "  pos pre: " << pos << ", pos post: " << pos*_detail << "\n";
      sum += /*std::abs*/( std::pow(_w,i) * _noise( pos * _detail * std::pow(_s,i), repeat ) );
    }
    return sum;
  }

private:
  std::function<double(const T&,double)> _noise;

  unsigned int _size;
  int _octaves;
  double _detail;
  double _w;
  double _s;
};


#endif
