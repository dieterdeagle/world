#include "FractionalBrownianMotion.h"

#include <cmath>

// ----------------------------------------------------------------------------

double fbm( Vec3 pos, int n, const PerlinNoise& perlin, double w, double s )
{
  double sum = 0.0;
  for( int i = 0; i < n; ++i )
  {
    sum += /*std::abs*/( std::pow(w,i) * perlin.noise( pos * std::pow(s,i) ) );
  }
  return sum;
}
