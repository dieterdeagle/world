#ifndef util_noise_ridgedNoise_h
#define util_noise_ridgedNoise_h

#include <math.h>
#include <functional>

template <typename T>
class RidgedNoiseFunctor
{
public:
  RidgedNoiseFunctor( std::function<double(const T&)> noise,
              int octaves = 8,
              double w = 0.5,
              double s = 2.0 )
    : _noise(noise), _octaves(octaves), _w(w), _s(s)
  {
  }

  double operator()( const T& pos ) const
  {
    double sum = 0.0;
    double freq = 1.0;
    double amp = 1.0;
    double lacunaricity = 2.0;
    double gain = 0.5;
    for( int i = 0; i < _octaves; ++i )
    {
      double n = ridgedNoise( pos*freq );
      sum += n*amp;
      freq *= lacunaricity;
      amp *= gain;
    }
    return sum;
  }

private:
  double ridgedNoise( const T& pos ) const
  {
    return 1.0 - std::abs( _noise(pos) );
  }

  std::function<double(const T&)> _noise;

  int _octaves;
  double _w;
  double _s;
};


#endif
