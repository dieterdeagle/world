#include "PerlinNoise.h"

#include <algorithm>
#include <cmath>
#include <iostream>

#include <random>

// ----------------------------------------------------------------------------

PerlinNoise::PerlinNoise(int seed, unsigned int size)
  : _size(size)
{
  _grad3 = std::vector< Vec3 >{ Vec3(1,1,0), Vec3(-1,1,0), Vec3(1,-1,0),
                                Vec3(-1,-1,0), Vec3(1,0,1), Vec3(-1,0,1),
                                Vec3(1,0,-1), Vec3(-1,0,-1), Vec3(0,1,1),
                                Vec3(0,-1,1), Vec3(0,1,-1), Vec3(0,-1,-1) };

//  _grad2 = std::vector<Vec2>{ Vec2(1,0), Vec2(-1,0), Vec2(0,1), Vec2(0,-1) };
//  _grad2 = std::vector<Vec2>{ Vec2(1,1), Vec2(-1,-1), Vec2(-1,1), Vec2(1,-1) };
  _grad2 = std::vector<Vec2>{ Vec2(1,0), Vec2(-1,0), Vec2(0,1), Vec2(0,-1),
      Vec2(1,1), Vec2(-1,-1), Vec2(-1,1), Vec2(1,-1) };
//  _grad2 = std::vector<Vec2>{ Vec2(1,0), Vec2(-1,0) };

  fillP( seed, _size );

//  std::cout << "Perlin repeat: " << _repeat << "\n";

  std::cout << "Perlin seed: " << seed << "\n";
  std::cout << "size of p: " << _p.size() << std::endl;

  _perm = std::vector<int>(2*_size);
  for( int i=0; i<2*_size; ++i )
  {
    _perm[i] = _p[i & (_size-1)];

  }
}

// ----------------------------------------------------------------------------

PerlinNoise::~PerlinNoise()
{

}

// ----------------------------------------------------------------------------

double PerlinNoise::noise( double x, double y, double repeat ) const
{
  if( repeat > 0 )
  {
    if( x > repeat )
    {
      x -= std::floor( x / repeat ) * repeat;
    }
    else if( x < 0.0 )
    {
      x += repeat;
    }
    if( y > repeat )
    {
      y -= std::floor( y / repeat ) * repeat;
    }
    else if( y < 0.0 )
    {
      y += repeat;
    }

  }

  int X = std::floor(x);
  int Y = std::floor(y);

  x = x - static_cast<double>(X);
  y = y - static_cast<double>(Y);

  X = X & (_size-1);
  Y = Y & (_size-1);

  int repeatI = _size;
  if( repeat > 0 )
  repeatI = std::ceil(repeat);

  // Calculate a set of eight hashed gradient indices
  int gi00 = _perm[ X%repeatI+_perm[ Y%repeatI ] ] % _grad2.size();
  int gi01 = _perm[ X%repeatI+_perm[ (Y+1)%repeatI ] ] % _grad2.size();
  int gi10 = _perm[ (X+1)%repeatI+_perm[ Y%repeatI ] ] % _grad2.size();
  int gi11 = _perm[ (X+1)%repeatI+_perm[ (Y+1)%repeatI ] ] % _grad2.size();

  // Calculate noise contributions from each of the eight corners
  double n00 = _grad2[gi00].dot( Vec2(x,y) );
  double n10 = _grad2[gi10].dot( Vec2(x-1,y) );
  double n01 = _grad2[gi01].dot( Vec2(x,y-1) );
  double n11 =_grad2[gi11].dot( Vec2(x-1,y-1) );

  // Compute the fade curve value for each of x, y, z
  double u = fade(x);
  double v = fade(y);

  // Interpolate along x the contributions from each of the corners
  double nx0 = mix(n00, n10, u);
  double nx1 = mix(n01, n11, u);

  // Interpolate the four results along y
  double nxy = mix(nx0, nx1, v);

//  std::cout << "nxy: " << nxy << "\n";
  return nxy;
}

// ----------------------------------------------------------------------------

double PerlinNoise::noise(double x, double y, double z, double repeat ) const
{

//  int X = (x > 0.0 ? (int)x : (int)x - 1);
  int X = std::floor(x);
//  int Y = (y > 0.0 ? (int)y : (int)y - 1);
  int Y = std::floor(y);
//  int Z = (z > 0.0 ? (int)z : (int)z - 1);
  int Z = std::floor(z);

//  std::cout << "x: " << x << ", y: " << y << ", z: " << z << std::endl;
  x = x - static_cast<double>(X);
  y = y - static_cast<double>(Y);
  z = z - static_cast<double>(Z);

//  std::cout << "x: " << x << ", y: " << y << ", z: " << z << std::endl;

  X = X & (_size-1);
  Y = Y & (_size-1);
  Z = Z & (_size-1);

  // Calculate a set of eight hashed gradient indices
  int gi000 = _perm[ X+_perm[ Y+_perm[Z] ] ] % 12;
  int gi001 = _perm[ X+_perm[ Y+_perm[Z+1] ] ] % 12;
  int gi010 = _perm[ X+_perm[ Y+1+_perm[Z] ] ] % 12;
  int gi011 = _perm[ X+_perm[ Y+1+_perm[Z+1] ] ] % 12;
  int gi100 = _perm[ X+1+_perm[ Y+_perm[Z] ] ] % 12;
  int gi101 = _perm[ X+1+_perm[ Y+_perm[Z+1] ] ] % 12;
  int gi110 = _perm[ X+1+_perm[ Y+1+_perm[Z] ] ] % 12;
  int gi111 = _perm[ X+1+_perm[ Y+1+_perm[Z+1] ] ] % 12;

  // Calculate noise contributions from each of the eight corners
  double n000 = dot(_grad3[gi000], x, y, z);
  double n100 = dot(_grad3[gi100], x-1, y, z);
  double n010 = dot(_grad3[gi010], x, y-1, z);
  double n110 = dot(_grad3[gi110], x-1, y-1, z);
  double n001 = dot(_grad3[gi001], x, y, z-1);
  double n101 = dot(_grad3[gi101], x-1, y, z-1);
  double n011 = dot(_grad3[gi011], x, y-1, z-1);
  double n111 = dot(_grad3[gi111], x-1, y-1, z-1);

  // Compute the fade curve value for each of x, y, z
  double u = fade(x);
  double v = fade(y);
  double w = fade(z);

//  std::cout << "u: " << u << ", v: " << v << ", w: " << w << std::endl;

  // Interpolate along x the contributions from each of the corners
  double nx00 = mix(n000, n100, u);
  double nx01 = mix(n001, n101, u);
  double nx10 = mix(n010, n110, u);
  double nx11 = mix(n011, n111, u);

  // Interpolate the four results along y
  double nxy0 = mix(nx00, nx10, v);
  double nxy1 = mix(nx01, nx11, v);

  // Interpolate the two last results along z
  double nxyz = mix(nxy0, nxy1, w);

  return nxyz;
}

// ----------------------------------------------------------------------------

double PerlinNoise::noise( const Vec3& xyz ) const
{
  return noise( xyz.getX(), xyz.getY(), xyz.getZ(), 0 );
}

// ----------------------------------------------------------------------------

Vec3 PerlinNoise:: perlinNoiseDerivative( const Vec2& pos, double seed ) const
{
  // http://www.decarpentier.nl/scape-procedural-extensions

  double iX = std::floor( pos.getX() );
  double iY = std::floor( pos.getY() );

  Vec2 f = pos - Vec2( iX, iY );

  // Get weights from the coordinate fraction
  Vec2 w = f * f * f * (f * (f * 6 - 15) + 10); // 6f^5 - 15f^4 + 10f^3
  std::vector<double> w4{ 1, w.getX(), w.getY(), w.getX() * w.getY() };

  // Get the derivative dw/df
  Vec2 dw = f * f * (f * (f * 30.0 - 60.0) + 30.0); // 30f^4 - 60f^3 + 30f^2

  // Get the derivative d(w*f)/df
  Vec2 dwp = f * f * f * (f * (f * 36.0 - 75.0) + 40.0); // 36f^5 - 75f^4 + 40f^3

  auto x = pos.getX();
  auto y = pos.getY();
  int X = std::floor(x);
  int Y = std::floor(y);

  x = x - static_cast<double>(X);
  y = y - static_cast<double>(Y);

  X = X & (_size-1);
  Y = Y & (_size-1);

  // Calculate a set of eight hashed gradient indices
  int gi00 = _perm[ X+_perm[ Y ] ] % _grad2.size();
  int gi01 = _perm[ X+_perm[ Y+1 ] ] % _grad2.size();
  int gi10 = _perm[ X+1+_perm[ Y ] ] % _grad2.size();
  int gi11 = _perm[ X+1+_perm[ Y+1 ] ] % _grad2.size();

  // Calculate noise contributions from each of the four corners
  Vec2 g1 = _grad2[gi00];
  Vec2 g2 = _grad2[gi10];
  Vec2 g3 = _grad2[gi01];
  Vec2 g4 = _grad2[gi11];
  double n00 = g1.dot( Vec2(x,y) ); // a
  double n10 = g2.dot( Vec2(x-1,y) ); // b
  double n01 = g3.dot( Vec2(x,y-1) ); // c
  double n11 = g4.dot( Vec2(x-1,y-1) ); // d

  // Compute the fade curve value for each of x, y, z
  double u = fade(x);
  double v = fade(y);

  // Interpolate along x the contributions from each of the corners
  double nx0 = mix(n00, n10, u);
  double nx1 = mix(n01, n11, u);

  // Interpolate the four results along y
  double nxy = mix(nx0, nx1, v);

  // Calculate the derivatives dn/dx and dn/dy
  float dx = (g1.getX() + (g3.getX()-g1.getX())*w.getY()) + ((g2.getY()-g1.getY())*f.getY() - g2.getX() +
             ((g1.getY()-g2.getY()-g3.getY()+g4.getY())*f.getY() + g2.getX() + g3.getY() - g4.getX() - g4.getY())*w.getY())*
             dw.getX() + ((g2.getX()-g1.getX()) + (g1.getX()-g2.getX()-g3.getX()+g4.getX())*w.getY())*dwp.getX();
  float dy = (g1.getY() + (g2.getY()-g1.getY())*w.getX()) + ((g3.getX()-g1.getX())*f.getX() - g3.getY() + ((g1.getX()-
             g2.getX()-g3.getX()+g4.getX())*f.getX() + g2.getX() + g3.getY() - g4.getX() - g4.getY())*w.getX())*dw.getY() +
             ((g3.getY()-g1.getY()) + (g1.getY()-g2.getY()-g3.getY()+g4.getY())*w.getX())*dwp.getY();

  return Vec3( nxy, dx, dy ) * 1.5;
}

// ----------------------------------------------------------------------------

void PerlinNoise::setSeed( int seed )
{
  fillP( seed, _size );
}

// ----------------------------------------------------------------------------

double PerlinNoise::operator()( const Vec2& input, double repeat ) const
{
  return this->noise( input.getX(), input.getY(), repeat );
}

// ----------------------------------------------------------------------------

double PerlinNoise::operator()( const Vec3& input, double repeat ) const
{
//  std::cout << " Perlin: Vec3\n";
  return this->noise( input );
}

// ----------------------------------------------------------------------------

double PerlinNoise::dot( const Vec3& g, double x, double y, double z ) const
{
  return g[0]*x + g[1]*y + g[2]*z;
}

// ----------------------------------------------------------------------------

double PerlinNoise::dot( const Vec3& g, double x, double y ) const
{
  return g[0]*x + g[1]*y;
}

// ----------------------------------------------------------------------------

double PerlinNoise::mix( double a, double b, double t ) const
{
  return (1.0-t)*a + t*b;
}

// ----------------------------------------------------------------------------

double PerlinNoise::fade( double t ) const
{
  return t*t*t*(t*(t*6-15)+10);
}

// ----------------------------------------------------------------------------

void PerlinNoise::fillP( int seed, unsigned int size )
{
//  std::srand( seed );
  std::uniform_int_distribution<int> dist( 0, size-1 );
//  std::default_random_engine randomEngine( seed );
  std::mt19937 randomEngine( seed );

  _p = std::vector<int>( size, 0 );
//  std::iota( _p.begin(), _p.end(), 0 );
//  std::random_shuffle( _p.begin(), _p.end() );

  for( int i = 0; i < size; ++i )
  {
    _p[i] = dist(randomEngine);
  }
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

//OctavePerlin::OctavePerlin( int seed, int octaves, double persistence )
//  : _perlin( seed ), _octaves(octaves), _persistence(persistence)
//{
//}

// ----------------------------------------------------------------------------

//double OctavePerlin::operator()( double x, double y ) const
//{
//  double total = 0;
//  double frequency = 1;
//  double amplitude = 1;
//  double maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
//  for( int i = 0; i < _octaves; ++i )
//  {
//    total += _perlin.noise( x*frequency, y*frequency ) * amplitude;

//    maxValue += amplitude;

//    amplitude *= _persistence;
//    frequency *= 2;
//  }

//  return total/maxValue;
//}

// ----------------------------------------------------------------------------

//double OctavePerlin::operator()( const Vec2& pos ) const
//{
//  return this->operator()( pos.getX(), pos.getY() );
//}

//// ----------------------------------------------------------------------------

//double OctavePerlin::operator()( double x, double y, double z ) const
//{
//  double total = 0;
//  double frequency = 1;
//  double amplitude = 1;
//  double maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
//  for( int i=0; i < _octaves; i++ )
//  {
//    total += _perlin.noise( x*frequency, y*frequency, z*frequency ) * amplitude;

//    maxValue += amplitude;

//    amplitude *= _persistence;
//    frequency *= 2;
//  }

//  return total/maxValue;
//}

// ----------------------------------------------------------------------------

