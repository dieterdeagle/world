#ifndef PerlinNoise_h
#define PerlinNoise_h

#include <vector>

#include "util/Vec2.h"
#include "util/Vec3.h"

// ----------------------------------------------------------------------------

/// impl from http://webstaff.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
class PerlinNoise
{
public:
  PerlinNoise( int seed = -1, unsigned int size = 256 );
  ~PerlinNoise();

  double noise( double x, double y, double repeat = 0 ) const;
  /// TODO: impl repeat
  double noise( double x, double y, double z, double repeat ) const;
  double noise( const Vec3& xyz ) const;

  Vec3 perlinNoiseDerivative( const Vec2& pos, double seed = 1.0 ) const;

  void setSeed( int seed );

  double operator()( const Vec2& input, double repeat = 0 ) const;
  double operator()( const Vec3& input, double repeat = 0 ) const;

private:
  std::vector<Vec2> _grad2;
  std::vector<Vec3> _grad3;

  double dot( const Vec3& g, double x, double y, double z ) const;
  double dot( const Vec3& g, double x, double y ) const;
  double mix( double a, double b, double t ) const;

  double fade( double t ) const;

  void fillP( int seed, unsigned int size );

  std::vector<int> _p;
  std::vector<int> _perm;

//  unsigned int _repeat;
  double _repeat;

  unsigned int _size = 256;
};

// ----------------------------------------------------------------------------

class OctavePerlin
{
public:
  OctavePerlin( int seed, int octaves, double persistence );

  double operator()( double x, double y ) const;
  double operator()( const Vec2& pos ) const;
  double operator()( double x, double y, double z ) const;

private:
  PerlinNoise _perlin;
  int _octaves;
  double _persistence;
};

#endif
