#ifndef util_noise_swissTurbulence_h
#define util_noise_swissTurbulence_h

#include <math.h>
#include <functional>

#include "util/noise/PerlinNoise.h"
#include "util/Vec2.h"
#include "util/Vec3.h"

// http://www.decarpentier.nl/scape-procedural-extensions
template <typename T>
class SwissTurbulenceFunctor
{
public:
  SwissTurbulenceFunctor( const PerlinNoise& perlin,
              int octaves = 8,
              double w = 0.5,
              double s = 2.0 )
    : _perlin(perlin), _octaves(octaves), _w(w), _s(s)
  {
  }

  double saturate( double x ) const
  {
    if( x < 0.0 )
      return 0.0;
    else if( x > 1.0 )
      return 1.0;
    return x;
  }

  double operator()( const T& pos ) const
  {
    double sum = 0.0;
    double freq = 1.0;
    double amp = 1.0;
    double lacunarity = 2.0;
    double gain = 0.5;
    double warp = 0.15;
    Vec2 dSum( 0.0, 0.0 );
    for( int i = 0; i < _octaves; ++i )
    {
      Vec3 n = _perlin.perlinNoiseDerivative((pos + warp * dSum)*freq/*, seed + i*/);
      sum += amp * (1 - std::abs(n.getX()));
      dSum += amp * Vec2(n.getY(),n.getZ()) * -n.getX();
      freq *= lacunarity;
      amp *= gain * saturate(sum);
    }
    return sum;
  }

private:
  double ridgedNoise( const T& pos ) const
  {
    return 1.0 - std::abs( _noise(pos) );
  }

//  std::function<double(const T&)> _noise;
  PerlinNoise _perlin;

  int _octaves;
  double _w;
  double _s;
};

#endif
