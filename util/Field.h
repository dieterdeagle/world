#ifndef util_Field_h
#define util_Field_h

#include <vector>

#include "util/Vec2.h"


namespace FieldDetail
{

std::pair<unsigned int, unsigned int> getUpperAndLowerId( double value,
                                                          const std::vector<double>& data );
}


template <typename T>
class Field
{
public:
  Field(){}

  template <typename ITER>
  Field( ITER dBegin, ITER dEnd,
         const std::vector<double>& coordsX,
         const std::vector<double>& coordsY )
    : _coordsX(coordsX), _coordsY(coordsY)
  {
    _data = std::vector<T>( dBegin, dEnd );
  }

  // -----------------------------------

  T getDatumAt( unsigned int idX, unsigned int idY ) const
  {
    return _data[ idX + idY * _coordsX.size() ];
  }

  // -----------------------------------

  T interpolate( const Vec2& pos ) const
  {
    // TODO: Repeat
//    if( pos[0] < _coordsX.front() || pos[0] > _coordsX.back() || pos[1] < _coordsY.front() || pos[1] > _coordsY.back() )
//      return _borderTensor;

    auto ids = this->findIdsOfSurroundingDataPts( pos );

    auto lbX = _coordsX[ ids[0] ];
    auto ubX = _coordsX[ ids[1] ];

    auto lbY = _coordsY[ ids[2] ];
    auto ubY = _coordsY[ ids[3] ];

    auto distX = ubX - lbX;
    auto distY = ubY - lbY;

    auto fracLbX = 1.0 - ( (pos[0] - lbX) / distX );
    auto fracUbX = 1.0 - ( (ubX - pos[0]) / distX );
    auto fracLbY = 1.0 - ( (pos[1] - lbY) / distY );
    auto fracUbY = 1.0 - ( (ubY - pos[1]) / distY );

    auto lowerLeftTensor = getDatumAt( ids[0], ids[2] );
    auto lowerRightTensor = getDatumAt( ids[1], ids[2] );
    auto upperLeftTensor = getDatumAt( ids[0], ids[3] );
    auto upperRightTensor = getDatumAt( ids[1], ids[3] );

    auto interpolLowerX = fracLbX * lowerLeftTensor + fracUbX * lowerRightTensor;
    auto interpolUpperX = fracLbX * upperLeftTensor + fracUbX * upperRightTensor;

    return fracLbY * interpolLowerX + fracUbY * interpolUpperX;
  }

  // -----------------------------------

private:
  std::vector<unsigned int> findIdsOfSurroundingDataPts( const Vec2& pos ) const
  {
    auto boundsX = FieldDetail::getUpperAndLowerId( pos[0], _coordsX );
    auto boundsY = FieldDetail::getUpperAndLowerId( pos[1], _coordsY );

    return std::vector<unsigned int>{ boundsX.first, boundsX.second, boundsY.first, boundsY.second };
  }

private:
  std::vector<T> _data;

  std::vector<double> _coordsX;
  std::vector<double> _coordsY;
};

#endif
