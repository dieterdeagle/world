#include "testsUtil.h"

#include "mathStuff.h"

// ----------------------------------------------------------------------------

bool UtilTests::matrixTests()
{
  std::vector<float> testData0{ 0.f, 1.f, 2.f,
                                3.f, 4.f, 5.f,
                                6.f, 7.f, 8.f };
  Matrix<float> testMatrix( testData0 );
  testMatrix.print();
  testMatrix( 1,1 ) = 12.f;
  testMatrix.print();

  std::vector<float> identityData{ 1.f, 0.f, 0.f,
                               0.f, 1.f, 0.f,
                               0.f, 0.f, 1.f };
  Matrix<float> identity( identityData );

  std::cout << "id:\n";
  (identity*testMatrix).print();
  std::cout << "id:\n";
  (testMatrix*identity).print();

  std::vector<float> amplifyData{ 2.f, 0.f, 0.f,
                                  0.f, 2.f, 0.f,
                                  0.f, 0.f, 2.f };
  Matrix<float> amplify( amplifyData );
  std::cout << "doubled:\n";
  (amplify*testMatrix).print();

  Matrix<float,3,2> lhs( std::vector<float>{  2.f,  3.f,  4.f,
                                             20.f, 30.f, 40.f } );
  Matrix<float,2,3> rhs( std::vector<float>{  5.f,  10.f,
                                             50.f, 100.f,
                                            500.f, 1000.f } );
  (lhs*rhs).print();

  return true;
}

// ----------------------------------------------------------------------------

bool UtilTests::rotationTests()
{
  std::cout << "rotation tests\n";
  Vec3 testVec0( 0.f, 1.f, 0.f );
  std::cout << testVec0 << "\n";
  std::cout << "(90,0,0): " << rotate( testVec0, 90.f, 0.f, 0.f ) << "\n";
  std::cout << "(0,90,0): " << rotate( testVec0, 0.f, 90.f, 0.f ) << "\n";
  std::cout << "(0,0,90): " << rotate( testVec0, 0.f, 0.f, 90.f ) << "\n";

  Vec3 testVec1( 1.f, 1.f, 0.f );
  std::cout << testVec1 << "\n";
  std::cout << "(90,0,0): " << rotate( testVec1, 90.f, 0.f, 0.f ) << "\n";
  std::cout << "(0,90,0): " << rotate( testVec1, 0.f, 90.f, 0.f ) << "\n";
  std::cout << "(0,0,90): " << rotate( testVec1, 0.f, 0.f, 90.f ) << "\n";

  return true;
}

// ----------------------------------------------------------------------------
