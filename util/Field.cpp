#include "Field.h"

#include <algorithm>

// ----------------------------------------------------------------------------

std::pair<unsigned int, unsigned int> FieldDetail::getUpperAndLowerId( double value,
                                                                  const std::vector<double>& data )
{
  auto lb = std::lower_bound( data.begin(), data.end(), value );
  auto ub = std::upper_bound( data.begin(), data.end(), value );

  if( lb == ub )
  {
    auto newLb = lb - 1;
    return std::make_pair( newLb-data.begin(), ub-data.begin() );
  }
  else if( ub == data.end() )
  {
    auto newLb = lb - 1;
    auto newUb = lb;
    return std::make_pair( newLb-data.begin(), newUb-data.begin() );
  }

  return std::make_pair( lb-data.begin(), ub-data.begin() );
}
