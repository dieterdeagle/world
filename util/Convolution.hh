#ifndef util_Convolution_hh
#define util_Convolution_hh

#include <iostream>

#include "Array2d.hh"

template <typename T>
Array2d<T> convolution( const Array2d<T>& input, const Array2d<T>& kernel )
{
  auto borderSize = std::min( kernel.width(), kernel.height() ) / 2;

  std::vector<T> convolutedData( input.width()*input.height(), 0 );

  // ignore border
  for( std::size_t j = borderSize; j < input.height()-borderSize; ++j )
  {
    for( std::size_t i = borderSize; i < input.width()-borderSize; ++i )
    {
      T sum = 0;
      // go through kernel
      for( int kJ = -static_cast<int>(borderSize); kJ <= static_cast<int>(borderSize); ++kJ )
      {
        for( int kI = -static_cast<int>(borderSize); kI <= static_cast<int>(borderSize); ++kI )
        {
          sum += input( i+kI, j+kJ ) * kernel( borderSize + kI, borderSize + kJ );
        }
      }
      convolutedData[ i + input.width() * j ] = sum;
    }
  }

  return Array2d<T>( input.width(), input.height(), convolutedData );
}

// --------------------------------

template <typename T>
Array2d<T> convolution3x3( const Array2d<T>& input, const Array2d<T>& kernel )
{
  auto borderSize = 1;

  std::vector<T> convolutedData( input.width()*input.height(), 0 );

  // ignore border
  for( std::size_t j = borderSize; j < input.height()-borderSize; ++j )
  {
    for( std::size_t i = borderSize; i < input.width()-borderSize; ++i )
    {
      T sum = 0;
      // go through kernel
      sum += input(i-1,j-1) * kernel(0,0);
      sum += input(i,j-1) * kernel(1,0);
      sum += input(i+1,j-1) * kernel(2,0);

      sum += input(i-1,j) * kernel(0,1);
      sum += input(i,j) * kernel(1,1);
      sum += input(i+1,j) * kernel(2,1);

      sum += input(i-1,j+1) * kernel(0,2);
      sum += input(i,j+1) * kernel(1,2);
      sum += input(i+1,j+1) * kernel(2,2);

      convolutedData[ i + input.width() * j ] = sum;
    }
  }

  return Array2d<T>( input.width(), input.height(), convolutedData );
}

// --------------------------------

/**
 * @param kernel1: first kernel _applied_!
 */
template <typename T>
Array2d<T> convolution3x3Seperated( const Array2d<T>& input, const Array2d<T>& kernel1, const Array2d<T>& kernel2 )
{
  auto borderSize = 1;

  std::vector<T> convolutedData( input.width()*input.height(), 0 );

  std::cout << "kernel1:\n" << kernel1;
  std::cout << "kernel2:\n" << kernel2;

  // ignore border
  for( std::size_t j = 0; j < input.height()-0; ++j )
  {
    for( std::size_t i = borderSize; i < input.width()-borderSize; ++i )
    {
      T sum = 0;
      // go through kernel
      sum += input(i-1,j) * kernel1(0,0);
      sum += input(i+0,j) * kernel1(1,0);
      sum += input(i+1,j) * kernel1(2,0);

      convolutedData[ i + input.width() * j ] = sum;
    }
  }

  std::vector<T> resultData( input.width()*input.height(), 0 );
  for( std::size_t j = borderSize; j < input.height()-borderSize; ++j )
  {
    for( std::size_t i = 0; i < input.width(); ++i )
    {
      T sum = 0;
      // go through kernel
      sum += convolutedData[i + input.width() * (j-1) ] * kernel2(0,0);
      sum += convolutedData[i + input.width() * (j+0) ] * kernel2(0,1);
      sum += convolutedData[i + input.width() * (j+1) ] * kernel2(0,2);

      std::cout << " sum: " << sum << ". input: " << input(i,j) << "\n";
      resultData[ i + input.width() * j ] = sum - 2* input(i,j); // - 2*input is a hack to achieve -1 in the middle of the kernel
    }
  }

  return Array2d<T>( input.width(), input.height(), resultData );
}

// --------------------------------

#endif
