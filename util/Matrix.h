#ifndef util_Matrix_h
#define util_Matrix_h

#include <iostream>
#include <vector>

#include "Vec3.h"

template <typename T, int N = 3, int M = 3>
class Matrix
{
public:
  // ------------
  Matrix( const std::vector<T>& data )
  {
    _data = data;
  }

  Matrix( const Matrix& rhs )
  {
    _data = rhs._data;
  }

  // ------------
  T& operator()( unsigned int i, unsigned int j )
  {
    return _data[ i + j*N ];
  }

  // ------------
  const T& operator()( unsigned int i, unsigned int j ) const
  {
    return _data[ i + j*N ];
  }

  // ------------
  void print() const
  {
    for( unsigned int j = 0; j < M; ++j )
    {
      for( unsigned int i = 0; i < N; ++i )
      {
        std::cout << this->operator()(i,j) << " ";
      }
      std::cout << "\n";
    }
  }

private:
  std::vector<T> _data;
};

template <typename T, int N, int M>
Matrix<T,M,M> operator*( const Matrix<T,N,M>& lhs, const Matrix<T,M,N>& rhs )
{
  std::vector<T> data;
  for( unsigned int j = 0; j < M; ++j )
  {
    for( unsigned int i = 0; i < M; ++i )
    {
      T sum = 0;
      for( unsigned int k = 0; k < N; ++k )
      {
        sum += lhs( k,j ) * rhs( i,k );
      }
      data.push_back( sum );
    }
  }
  return Matrix<T,M,M>( data );
}

// ----------------
template <typename T>
Vec3 operator*( const Matrix<T,3,3>& m, const Vec3& vec )
{
  float x = m(0,0)*vec.getX() + m(1,0)*vec.getY() +m(2,0)*vec.getZ();
  float y = m(0,1)*vec.getX() + m(1,1)*vec.getY() +m(2,1)*vec.getZ();
  float z = m(0,2)*vec.getX() + m(1,2)*vec.getY() +m(2,2)*vec.getZ();

  return Vec3( x,y,z );
}

#endif
