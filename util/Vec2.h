#ifndef util_Vec2_h
#define util_Vec2_h

#include <ostream>
#include <vector>

#include <osg/Vec2d>
#include <osg/Vec2f>

class Vec2
{
public:

  using ValueType = double;

  Vec2( ValueType x, ValueType y );
  Vec2( const Vec2& rhs);
  explicit Vec2( const osg::Vec2d& osgVec2);
  explicit Vec2( const osg::Vec2f& osgVec2);

  ~Vec2();

  ValueType& operator[]( unsigned int i );
  ValueType operator[]( unsigned int i ) const;

  Vec2& operator+=( const Vec2& rhs );
  Vec2& operator+=( ValueType rhs );
  Vec2& operator-=( ValueType rhs );
  Vec2& operator-=( const Vec2& rhs );
  Vec2& operator/=( const Vec2& rhs );
  Vec2& operator/=( ValueType rhs );
  Vec2& operator*=( ValueType rhs );
  Vec2& operator*=( const Vec2& rhs );

  ValueType getX() const { return this->x(); }
  ValueType getY() const { return this->y(); }
  ValueType x() const { return _values[0]; }
  ValueType y() const { return _values[1]; }

  std::vector<ValueType> getValues() const { return _values; }

  explicit operator osg::Vec2f() const;

  ValueType distanceEuclidean( const Vec2& rhs ) const;

  Vec2 normalized() const;

  ValueType length() const;

  ValueType dot( const Vec2& rhs ) const;

  Vec2 getOrtho() { return Vec2( -this->y(), this->x() ); }

private:
  std::vector<ValueType> _values;
};

const Vec2 operator+( const Vec2& lhs, const Vec2& rhs );
const Vec2 operator-( const Vec2& lhs, const Vec2& rhs );
const Vec2 operator*( const Vec2& lhs, const Vec2& rhs );
const Vec2 operator/( const Vec2& lhs, const Vec2& rhs );
const Vec2 operator/( const Vec2& lhs, double rhs );
const Vec2 operator*( const Vec2& lhs, double rhs );
const Vec2 operator*( double lhs, const Vec2& rhs );
const Vec2 operator+( const Vec2& lhs, double rhs );
const Vec2 operator+( double lhs, const Vec2& rhs );
const Vec2 operator-( const Vec2& lhs, double rhs );
const Vec2 operator-( double lhs, const Vec2& rhs );

std::ostream& operator<<( std::ostream& stream, const Vec2& rhs );

/**
 * @brief floor floors vec components
 */
Vec2 floor( const Vec2& in );
Vec2 ceil( const Vec2& in );

#endif
