#ifndef Randomizer_h
#define Randomizer_h

#include <random>
#include <ctime>

class Randomizer
{

public:
  Randomizer(){ std::srand( std::time(0) ); }
  Randomizer( int seed ){ setSeed( seed ); }

  void setSeed( int seed ){ std::srand( seed ); }

  float randomize( float value, float maxDev );

  /**
   * @brief randomDecision
   * @param probability should be in [0,1]
   * @returns true if random number is smaller than probability
   */
  bool randomDecision( float probability );

  /**
   * @brief getRandom
   * @return random number in [0,1]
   */
  float getRandom();
};



#endif
