#ifndef util_Array2d_hh
#define util_Array2d_hh

#include <iomanip>
#include <ostream>
#include <vector>

template <typename T = double>
class Array2d
{
public:
  Array2d( std::size_t width,
           std::size_t height,
           const std::vector<T>& data)
    : _width(width), _height(height), _data(data)
  {
  }

  // ----------------------
  T& operator()( std::size_t i, std::size_t j )
  {
    return _data[ i + _width*j ];
  }

  // ----------------------
  const T& operator()( std::size_t i, std::size_t j ) const
  {
    return _data[ i + _width*j ];
  }

  // ----------------------
  std::size_t width() const { return _width; }
  std::size_t height() const { return _height; }

private:
  std::vector<T> _data;

  std::size_t _width;
  std::size_t _height;
};

// -------------------

template <typename T>
std::ostream& operator<<( std::ostream& o, const Array2d<T>& arr )
{
  for( std::size_t j = 0; j < arr.height(); ++j )
  {
    for( std::size_t i = 0; i < arr.width(); ++i )
    {
      o << std::setprecision(4) << std::setw(8) << arr(i,j) << " ";
    }
    o << "\n";
  }

  return o;
}

#endif
