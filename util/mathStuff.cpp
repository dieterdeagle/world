#include "mathStuff.h"

#include "Matrix.h"

// ----------------------------------------------------------------------------

Vec3 computeNormalTriangle( const Vec3& A, const Vec3& B, const Vec3& C )
{
  Vec3 U = B - A;
  Vec3 V = C - A;

  float Nx = U.getY()*V.getZ() - U.getZ()*V.getY();
  float Ny = U.getZ()*V.getX() - U.getX()*V.getZ();
  float Nz = U.getX()*V.getY() - U.getY()*V.getX();

  return Vec3( Nx, Ny, Nz );
}

// ----------------------------------------------------------------------------

Vec3 rotate( const Vec3& input, float alpha, float beta, float gamma )
{
  Matrix<double,3,3> rotX( std::vector<double>{ 1.0,                 0.0,                  0.0,
                                              0.f, cos(alpha*PI/180.0), -sin(alpha*PI/180.0),
                                              0.f, sin(alpha*PI/180.0),  cos(alpha*PI/180.0) } );

  Matrix<double,3,3> rotY( std::vector<double>{ cos(beta*PI/180.0), 0.0, sin(beta*PI/180.0),
                                                             0.0, 1.0,                0.0,
                                             -sin(beta*PI/180.0), 0.0, cos(beta*PI/180.0) } );

  Matrix<double,3,3> rotZ( std::vector<double>{ cos(gamma*PI/180.0), -sin(gamma*PI/180.0), 0.0,
                                              sin(gamma*PI/180.0),  cos(gamma*PI/180.0), 0.0,
                                                              0.0,                  0.0, 1.0 } );

  return rotZ * ( rotY * (rotX * input) );
}

// ----------------------------------------------------------------------------

Vec2 rotate( const Vec2& in, double degree )
{
  degree = degree * PI / 180;
//  auto trans = in - center;
  Vec2 out( in.x() * std::cos(degree) - in.y() * std::sin(degree),
            in.y() * std::cos(degree) + in.x() * std::sin(degree) );

//  out += center;
  return out;
}

// ----------------------------------------------------------------------------
