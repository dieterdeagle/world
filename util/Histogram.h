#ifndef util_Histogram_h
#define util_Histogram_h

#include <algorithm>
#include <iostream>
#include <ostream>
#include <vector>

template <typename T>
class Histogram
{
public:

  Histogram( std::size_t nbBins )
  {
    _bins = std::vector<unsigned int>( nbBins, 0 );
  }

  // -------------

  void insert( std::vector<T> data )
  {
//    std::cout << "Histogram.\n";

//    std::cout << "bins size: " << _bins.size() << "\n";

    _min = *std::min_element( data.begin(), data.end() ) - std::numeric_limits<T>::epsilon();
    _max = *std::max_element( data.begin(), data.end() ) + std::numeric_limits<T>::epsilon();

//    std::cout << "  | Interval: [" << _min << "," << _max << "]\n";

    _range = _max - _min;
    _stepsize = _range / _bins.size() + std::numeric_limits<T>::epsilon();

//    std::cout << "  | range: " << _range << ". stepsize: " << _stepsize << "\n";

    for( const auto& d : data )
    {
//      std::cout << "  |   d: " << d << "\n";
//      std::cout << "  |    d - min: " << d-_min << "\n";
      unsigned int id = std::floor( (d-_min)  / _stepsize );
//      std::cout << "  |   id: " << id << " / " << _bins.size() << "\n";
//      std::cout << "  |   _bins[id]: " << _bins[id] << "\n";
      _bins[ id ] += 1;
//      std::cout << "  |    _bins[id]: " << _bins[id] << "\n";
    }
  }

  // -------------

  std::vector<unsigned int> getBins() const
  {
    return _bins;
  }

  // -------------

  unsigned int getBinsize( std::size_t id ) const
  {
    return _bins[id];
  }

  // -------------

  std::size_t getNbBins() const
  {
    return _bins.size();
  }

  // -------------

  std::pair<T,T> getBinInterval( std::size_t id ) const
  {
    auto min = _min + id * _stepsize;
    auto max = _min + (id+1) * _stepsize;

    return std::make_pair( min, max );
  }

private:

  T _min;
  T _max;
  T _range;

  T _stepsize;

  std::vector<unsigned int> _bins;
};

// --------------

template <typename T>
std::ostream& operator<<( std::ostream& o, const Histogram<T>& histo )
{
  auto nbBins = histo.getNbBins();

  std::cout << "Histogram.\n";
  for( std::size_t i = 0; i < nbBins; ++i )
  {
    auto binSize = histo.getBinsize( i );
    auto interval = histo.getBinInterval( i );

    o << " [" << interval.first << "," << interval.second << "]: " << binSize << "\n";
  }

  return o;
}

#endif
