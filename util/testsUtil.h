#ifndef util_testsUtil_h
#define util_testsUtil_h

#include "Matrix.h"
#include "Vec3.h"

namespace UtilTests
{

bool rotationTests();

bool matrixTests();

} // end of namespace UtilTests

#endif
