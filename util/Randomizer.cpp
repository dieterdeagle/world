#include "Randomizer.h"

#include <iostream>

float Randomizer::randomize( float value, float maxDev )
{
  float rand = getRandom();

  return value + (rand*2.0 - 1.0)*maxDev;
}

//-----------------------------------------------------------------------------

bool Randomizer::randomDecision( float probability )
{
  float rand = getRandom();
  bool decision = rand < probability;
//  std::cout << rand << "<" << probability << " = " << decision << std::endl;
  return( decision );
}

//-----------------------------------------------------------------------------

float Randomizer::getRandom()
{
  return std::rand() / static_cast<float>( RAND_MAX );
}

//-----------------------------------------------------------------------------
