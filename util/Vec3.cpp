#include "Vec3.h"

#include <cmath>
#include <iostream>
#include <limits>

// ----------------------------------------------------------------------------

Vec3::Vec3( ValueType x, ValueType y, ValueType z )
{
  _values = std::vector<Vec3::ValueType>{ x, y, z };
}

// ----------------------------------------------------------------------------

Vec3::Vec3( const Vec3& rhs )
{
  _values = rhs._values;
}

// ----------------------------------------------------------------------------

Vec3::Vec3( const Vec2& rhsXY, ValueType rhsZ )
{
  _values = std::vector<Vec3::ValueType>{ rhsXY.getX(), rhsXY.getY(), rhsZ };
}

// ----------------------------------------------------------------------------

Vec3::Vec3( const osg::Vec3f& osgVec3 )
{
  _values = std::vector<Vec3::ValueType>{ osgVec3.x(), osgVec3.y(), osgVec3.z() };
}

// ----------------------------------------------------------------------------

Vec3::~Vec3()
{

}

// ----------------------------------------------------------------------------

Vec3::ValueType& Vec3::operator[]( unsigned int i )
{
  return _values[i];
}

// ----------------------------------------------------------------------------

Vec3::ValueType Vec3::operator[]( unsigned int i ) const
{
  return _values[i];
}

// ----------------------------------------------------------------------------

Vec3& Vec3::operator+=( const Vec3& rhs )
{
  _values[0] += rhs.getX();
  _values[1] += rhs.getY();
  _values[2] += rhs.getZ();

  return *this;
}

// ----------------------------------------------------------------------------

Vec3& Vec3::operator-=( const Vec3& rhs )
{
  _values[0] -= rhs.getX();
  _values[1] -= rhs.getY();
  _values[2] -= rhs.getZ();

  return *this;
}

// ----------------------------------------------------------------------------

Vec3& Vec3::operator/=( Vec3::ValueType rhs )
{
  _values[0] /= rhs;
  _values[1] /= rhs;
  _values[2] /= rhs;

  return *this;
}

// ----------------------------------------------------------------------------

Vec3& Vec3::operator*=( Vec3::ValueType rhs )
{
  _values[0] *= rhs;
  _values[1] *= rhs;
  _values[2] *= rhs;

  return *this;
}

// ----------------------------------------------------------------------------

Vec3& Vec3::operator=( const Vec3& rhs )
{
 _values = rhs._values;

 return *this;
}

// ----------------------------------------------------------------------------

Vec3::operator osg::Vec3f() const
{
  return osg::Vec3f( this->x(), this->y(), this->z() );
}

// ----------------------------------------------------------------------------

Vec3::ValueType Vec3::distanceEuclidean( const Vec3& rhs ) const
{
  Vec3 tmp = *this - rhs;

  return tmp.length();
}

// ----------------------------------------------------------------------------

Vec3 Vec3::normalized() const
{
  if( this->length() < std::numeric_limits<Vec3::ValueType>::epsilon() )
    return *this;

  Vec3 tmp = *this / this->length();

  return tmp;
}

// ----------------------------------------------------------------------------

Vec3::ValueType Vec3::length() const
{
  Vec3::ValueType summedQuadraticDistance =
        ( this->getX() * this->getX() )
      + ( this->getY() * this->getY() )
      + ( this->getZ() * this->getZ() );

  return std::sqrt( summedQuadraticDistance );
}

// ----------------------------------------------------------------------------

Vec3::ValueType Vec3::dot( const Vec3& rhs ) const
{
  return this->getX()*rhs.getX() + this->getY()*rhs.getY() + this->getZ()*rhs.getZ();
}

// ----------------------------------------------------------------------------

Vec3 Vec3::cross( const Vec3& rhs ) const
{
 return Vec3( this->getY()*rhs.getZ() - this->getZ()*rhs.getY(),
              this->getZ()*rhs.getX() - this->getX()*rhs.getZ(),
              this->getX()*rhs.getY() - this->getY()*rhs.getX() );
}

// ----------------------------------------------------------------------------

Vec3 Vec3::findPerpendicularVector() const
{
  Vec3 C( 0,1,0 );
  if( std::abs(this->y()) > 0.0 || std::abs(this->z()) > 0.0 )
  {
    C = Vec3(1,0,0);
  }
  return this->cross( C ).normalized();
}

// ----------------------------------------------------------------------------

/// ---------------------------------------------------------------------------
/// ---------------------------------------------------------------------------

// ----------------------------------------------------------------------------

const Vec3 operator+( const Vec3& lhs, const Vec3& rhs )
{
  Vec3 tmp( lhs );
  return tmp += rhs;
}

// ----------------------------------------------------------------------------

const Vec3 operator-( const Vec3& lhs, const Vec3& rhs )
{
  Vec3 tmp( lhs );
  return tmp -= rhs;
}

// ----------------------------------------------------------------------------

const Vec3 operator/(const Vec3& lhs, Vec3::ValueType rhs)
{
  Vec3 tmp( lhs );
  return tmp /= rhs;
}

// ----------------------------------------------------------------------------

const Vec3 operator*(const Vec3& lhs, Vec3::ValueType rhs)
{
  Vec3 tmp( lhs );
  return tmp *= rhs;
}

// ----------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& stream, const Vec3& rhs)
{
  return stream << "(" << rhs.getX() << "," << rhs.getY() << "," << rhs.getZ() << ")";
}

// ----------------------------------------------------------------------------

const Vec3 operator*( Vec3::ValueType lhs, const Vec3& rhs )
{
  return rhs * lhs;
}

// ----------------------------------------------------------------------------
