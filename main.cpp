
#include <osg/Geometry>
#include <osg/Light>
#include <osg/Matrixd>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/Texture2D>
#include <osg/Uniform>

#include <osgDB/ReadFile>

#include <osgGA/TrackballManipulator>
#include <osgGA/TerrainManipulator>
#include <osgGA/UFOManipulator>
#include <osgGA/FlightManipulator>

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include <iostream>
#include <functional>
#include <limits>
#include <random>
#include <time.h>


//#include "SphericalTerrain/DisplacementCube.h"
#include "SphericalTerrain/RenderUtil.h"
#include "SphericalTerrain/HeightFunctions.h"
//#include "SphericalTerrain/SphericalTerrainGenerator.h"

#include "TreeGen/Tree.h"
#include "TreeGen/TreeGenerator.h"
#include "graphics/RenderUtil.h"

#include "util/noise/FractionalBrownianMotion.h"
#include "util/Vec3.h"

// ----------------------------------------------------------------------------

bool simpleTests()
{
  float EPS = std::numeric_limits<float>::epsilon();

  Vec3 A( 0.f,0.f,0.f );
  Vec3 B( 0.f,1.f,0.f );
  Vec3 C( 5.f,-10.f,10.f );

  if( std::abs( A.distanceEuclidean(B) - 1.f ) > EPS )
    throw( std::string("Eucl dist error!") );
  if( std::abs( A.distanceEuclidean(A) ) > EPS )
    throw( std::string("Eucl dist error!") );
  if( std::abs( A.distanceEuclidean(C) - 15.f) > EPS )
    throw( std::string("Eucl dist error!") );

  Vec3 D = B.normalized();
  float sum = D.getX() + D.getY() + D.getZ();
  if( std::abs( sum - 1.f ) > EPS )
    throw( std::string("Normalizing error!") );

  return true;
}

// ----------------------------------------------------------------------------

static unsigned int seed = 1;
void testSRand( int newseed )
{
    seed = (unsigned)newseed & 0x7fffffffU;
}

int testRand()
{
    seed = (seed * 1103515245U + 12345U) & 0x7fffffffU;
    return (int)seed;
}

int testMapping( int seed, int init )
{
  return ((init+seed) * 1103515245U + 12345U) & 0x7fffffffU;
}

// ----------------------------------------------------------------------------

int main()
{

  try
  {
    if( simpleTests() )
    {
      std::cout << "simple tests run succesfully." << std::endl;
    }
  }
  catch( std::string e )
  {
    std::cout << "An error occurred while running simpleTests: " << e << std::endl;
    return -1;
  }


  osgViewer::Viewer viewer;
  viewer.setUpViewInWindow( 100, 100, 1024, 512 );
  viewer.getCamera()->setClearColor( osg::Vec4(0.0,0.0,0.0,0.0) );
  std::vector<osgViewer::View*> views;
  viewer.getViews(views);
  for( auto view : views )
  {
    view->addEventHandler( new osgViewer::StatsHandler );
  }



  testSRand( 4 );
  for( int i = 0; i < 20; ++i )
  {
    std::cout << testMapping(i, 10) / static_cast<double>(RAND_MAX) << std::endl;
  }

  osg::ref_ptr<osg::Group> root( new osg::Group );

//  SphericalTerrainGenerator terrainGenerator;

//  SphericalTerrain terrain = terrainGenerator.generateSphere( Vec3( 0.f,0.f,0.f), 2000.f, 7, seed );
//  PerlinNoise noise( seed );
//  auto heightFunctor = std::bind( fbm, std::placeholders::_1, 8, noise, 0.5, 2.0 );
//  SphericalTerrain terrain( Vec3(0,0,0), 2000.f, heightFunctor );

//  osg::ref_ptr<osg::Geode> terrainGeode = getSphericalTerrainGeode( terrain, 2 );
//  root->addChild( terrainGeode );


  int seed = 123972324;
  PerlinNoise noise( seed );
  FbmFunctor<Vec3> fbmFunctor( noise, 8, 1.0, 0.5, 2.0 );

  SphericalTerrain terrain( Vec3(0,0,0), 2000.f, 0.3, fbmFunctor );
  osg::ref_ptr<osg::Geode> terrainGeode = getSphericalTerrainGeode( terrain, 6 );
  root->addChild( terrainGeode );

  std::cout << "Perlin\n";
  std::cout << noise( Vec3(0.1,0.0,0.0) ) << "," << noise( Vec3(2.1,0.0,0.0) ) << "," << noise( Vec3(10.1,0.0,0.0) ) << std::endl;
  std::cout << noise( Vec3(0.5,0.4,0.3) ) << "," << noise( Vec3(0.5,0.5,0.5) ) << "," << noise( Vec3(10.0,0.0,0.0) ) << std::endl;


  root->getOrCreateStateSet()->setMode( GL_LIGHTING, osg::StateAttribute::OFF );

  viewer.setSceneData( root );

  viewer.run();

  return 0;
}
