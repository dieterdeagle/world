#ifndef Tree_h
#define Tree_h

#include <iostream>
#include <memory>
#include <vector>

//#include <osg/Vec3f> // FIXME: substitute

#include <osg/Image>

#include "util/Vec3.h"

//typedef std::vector<float,3> vec3f;
//typedef Vec3 vec3f;
using vec3f = Vec3;
//typedef osg::Vec3f vec3f;
typedef unsigned int uint;

// ------------------

struct BranchSegmentAttributesPre
{
  BranchSegmentAttributesPre( const Vec3& startPos, float depthInBranch, float depthOfBranch )
    : _startPos(startPos), _depthInBranch(depthInBranch), _depthOfBranch(depthOfBranch)
  {}

  Vec3 _startPos;
//  vec3f _startOfTree; // this way we should be able to compute some interesting
                      // properties without storing them explicitly.
  float _depthInBranch; // 0.f is first, 1.f last element
  float _depthOfBranch; // depth of branch in tree generation
};

// -------------------

struct BranchSegment
{
  BranchSegment( vec3f start, vec3f end, float thickness )
    : _start(start), _end(end), _thickness(thickness)
  {}

  vec3f _start;
  vec3f _end;
  float _thickness;
};

// -------------------

struct Leaves
{
  Leaves( const std::vector<Vec3>& strip, const osg::ref_ptr<osg::Image>& img, const std::vector<Vec2>& texCoords )
    : _triangleStrip(strip), _img(img), _texCoords(texCoords)
  {}
  std::vector<Vec3> _triangleStrip;
  osg::ref_ptr<osg::Image> _img;
  std::vector<Vec2> _texCoords;
};

// -------------------

struct Branch
{
  Branch( vec3f startingPoint, vec3f direction,
          float initialThickness/*, uint nbSegments,
          float lengthOfSegments */);

  /// float verjuengungsFaktor

  vec3f _startingPoint;
  vec3f _direction;

  float _initialThickness;

  uint _nbSegments;
  float _lengthOfSegments;

  std::vector< std::shared_ptr<BranchSegment> > _segments;

  std::shared_ptr<Leaves> _leaves = nullptr;

  std::vector< std::shared_ptr<Branch> > _children;

};

// -------------------

struct Tree
{
  Tree(){}

  std::vector< std::shared_ptr<Branch> > _branches;
};


#endif
