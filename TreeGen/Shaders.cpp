#include "Shaders.h"



#include <iostream>

osg::Program* createGrassShader()
{
  std::cout << "Create grass shader.." << std::endl;
  osg::Program* pgm = new osg::Program;
  pgm->setName( "osgshader2 demo" );

  pgm->addShader( new osg::Shader( osg::Shader::VERTEX,   vertGrass ) );
  pgm->addShader( new osg::Shader( osg::Shader::FRAGMENT, fragGrass ) );
  pgm->addShader( new osg::Shader( osg::Shader::GEOMETRY, geomGrass ) );

  pgm->setParameter( GL_GEOMETRY_VERTICES_OUT_EXT, 7 );
  pgm->setParameter( GL_GEOMETRY_INPUT_TYPE_EXT, GL_POINTS );
  pgm->setParameter( GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLE_STRIP );

  return pgm;
}

//-------------------------------------------------------

osg::Program* createTreeShader()
{
  std::cout << "Create tree shader.." << std::endl;
  osg::Program* pgm = new osg::Program;
  pgm->setName( "tree shader test" );


  pgm->addShader( osg::Shader::readShaderFile( osg::Shader::VERTEX, "/home/ntr/code/cpp/TreeGen/src/shaders/TreeShader.vert" ) );
  pgm->addShader( osg::Shader::readShaderFile( osg::Shader::GEOMETRY, "/home/ntr/code/cpp/TreeGen/src/shaders/TreeShader.geom" ) );
  pgm->addShader( osg::Shader::readShaderFile( osg::Shader::FRAGMENT, "/home/ntr/code/cpp/TreeGen/src/shaders/TreeShader.frag" ) );


  pgm->setParameter( GL_GEOMETRY_VERTICES_OUT_EXT, 2*8*3 );
  pgm->setParameter( GL_GEOMETRY_INPUT_TYPE_EXT, GL_POINTS );
  pgm->setParameter( GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLES );

  return pgm;
}

//-------------------------------------------------------
