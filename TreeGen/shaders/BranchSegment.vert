#version 330

layout(location = 0) in vec3 pos;
layout(location = 1) in float thickness;
layout(location = 2) in vec3 prevDir;
//layout(location = 2) in vec3 dir;
//layout(location = 3) in vec3 prevDir;


out vec3 posW;
out float v_thickness;
out vec3 v_prevDirW;
//out vec3 dirW;
//out vec3 prevDirW;

void main()
{
  posW = pos;
  v_thickness = thickness;
  v_prevDirW = prevDir;
//  dirW = dir;
  gl_Position = vec4( pos, 1.0 );
}
