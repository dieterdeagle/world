#version 330

in vec4 posE;
in vec3 posWOut;
in vec3 pos0W;
//in vec3 normalE;
//in vec3 matColor;

out vec4 FragColor;

vec3 LightDirection = normalize( vec3(1.0,1.0,1.0) );
vec3 LightColor = vec3( 0.9, 0.8, 0.7 );
float diffuseStrength = 0.5;
float specHardness = 10.0;
float specStrength = 0.5;

//-----------------
void main()
{
  vec3 matColor = vec3( 0.6,0.2,0.1 );

  vec3 eye = normalize(-posE.xyz);

  vec3 normalE_new = normalize( gl_NormalMatrix * (posWOut-pos0W) );
  float NormalDotLight = max(0.0,dot( normalE_new,LightDirection ) );
  float intensityDiffuse = NormalDotLight;

  vec3 diffuse = intensityDiffuse * LightColor * diffuseStrength;

  vec3 halfVec = normalize(LightDirection + eye);
  float NormalDotHalf = max(0.0,dot( normalE_new, halfVec ) );
  float intensitySpec = pow( NormalDotHalf, specHardness ) * specStrength;

  float ambientIntensity = 0.2;
//  FragColor = vec4( 0.6,0.2,0.1, 1.0 );
//  FragColor = vec4( vec3(intensitySpec), 1.0 );
  FragColor = vec4( matColor * 0.2 + matColor * diffuse + LightColor * intensitySpec, 1.0 );
}
