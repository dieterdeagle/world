#version 330

layout(lines) in;
layout(triangle_strip, max_vertices = 16) out;

in vec3 posW[];
in float v_thickness[];
in vec3 v_prevDirW[];
//in vec3 dirW[];
//in vec3 prevDirW[];
//in vec3 nextDirW[];

out vec4 posE;
out vec3 posWOut;
out vec3 pos0W;
//out vec3 normalE;

vec3 findNearest( vec3 A, vec3 a, vec3 b, vec3 c, vec3 d )
{
  float distToa = length( A-a );
  float distTob = length( A-b );
  float distToc = length( A-c );
  float distTod = length( A-d );

  if( distToa <= distTob && distToa <= distToc && distToa <= distTod )
    return a;

  if( distTob <= distToa && distTob <= distToc && distTob <= distTod )
    return b;

  if( distToc <= distTob && distToc <= distToa && distToc <= distTod )
    return c;

  if( distTod <= distToa && distTod <= distTob && distTod <= distToc )
    return d;

  return a;
}

// -----------------
void EmitQuad( vec3 A, vec3 B, vec3 C, vec3 D )
{
  vec3 normal = normalize( gl_NormalMatrix * cross( B-A, C-A ) );

  posWOut = A;
  posE = gl_ModelViewMatrix * vec4(A,1.0);
//  normalE = normal;
//  normalE = normalize( gl_NormalMatrix * (A-posW[0]) );
  pos0W = posW[0];
  gl_Position = gl_ModelViewProjectionMatrix * vec4(A,1.0);
  EmitVertex();

  posWOut = B;
  posE = gl_ModelViewMatrix * vec4(B,1.0);
  //normalE = normal;
 // normalE = normalize( gl_NormalMatrix * (B-posW[0]) );
  pos0W = posW[0];
  gl_Position = gl_ModelViewProjectionMatrix * vec4(B,1.0);
  EmitVertex();

  posWOut = C;
  posE = gl_ModelViewMatrix * vec4(C,1.0);
//  normalE = normal;
//  normalE = normalize( gl_NormalMatrix * (C-posW[1]) );
  pos0W = posW[1];
  gl_Position = gl_ModelViewProjectionMatrix * vec4(C,1.0);
  EmitVertex();

  posWOut = D;
  posE = gl_ModelViewMatrix * vec4(D,1.0);
//  normalE = normal;
//  normalE = normalize( gl_NormalMatrix * (D-posW[1]) );
  pos0W = posW[1];
  gl_Position = gl_ModelViewProjectionMatrix * vec4(D,1.0);
  EmitVertex();

  EndPrimitive();
}


void main()
{
  vec3 dirW = posW[1]-posW[0];
  dirW = normalize(dirW);
  vec3 helper0 = vec3( 0.0,0.0,1.0 );
  vec3 helper1 = vec3( 1.0,0.0,0.0 );

  float dot0_curr = dot( helper0, dirW );
  float dot1_curr = dot( helper1, dirW );

  vec3 ortho0_curr = normalize( cross( helper0, dirW ) );
  if( dot1_curr > dot0_curr )
    ortho0_curr = normalize( cross( helper1, dirW ) );

  vec3 ortho1_curr = normalize( cross( ortho0_curr, dirW ) );

  ortho0_curr = ortho0_curr * 0.5 * v_thickness[0];
  ortho1_curr = ortho1_curr * 0.5 *  v_thickness[0];

//--

  float dot0_prev = dot( helper0, v_prevDirW[0] );
  float dot1_prev = dot( helper1, v_prevDirW[0] );

  vec3 ortho0_prev = normalize( cross( helper0, v_prevDirW[0] ) );
  if( dot1_prev > dot0_prev )
    ortho0_prev = normalize( cross( helper1, v_prevDirW[0] ) );

  vec3 ortho1_prev = normalize( cross( ortho0_prev, v_prevDirW[0] ) );

  ortho0_prev = ortho0_prev * 0.5 *  v_thickness[0];
  ortho1_prev = ortho1_prev * 0.5 *  v_thickness[0];

  if( length(ortho0_prev - ortho0_curr) > length(ortho0_prev - ortho1_curr) 
     || length(ortho1_prev - ortho1_curr) > length(ortho1_prev - ortho0_curr) )
  {
    vec3 ortho0PrevTemp = ortho0_prev;
    ortho0_prev = -ortho0_prev;
    ortho1_prev = -ortho1_prev;
  }


//-------------------

//      B
// 
// A   posW   C
//
//      D

  vec3 A0 = posW[0] - ortho0_prev;
  vec3 B0 = posW[0] + ortho1_prev;
  vec3 C0 = posW[0] + ortho0_prev;
  vec3 D0 = posW[0] - ortho1_prev;

  vec3 A1 = posW[1] - ortho0_curr;
  vec3 B1 = posW[1] + ortho1_curr;
  vec3 C1 = posW[1] + ortho0_curr;
  vec3 D1 = posW[1] - ortho1_curr;

  vec3 A_ = findNearest( A0, A1, B1, C1, D1 );
  vec3 B_ = findNearest( B0, A1, B1, C1, D1 );
  vec3 C_ = findNearest( C0, A1, B1, C1, D1 );
  vec3 D_ = findNearest( D0, A1, B1, C1, D1 );

  EmitQuad( A0, B0, A_, B_ );
  EmitQuad( B0, C0, B_, C_ );
  EmitQuad( C0, D0, C_, D_ );
  EmitQuad( D0, A0, D_, A_ );
}
