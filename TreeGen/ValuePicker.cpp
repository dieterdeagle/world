#include "ValuePicker.h"

// ----------------------------------------------------------------------------

ValuePicker::ValuePicker( ValuePicker::AttributeType attributeType,
                          const std::shared_ptr<FunctorUnary>& attributeFunctor,
                          ValuePicker::DistributionType distributionType,
                          float dev,
                          const std::shared_ptr<FunctorUnary>& postProcessing )
  : _attributeType(attributeType), _attributeFunctor(attributeFunctor),
    _distributionType(distributionType), _dev(dev),
    _postProcessing(postProcessing)
{

}

// ----------------

ValuePicker::ValuePicker( float mean,
                          ValuePicker::DistributionType distributionType,
                          float dev,
                          const std::shared_ptr<FunctorUnary>& postProcessing )
  : _mean(mean), _attributeType(AttributeType::None), _distributionType(distributionType), _dev(dev), _postProcessing(postProcessing)
{

}

// ----------------------------------------------------------------------------

float ValuePicker::operator()( const std::shared_ptr<std::default_random_engine>& randomEngine,
                               const std::shared_ptr<BranchSegmentAttributesPre>& attributes )
{
  float mean = _mean;

  if( attributes )
    {
    float attributeValue;
    switch( _attributeType )
    {
    case AttributeType::None:
      attributeValue = _mean;
    case AttributeType::BranchDepth:
      attributeValue = attributes->_depthOfBranch;
      break;
    case AttributeType::SegmentDepth:
      attributeValue = attributes->_depthInBranch;
      break;
    default:
      attributeValue = 0.f;
      break;
    }

    mean = _attributeFunctor->operator ()( attributeValue );
  }

  float randValue;
  switch( _distributionType )
  {
  case DistributionType::Uniform:
  {
    std::uniform_real_distribution<float> uniformDist( mean-_dev, mean+_dev );
    randValue = uniformDist( *randomEngine );
    break;
  }
  case DistributionType::Normal:
  {
    std::normal_distribution<float> normalDist( mean, _dev );
    randValue = normalDist( *randomEngine );
    break;
  }
  }

  return _postProcessing->operator()( randValue );
}

// ----------------------------------------------------------------------------
