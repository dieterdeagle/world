#include "TreeGenerator.h"

#include <chrono>
#include <iostream>

#include "geometry/Polygon.h"
#include "graphics/ImageManipulation.h"

#include "util/mathStuff.h"


///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
/// Distribution Based
///----------------------------------------------------------------------------

std::shared_ptr<Tree> TreeGeneratorDistributionBased::generateTree( int seed,
                                                                    const Vec3& startingPos,
                                                                    const Vec3& up ) const
{
//  _currSeed = seed;
//  _currStartingPos = startingPos;
//  _currUp = up;

  _randomEngine->seed( seed );

  std::shared_ptr<Tree> tree = std::make_shared<Tree>();

  float trunkLength = _initialBranchLength( _randomEngine, nullptr );
  float thickness = _initialThickness( _randomEngine, nullptr );

  std::shared_ptr<Branch> trunk = this->growBranch( startingPos, up, thickness, trunkLength, 0 );

  tree->_branches.push_back(trunk);

  return tree;
}

// ----------------------------------------------------------------------------

std::shared_ptr<Branch> TreeGeneratorDistributionBased::growBranch( const Vec3& startingPos,
                                                                    const Vec3& direction,
                                                                    float thickness,
                                                                    float length,
                                                                    unsigned int branchDepth ) const
{

  std::shared_ptr<Branch> branch = std::make_shared<Branch>( startingPos, direction, thickness );

  float branchDepthFract = static_cast<float>( branchDepth/static_cast<double>(_maxDepth) );

  std::shared_ptr<BranchSegmentAttributesPre> attributes
      = std::make_shared<BranchSegmentAttributesPre>( startingPos, 0.f, branchDepthFract );

  unsigned int nbSegments = _nbSegments( _randomEngine, attributes ); /// FIXME
  float segLength = length / static_cast<float>(nbSegments);
  Vec3 currPos = startingPos;

  float branchLengthFactor = _branchLengthFactor( _randomEngine, nullptr );

  unsigned int MAX_LEAVES_DEPTH = 2;

  for( unsigned int i = 0; i < nbSegments; ++i )
  {
    float fract = i / static_cast<float>(nbSegments);
    attributes = std::make_shared<BranchSegmentAttributesPre>( startingPos, fract, branchDepthFract );

    Vec3 segmentDirection = rotate( direction, _segmentRotation( _randomEngine, nullptr ),
                                               _segmentRotation(_randomEngine, nullptr ),
                                               _segmentRotation(_randomEngine, nullptr ) );
    Vec3 segmentEndPosition = currPos + segmentDirection * segLength;

    float currThickness = thickness; /// FIXME: add verjüngung
    std::shared_ptr<BranchSegment> currSegment = std::make_shared<BranchSegment>( currPos, segmentEndPosition, thickness );
    branch->_segments.push_back( currSegment );


    // branching
    if( branchDepth < _maxDepth - MAX_LEAVES_DEPTH )
    {
      unsigned int nbNewBranches = _nbBranches( _randomEngine, attributes );
      for( unsigned int j = 0; j < nbNewBranches; ++j )
      {
        Vec3 childBranchDirection = rotate( direction, _branchRotation( _randomEngine, nullptr ),
                                                       _branchRotation( _randomEngine, nullptr ),
                                                       _branchRotation( _randomEngine, nullptr ) );
        branch->_children.push_back( growBranch( segmentEndPosition,
                                                 childBranchDirection,
                                                 currThickness*_thicknessFactor( _randomEngine, nullptr ),
                                                 length * branchLengthFactor,
                                                 branchDepth+1 ) );
      }
    }

    currPos = segmentEndPosition;
  }

#define LEAVES false
#if LEAVES
  double LEAVES_WIDTH_HALF = 0.00025;
  if( branchDepth == _maxDepth - MAX_LEAVES_DEPTH )
  {
    // leaves
    std::cout << "need to grow leaves!\n";
    auto segs = branch->_segments;
    auto img = this->createLeavesImage( Vec3(segs.front()->_start), branchDepth );
    std::vector<Vec3> triangleStrip;
    std::vector<Vec2> texCoords;
    Vec3 currEnd(0,0,0);
    Vec3 currOrtho(0,0,0);
    for( std::size_t sId = 0; sId < segs.size(); ++sId )
//    for( const auto& s : segs )
    {
      auto s = segs[sId];
      auto start = s->_start;
      auto end   = s->_end;
      currEnd = end;
      Vec3 dir = end - start;
      currOrtho = dir.findPerpendicularVector();
      triangleStrip.push_back( start - currOrtho * LEAVES_WIDTH_HALF );
      texCoords.push_back( Vec2(sId/static_cast<double>(segs.size()), 0.0) );
      triangleStrip.push_back( start + currOrtho * LEAVES_WIDTH_HALF );
      texCoords.push_back( Vec2(sId/static_cast<double>(segs.size()), 1.0) );
    }
    branch->_leaves = std::make_shared<Leaves>( triangleStrip, img, texCoords );
  }
#endif

  return branch;
}

// ----------------------------------------------------------------------------

std::vector<Vec2> TreeGeneratorDistributionBased::createLeaf( const LineSegment& mainAxis,
                                                              const Vec2& extent ) const
{
  auto basicForm = PolygonUtil::createEllipseFromLineSegs( mainAxis, extent, 3 );

  // Todo: apply noise to basic form

  return basicForm;
}

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Image> TreeGeneratorDistributionBased::createLeavesImage( const Vec3& startingPosW,
                                                                            unsigned int branchDepth,
                                                                            double widthW, double heightW,
                                                                            unsigned int pixelPerMeter ) const
{
  std::cout << "create leaves img\n";
  unsigned int LEAVES_PER_SEGMENT = 1;
  double LEAF_SIZE_W = 0.00025; // 25 cm
  Color COLOR_STENGEL( 80,80,15 );
  Color COLOR_LEAF( 20,165,5 );

  unsigned int leafSizePixels = std::ceil( LEAF_SIZE_W * pixelPerMeter * 1000 );

  osg::ref_ptr<osg::Image> img( new osg::Image );
  auto width  = std::ceil( widthW * pixelPerMeter * 1000 ) * 2;
  auto height = std::ceil( heightW * pixelPerMeter * 1000 );
  std::cout << "img. w: " << width << ", h: " << height << "\n";
  img->allocateImage( width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE );


  float branchDepthFract = static_cast<float>( branchDepth/static_cast<double>(_maxDepth) );
  std::shared_ptr<BranchSegmentAttributesPre> attributes
      = std::make_shared<BranchSegmentAttributesPre>( startingPosW, 0.f, branchDepthFract );

  unsigned int nbSegments = _nbSegments( _randomEngine, attributes );
  std::cout << "Nb segments " << nbSegments << "\n";

  double stepsize = height / (nbSegments+1);
  std::cout << "STEPSIZE: " << stepsize << "\n";

  Vec2 startPos( width / 2.0, 0 );
  Vec2 mainDir( 0, 1.0 );

  this->drawBranches2d( img, startPos, mainDir, 20.0, height-stepsize, 0 );

  return img;

  // ---------------------------------
  // ---------------------------------

  auto currPos = startPos;
  for( unsigned int i = 0; i < nbSegments; ++i )
  {
    std::cout << "i: " << i << "\n";
    currPos += Vec2(0,stepsize);
    std::cout << "  curr pos: " << currPos << "\n";

    auto currDir = rotate( mainDir, _branchRotation( _randomEngine, nullptr ) );
    auto innerPos = currPos;
    auto innerDir = currDir;
    for( unsigned int j = 0; j < nbSegments * 2; ++j )
    {
      auto innerStepsize = stepsize / 5.0;
      drawLine( img, innerPos, innerPos + innerDir*innerStepsize );
      innerPos += innerDir*innerStepsize;
      innerDir = rotate( innerDir, _segmentRotation( _randomEngine, nullptr ) ).normalized();
    }

    // draw stengel
    auto stengelEnd = currPos + currDir.normalized() * leafSizePixels / (1.618033*1.618033);
    drawLine( img, currPos, stengelEnd, COLOR_STENGEL, std::sqrt(leafSizePixels) / 5.0 );

    auto leafEnd = stengelEnd + currDir.normalized() * leafSizePixels;
    auto leaf = this->createLeaf( LineSegment( stengelEnd, leafEnd ),
                                  currDir.getOrtho().normalized() * leafSizePixels / (2*1.618033) );

//    drawPolygonFilled( img, leaf.begin(), leaf.end(), COLOR_LEAF, 2, COLOR_LEAF );

  }


  return img;
  // Todo!!
//  drawPolygon( img, )

}

// ----------------------------------------------------------------------------

void TreeGeneratorDistributionBased::drawBranches2d( const osg::ref_ptr<osg::Image>& img,
                                                     const Vec2& startPos, const Vec2& initDir,
                                                     double thickness, double length,
                                                     unsigned int branchDepth ) const
{

  Color COLOR_LEAF( 20,165,5 );


  float branchDepthFract = static_cast<float>( branchDepth/static_cast<double>(_maxDepth) );
  std::shared_ptr<BranchSegmentAttributesPre> attributes
      = std::make_shared<BranchSegmentAttributesPre>( Vec3( startPos,0), 0.f, branchDepthFract );

  unsigned int nbSegments = _nbSegments( _randomEngine, attributes ) -2;
  std::cout << "Nb segments " << nbSegments << "\n";

  double stepsize = length / (nbSegments+1);
  std::cout << "STEPSIZE: " << stepsize << "\n";

  auto currPos = startPos;
  auto lastPos = startPos;
  auto currDir = initDir.normalized();
  for( unsigned int i = 0; i < nbSegments; ++i )
  {
    currPos += currDir * stepsize;
    drawLine( img, lastPos, currPos, Color(50,60,5), thickness ); // Todo: color(s) as ValuePicker

    currDir = rotate( currDir, _segmentRotation( _randomEngine, nullptr ) );

    if( branchDepth < _maxDepth )
    {
      auto newBranchDir = rotate( currDir, _branchRotation(_randomEngine, nullptr ) );
      this->drawBranches2d( img,
                            currPos,
                            newBranchDir,
                            thickness * _thicknessFactor(_randomEngine, nullptr ),
                            length * _branchLengthFactor(_randomEngine, nullptr ),
                            branchDepth+1 );
    }

    if( branchDepth == _maxDepth )
    {
      // Todo: draw leaves
      auto newBranchDir = rotate( currDir, _branchRotation(_randomEngine, nullptr ) );
      auto leaf = this->createLeaf( LineSegment( currPos, currPos + newBranchDir.normalized() * stepsize ),
                                    currDir.getOrtho().normalized() * stepsize / (2*1.618033) );

      drawPolygonFilled( img, leaf.begin(), leaf.end(), COLOR_LEAF, 2, COLOR_LEAF );
    }

    lastPos = currPos;
  }


}

// ----------------------------------------------------------------------------

TreeGeneratorDistributionBased::TreeGeneratorDistributionBased( ValuePicker initialThickness,
                                                                ValuePicker thicknessFactor,
                                                                ValuePicker initialBranchLength,
                                                                ValuePicker branchLengthFactor,
                                                                ValuePicker nbBranches,
                                                                ValuePicker nbSegments,
                                                                unsigned int maxDepth,
                                                                ValuePicker branchRotation,
                                                                ValuePicker segmentRotation )
  : _initialThickness(initialThickness), _thicknessFactor(thicknessFactor), _initialBranchLength(initialBranchLength), _branchLengthFactor(branchLengthFactor),
    _nbBranches(nbBranches), _nbSegments(nbSegments), _branchRotation(branchRotation),
    _segmentRotation(segmentRotation), _maxDepth(maxDepth)
{
}

// ----------------------------------------------------------------------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setInitialThickness( const ValuePicker& initialThickness )
{
  _initialThickness = initialThickness;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setThicknessFactor( const ValuePicker& thicknessFactor )
{
  _thicknessFactor = thicknessFactor;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setInitialBranchLength( const ValuePicker& initialBranchLength )
{
  _initialBranchLength = initialBranchLength ;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setBranchLengthFactor( const ValuePicker& branchLengthFactor )
{
  _branchLengthFactor = branchLengthFactor;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setNbBranches( const ValuePicker& nbBranches )
{
  _nbBranches = nbBranches;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setMaxDepth( unsigned int maxDepth )
{
  _maxDepth = maxDepth;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setNbSegments( const ValuePicker& nbSegments )
{
  _nbSegments = nbSegments;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setBranchRotation( const ValuePicker& branchRotation )
{
  _branchRotation = branchRotation;
  return *this;
}

// ----------------

TreeGeneratorDistributionBased::Builder& TreeGeneratorDistributionBased::Builder::setSegmentRotation( const ValuePicker& segmentRotation )
{
  _segmentRotation = segmentRotation;
  return *this;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

TreeGeneratorDistributionBased TreeGeneratorHelpers::randomTreeGenerator( int seed )
{

  std::mt19937 twister;
  if( seed < 0 )
    seed = std::time(0);
  twister.seed( seed );
  std::cout << "Random tree generator, seed: " << seed << "\n";


  std::uniform_int_distribution<int> uniIntDist;

  TreeGeneratorDistributionBased::Builder builder;
  auto nbBranches = uniIntDist(twister) % 4 + 1; /// FIXME: 8? Other value?
  std::uniform_real_distribution<float> uniFloatDist( 0.f, 0.9f );
  auto nbBranchesDev = uniFloatDist(twister);
  auto nbBranchesPostVal = std::uniform_real_distribution<float>(0.f,0.5f)(twister);
  builder.setNbBranches( ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                      std::make_shared<FunctorUnaryConstant>(nbBranches),
                                      ValuePicker::DistributionType::Uniform,
                                      nbBranchesDev,
                                      std::make_shared<FunctorUnarySetToZero>(nbBranchesPostVal) ) );

  auto rotationVal = std::normal_distribution<float>( 40.f, 12.5f )(twister);
  auto rotationDev = std::normal_distribution<float>( 15.f, 2.5f )(twister);
  builder.setBranchRotation( ValuePicker( rotationVal,
                                          ValuePicker::DistributionType::Uniform,
                                          rotationDev,
                                          std::make_shared<FunctorUnaryRandomSign>() ) );


  float initialBranchLength = 0.f;
  {
  std::uniform_real_distribution<float> uniFloatDist( 0.0005f, 0.015f );
  initialBranchLength = uniFloatDist(twister);
  builder.setInitialBranchLength( ValuePicker( initialBranchLength,
                                               ValuePicker::DistributionType::Normal,
                                               0.0001f /*FIXME*/) );
  }

  {
  std::normal_distribution<float> normFloatDist( std::sqrt(initialBranchLength) / 500.f,
                                                 0.00005f );
//    std::uniform_real_distribution<float> uniFloatDist( std::sqrt(initialBranchLength) / 1000.f, std::sqrt(initialBranchLength) / 500.f );
  auto initialThickness = normFloatDist(twister);
  builder.setInitialThickness( ValuePicker( initialThickness,
                                            ValuePicker::DistributionType::Normal,
                                            0.000005f /*FIXME*/) );
  }



  auto nbSegmentsVal0 = std::uniform_real_distribution<float>( 1.f, 15.f )(twister);
  auto nbSegmentsVal1 = std::uniform_real_distribution<float>( 1.f, 15.f )(twister);
  auto nbSegmentsDev = std::uniform_real_distribution<float>( 0.f, 1.f )(twister);
  builder.setNbSegments( ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                      std::make_shared<FunctorLinear>( std::make_pair(0.f,nbSegmentsVal0), std::make_pair(1.f,nbSegmentsVal1) ),
                                      ValuePicker::DistributionType::Uniform,
                                      nbSegmentsDev ) );

  {
//  std::uniform_real_distribution<float> distrSegRot( -10.f, 10.f );
  auto segRotMean = std::normal_distribution<float>( 0.f, 5.f )(twister);
  auto segRotDev  = std::normal_distribution<float>( 0.5f, 5.f )(twister);
  builder.setSegmentRotation( ValuePicker( segRotMean,//0.f,
                                           ValuePicker::DistributionType::Normal,
                                           segRotDev, //10.f,
                                           std::make_shared<FunctorUnaryRandomSign>() ) );
  }

  auto maxDepth = 4; /* FIXME: random/varying max depth? */
  auto currGen = builder.setMaxDepth( maxDepth ).build();

  return currGen;
}
