#include "Functors.h"

#include "cassert"

// ----------------------------------------------------------------------------

void FunctorTest::simpleTests()
{
  std::map<float,float> testMap;
  testMap[0.1] = 1.1;
  testMap[0.2] = 1.2;
  testMap[0.4] = 1.4;
  testMap[0.8] = 1.8;
  FunctorUnaryMapInterpolation mapInterpol( testMap );
  assert( std::abs(mapInterpol( 0.1 ) - 1.1) < 1.0e-7 );
  assert( std::abs(mapInterpol( 0.15 ) - 1.15) < 1.0e-7 );
  assert( std::abs(mapInterpol( 0.0 ) - 1.1) < 1.0e-7 );
  assert( std::abs(mapInterpol( 0.8 ) - 1.8) < 1.0e-7 );
  assert( std::abs(mapInterpol( 0.9 ) - 1.8) < 1.0e-7 );
}

// ----------------------------------------------------------------------------

float FunctorUnaryMapInterpolation::operator()( float x )
{
  auto findIter = _map.find( x );
  if( findIter != _map.end() )
    return findIter->second;

  // clamping
  if( x <= _map.begin()->first )
    return _map.begin()->second;
  if( x >= _map.rbegin()->first )
    return _map.rbegin()->second;

  auto lower = _map.lower_bound( x );
  auto upper = _map.upper_bound( x );

  if( lower->first > x )
    lower--;

  float distance = upper->first - lower->first;

  float weightLower = 1.f - (x - lower->first) / distance;
  float weightUpper = 1.f - (upper->first - x) / distance;

  return weightLower*lower->second + weightUpper*upper->second;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

AdapterUnary::AdapterUnary(std::shared_ptr<FunctorUnary> f, std::shared_ptr<ValueExtractorBranchSegmentAttributes> valueExtractor)
  : _f(f), _valueExtractor(valueExtractor)
{

}

// ----------------------------------------------------------------------------

float AdapterUnary::operator()( const BranchSegmentAttributesPre& input )
{
  return _f->operator ()( _valueExtractor->operator()(input) );
}


// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

FunctorLinear::FunctorLinear( std::pair<float, float> lower, std::pair<float, float> upper )
  : _map( FunctorUnaryMapInterpolation( std::map<float,float>{ lower,upper } ) )
{}

// --------------

float FunctorLinear::operator()( float x )
{
  float tempVal = _map(x);
  return tempVal;
}
