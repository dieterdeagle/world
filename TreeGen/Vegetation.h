#ifndef TreeGen_Vegetation_h
#define TreeGen_Vegetation_h

#include <memory>

#include "PlaneTerrain/PlaneTerrain.h"

#include "util/noise/FractionalBrownianMotion.h"
#include "util/noise/PerlinNoise.h"
#include "util/Vec2.h"

#include "Tree.h"
#include "TreeGenerator.h"


class Vegetation
{
public:
  Vegetation( int seed, // or: perlin or function
              const std::shared_ptr<PlaneTerrain>& terrain,
              unsigned int nbSamplesPerUnit );

  std::vector< std::shared_ptr<Tree> > createVegetationForPatch( const Vec2& minPos, const Vec2& maxPos ) const;


private:
  void initTreeGenerators();

  Vec3 ruettel( const Vec3& pos, const Vec2& maxDist ) const;

private:
  std::mt19937 _randomEngine;

  std::shared_ptr<PlaneTerrain> _terrain;

  /**
   * @brief _treeGenerators Each treeGenerator represents a different type of tree
   */
  std::vector<TreeGeneratorDistributionBased> _treeGenerators;

  /**
   * @brief _population Describes the types of trees at given positions.
   *   The range of noise values is splitted into intervals. Each interval
   *   represents a treeType.
   */
  PerlinNoise _perlin;
  FbmFunctor<Vec2> _population{ _perlin };

  PerlinNoise _positionRuettlerX;
  PerlinNoise _positionRuettlerY;

  /**
   * @brief _seeder Used to create seeds for positions.
   */
  PerlinNoise _seeder;

  unsigned int _nbTreeTypes;

  unsigned int _nbSamplesPerUnit;

  double _width;
  double _depth;
};

#endif
