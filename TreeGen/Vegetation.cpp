#include "Vegetation.h"

#include <random>

// ----------------------------------------------------------------------------

Vegetation::Vegetation( int seed,
                        const std::shared_ptr<PlaneTerrain>& terrain,
                        unsigned int nbSamplesPerUnit )
  : _terrain(terrain), _nbSamplesPerUnit(nbSamplesPerUnit)
{

  _width = _terrain->getWidth();
  _depth = _terrain->getDepth();

  std::uniform_int_distribution<int> dist;
  _randomEngine = std::mt19937( seed );

  /// FIXME: Parameter?
  int MAXNUM_TREETYPES = 128;
  _nbTreeTypes = 1 + dist(_randomEngine ) % MAXNUM_TREETYPES;
  std::cout << "Vegetation. nb of tree types: " << _nbTreeTypes << "\n";
  // init generators etc

  _perlin = PerlinNoise( dist(_randomEngine), std::max( terrain->getWidth(),terrain->getDepth() ) /*, FIXME: Repeat*/ );
  _population = FbmFunctor<Vec2>( _perlin, 4, 0.5, 2 );
  _seeder = PerlinNoise( dist(_randomEngine) /*, FIXME: Repeat*/ );

  _positionRuettlerX = PerlinNoise( dist(_randomEngine) /*, FIXME: Repeat */ );
  _positionRuettlerY = PerlinNoise( dist(_randomEngine) /*, FIXME: Repeat */ );

  this->initTreeGenerators();
}

// ----------------------------------------------------------------------------

std::vector< std::shared_ptr<Tree> > Vegetation::createVegetationForPatch( const Vec2& minPos,
                                                                           const Vec2& maxPos ) const
{
  auto flooredMin = floor( minPos );
  auto ceiledMax = ceil( maxPos );

  auto range = ceiledMax - flooredMin;
  unsigned int nbSamplesX = range.getX() * _nbSamplesPerUnit;
  unsigned int nbSamplesY = range.getY() * _nbSamplesPerUnit;

  Vec2 maxDist = range / _nbSamplesPerUnit;

//  std::cout << "veg samples floored to ceiled: " << nbSamplesX << " x " << nbSamplesY << "\n";

  auto treeGeneratorsCopied = _treeGenerators;

  double stepsize = 1.0 / _nbSamplesPerUnit;

  std::vector< std::shared_ptr<Tree> > vegetation;
  for( unsigned int i = 0; i < nbSamplesX; ++i )
  {
    Vec2 currPos( minPos.getX() + i*stepsize, minPos.getY() );
    for( unsigned int j = 0; j < nbSamplesY; ++j )
    {
      currPos[1] = minPos.getY() + j*stepsize;

      // check if curr pos in bounding box.
      if( currPos.getX() > minPos.getX() && currPos.getX() < maxPos.getX()
          && currPos.getY() > minPos.getY() && currPos.getY() < maxPos.getY() )
      {
        auto typeVal = _population( currPos ) + 0.5;
        auto grad = _terrain->getGradAt( currPos );

        if( typeVal > 0.4 && grad.length() < _terrain->getEPSILON()*2 )
        {
          auto currHeight = _terrain->getHeightAt( currPos );
          auto type = std::floor( typeVal * _nbTreeTypes );
//          std::cout << "curr tree type: " << type << "\n";
          int seed = (_seeder( currPos ) + 1.0) / 2.5 * std::numeric_limits<int>::max(); /// FIXME: check range of perlin. now assuming [-1,1]
          vegetation.push_back( treeGeneratorsCopied[type].generateTree( seed,
                                                                         ruettel( Vec3( currPos.getX(), currPos.getY(), currHeight), maxDist ),
                                                                         _terrain->getUp() ) );
        }
      }
    }
  }

  return vegetation;
}

// ----------------------------------------------------------------------------

void Vegetation::initTreeGenerators()
{
  std::uniform_int_distribution<int> uniIntDist;

  std::normal_distribution<double> normalDoubleDist;

  for( unsigned int i = 0; i < _nbTreeTypes; ++i )
  {
    TreeGeneratorDistributionBased::Builder builder;
    auto nbBranches = uniIntDist(_randomEngine) % 4 + 1; /// FIXME: 4? Other value?
    std::uniform_real_distribution<float> uniFloatDist( 0.f, 0.9f );
    auto nbBranchesDev = uniFloatDist(_randomEngine);
    auto nbBranchesPostVal = std::uniform_real_distribution<float>(0.f,0.5f)(_randomEngine);
    builder.setNbBranches( ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                        std::make_shared<FunctorUnaryConstant>(nbBranches),
                                        ValuePicker::DistributionType::Uniform,
                                        nbBranchesDev,
                                        std::make_shared<FunctorUnarySetToZero>(nbBranchesPostVal) ) );

    auto rotationVal = std::normal_distribution<float>( 40.f, 12.5f )(_randomEngine);
    auto rotationDev = std::normal_distribution<float>( 15.f, 2.5f )(_randomEngine);
    builder.setBranchRotation( ValuePicker( rotationVal,
                                            ValuePicker::DistributionType::Uniform,
                                            rotationDev,
                                            std::make_shared<FunctorUnaryRandomSign>() ) );


    float initialBranchLength = 0.f;
    {
    std::uniform_real_distribution<float> uniFloatDist( 0.0005f, 0.015f );
    initialBranchLength = uniFloatDist(_randomEngine);
    builder.setInitialBranchLength( ValuePicker( initialBranchLength,
                                                 ValuePicker::DistributionType::Normal,
                                                 0.0001f /*FIXME*/) );
    }

    {
    std::normal_distribution<float> normFloatDist( std::sqrt(initialBranchLength) / 500.f,
                                                   0.00005f );
//    std::uniform_real_distribution<float> uniFloatDist( std::sqrt(initialBranchLength) / 1000.f, std::sqrt(initialBranchLength) / 500.f );
    auto initialThickness = normFloatDist(_randomEngine);
    builder.setInitialThickness( ValuePicker( initialThickness,
                                              ValuePicker::DistributionType::Normal,
                                              0.000005f /*FIXME*/) );
    }



    auto nbSegmentsVal0 = std::uniform_real_distribution<float>( 1.f, 15.f )(_randomEngine);
    auto nbSegmentsVal1 = std::uniform_real_distribution<float>( 1.f, 15.f )(_randomEngine);
    auto nbSegmentsDev = std::uniform_real_distribution<float>( 0.f, 1.f )(_randomEngine);
    builder.setNbSegments( ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                        std::make_shared<FunctorLinear>( std::make_pair(0.f,nbSegmentsVal0), std::make_pair(1.f,nbSegmentsVal1) ),
                                        ValuePicker::DistributionType::Uniform,
                                        nbSegmentsDev ) );

    auto maxDepth = 2; /* FIXME: random/varying max depth? */
    auto currGen = builder.setMaxDepth( maxDepth ).build();
    _treeGenerators.push_back( currGen );
  }
}

// ----------------------------------------------------------------------------

Vec3 Vegetation::ruettel( const Vec3& pos,
                          const Vec2& maxDist ) const
{
  auto ruettelX = _positionRuettlerX.noise( pos * 4000.0 ) * maxDist.getX();
  auto ruettelY = _positionRuettlerY.noise( pos * 63400.0 ) * maxDist.getY();

  return pos + Vec3( ruettelX, ruettelY, 0.0 );
}

// ----------------------------------------------------------------------------
