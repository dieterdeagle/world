#ifndef TreeGen_Functors_h
#define TreeGen_Functors_h

#include "Tree.h"

#include <ctime>
#include <map>
#include <memory>
#include <random>

// -----------------------------------
namespace FunctorTest
{
void simpleTests();
}

// -----------------------------------

class FunctorUnary
{
public:
  virtual float operator()( float x ) = 0;
};

// ---------------

class FunctorUnaryConstant : public FunctorUnary
{
public:
  FunctorUnaryConstant( float constant ){ _constant = constant; }
  virtual float operator()( float ){ return _constant; }
private:
  float _constant;
};

// ---------------

class FunctorUnaryKeepValue : public FunctorUnary
{
public:
  virtual float operator()( float x ){ return x; }
};

// ---------------

class FunctorUnaryRandomSign : public FunctorUnary
{
public:
  virtual float operator()( float x )
  {
    std::discrete_distribution<int> distr{ 1.0, 1.0 }; // FIXME: da stimmt doch was nicht..

    int randVal = distr( _generator );
    float val;
    switch( randVal )
    {
    case 0:
      val = x*-1.0;
      return val;
      break;
    case 1:
      val = x*1.0;
      return val;
      break;
    }
  }
private:
  std::default_random_engine _generator;
};

// ---------------

class FunctorUnarySetToZero: public FunctorUnary
{
public:
  FunctorUnarySetToZero( float probability )
    : _probability( std::min(1.f, std::max(0.f, probability)) )
  {}

  virtual float operator()( float x )
  {
    // clamp
//    x = std::min( 1.f, std::max(0.f, x) );
    std::discrete_distribution<int> distr{ _probability, 1.f-_probability };

    int randVal = distr( _generator );
    auto probs = distr.probabilities();
//    for( auto prob : probs )
//      std::cout << "prob: " << prob << "\n";
//    std::cout << randVal << "\n";
    return randVal * x;
  }
private:
  std::default_random_engine _generator;
  float _probability;
};

// ---------------

class FunctorUnaryMapInterpolation : public FunctorUnary
{
public:
  FunctorUnaryMapInterpolation( const std::map<float,float>& map ){ _map = map; }
  virtual float operator()( float x );

private:
  std::map<float,float> _map;
};

// ---------------

class FunctorLinear : public FunctorUnary
{
public:
  FunctorLinear( std::pair<float,float> lower, std::pair<float,float> upper );
  virtual float operator()( float x );
private:
  FunctorUnaryMapInterpolation _map;
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class ValueExtractorBranchSegmentAttributes
{
public:
  typedef BranchSegmentAttributesPre input_type;
  virtual float operator()( const BranchSegmentAttributesPre& input ) = 0;
};

// ---------------

class ExtractBranchDepth : public ValueExtractorBranchSegmentAttributes
{
  virtual float operator()( const BranchSegmentAttributesPre& input )
  {
    return input._depthOfBranch;
  }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

class AdapterUnary
{
public:
  AdapterUnary( std::shared_ptr<FunctorUnary> f, std::shared_ptr<ValueExtractorBranchSegmentAttributes> valueExtractor );
  float operator()( const BranchSegmentAttributesPre& input );

private:
  std::shared_ptr<FunctorUnary> _f;
  std::shared_ptr<ValueExtractorBranchSegmentAttributes> _valueExtractor;
};

// -------------------------------------------

#endif
