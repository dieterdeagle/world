#ifndef ValuePicker_h
#define ValuePicker_h

#include <memory>
#include <random>

#include "Functors.h"
#include "Tree.h"

class ValuePicker
{
public:
  enum class AttributeType { None, BranchDepth, SegmentDepth };
  enum class DistributionType { Uniform, Normal };

  ValuePicker( AttributeType attributeType,
               const std::shared_ptr<FunctorUnary>& attributeFunctor,
               DistributionType distributionType,
               float dev,
               const std::shared_ptr<FunctorUnary>& postProcessing = std::make_shared<FunctorUnaryKeepValue>() );

  ValuePicker( float mean,
               DistributionType distributionType,
               float dev,
               const std::shared_ptr<FunctorUnary>& postProcessing = std::make_shared<FunctorUnaryKeepValue>() );

  float operator()( const std::shared_ptr<std::default_random_engine>& randomEngine,
                    const std::shared_ptr<BranchSegmentAttributesPre>& attributes = nullptr );

private:
  AttributeType _attributeType;
  std::shared_ptr<FunctorUnary> _attributeFunctor;
  float _mean = 0.f;
  DistributionType _distributionType;

  float _dev;


  std::shared_ptr<FunctorUnary> _postProcessing;
};

// ----------------

#endif
