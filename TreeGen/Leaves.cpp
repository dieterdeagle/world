#include "Leaves.h"

#include <iostream>
#include <string>

#include <osg/Image>
#include <osgDB/WriteFile>

#include "geometry/Polygon.h"

#include "graphics/ImageManipulation.h"
#include "../util/Vec3.h"

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Program> createLeaveShaderTest()
{
  Vec3 stalkStart( 0,-1,0 );
  Vec3 stalkEnd( 0,0,0 ); // leave start
  Vec3 A = Vec3(-1,0,0);
  Vec3 B = Vec3(-1,2,0);
  Vec3 C = Vec3(1,2,0);
  Vec3 D = Vec3(1,0,0);

  std::vector<Triangle> triangles{ Triangle{A,B,C}, Triangle{A,C,D} };
  Vec3 center( 0,0,0 );

  std::string geometryShader
      = std::string("#version 330\n")
      + "layout(points) in;\n"
      + "layout(triangle_strip, max_vertices=" + std::to_string(triangles.size()*3) + ") out;\n";

  std::cout << geometryShader << std::endl;

  /// FIXME
  return nullptr;
}

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Image> createLeavesImg()
{
  int width = 512;
  int height = 1024;

  osg::ref_ptr<osg::Image> img( new osg::Image );
  img->allocateImage( width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE );

  unsigned int SAMPLES_PER_BRANCH = 4;
  unsigned int DEPTH = 2;
  double LEAF_SIZE = 80;
  double leafWidth = LEAF_SIZE / ( 1.618033 );

  double stepsizeTrunk = height / static_cast<double>( SAMPLES_PER_BRANCH+1 );
  std::cout << "stepsize trunk: " << stepsizeTrunk << "\n";
  Vec2 currPos( width/2.0, 0 );
  for( unsigned int i = 0; i < SAMPLES_PER_BRANCH; ++i )
  {
    auto lastPos = currPos;
    currPos += Vec2(0,stepsizeTrunk);
    std::cout << "stepsize trunk: " << stepsizeTrunk << "\n";
    std::cout << "currpos: " << currPos << "\n";
    auto currDir = (currPos - lastPos).normalized();
    std::cout << "  curr dir: " << currDir << "\n";
    auto ortho1 = currDir.getOrtho().normalized();
    Vec2 ortho2 = ortho1 * -1.0;
    auto subBranchDir = ( (currDir + ortho1) / 2.0 ).normalized();
    if( i % 2 == 0 )
      subBranchDir = ( (currDir + ortho2) / 2.0 ).normalized();
    auto leafStart = currPos + subBranchDir * 40;
    drawLine( img, currPos, leafStart, Color(0,0,0), 10.0 );

    drawLeaf( img,
              leafStart - subBranchDir.getOrtho()*leafWidth*0.5,
              leafStart + subBranchDir*LEAF_SIZE + subBranchDir.getOrtho()*leafWidth*0.5,
              leafStart + subBranchDir*LEAF_SIZE - subBranchDir.getOrtho()*leafWidth*0.5,
              leafStart + subBranchDir.getOrtho()*leafWidth*0.5,
              Color(0,0,255) );

    for( unsigned int d = 0; d < DEPTH; ++d )
    {

    }
  }

  Vec2 samplePt( width/2, height/10 );
  Vec2 sampleDir( -1 * width/10,1*height/10 );
  Vec2 sampleOrtho( -1*height/30, -1*width/30 );
  drawLeaf( img, samplePt + sampleOrtho,
                  samplePt + sampleDir+sampleOrtho,
                  samplePt + sampleDir-sampleOrtho,
                  samplePt - sampleOrtho, osg::Vec4(0.0,0.0,1.0,1.0) );

  drawLeaf( img, Vec2(15.0,15.0), Vec2(15,65), Vec2(30,65), Vec2(30,15), Color(0,255,0) );
  drawLeaf( img, Vec2(100.0,400.0), Vec2(100,470), Vec2(130,470), Vec2(130,400), Color(0,255,0,255) );
//  drawLeaf( img, osg::Vec2(0,0), osg::Vec2(0,1024), osg::Vec2(512,1024),osg::Vec2(512,0), osg::Vec4(0.0,1.0,0.0,1.0) );

  drawLine( img, Vec2(width/2, 0), Vec2(width/2,height), Color(100,100,10), std::sqrt(width) / 2 );

  osgDB::writeImageFile( *img, "/tmp/testImg.png" );

  return img;
}

// ----------------------------------------------------------------------------

void drawLeaf( const osg::ref_ptr<osg::Image>& img,
                const Vec2& pt1, const Vec2& pt2,
                const Vec2& pt3, const Vec2& pt4,
                const Color& color )
{
  std::cout << "HEEEE!\n";
  auto ellipse = PolygonUtil::createEllipseFromLineSegs( LineSegment( Vec2((pt1+pt4)/2.0), Vec2((pt2+pt3)/2.0) ),
                                          Vec2((pt1-pt4)/2.0), 10 );

  drawPolygonFilled( img, ellipse.begin(), ellipse.end(), Color(255,0,0), 3.0, Color(50,150,5) );

//  drawPolygon( img, ellipse.begin(), ellipse.end(), osg::Vec4(1.0,0.0,1.0,1.0), 2.0 );
//  fillAroundPixel( img, (ellipse.front()+ellipse[ellipse.size()/2] ) / 2.0, Color(20,180,0,255) );

  drawLine( img, ellipse.front(), ellipse[(ellipse.size())/2-1], Color(75,125,10), 5 );
}
