#ifndef Leaves_h
#define Leaves_h

//#include "util/Vec3.h"
#include <osg/Program>

#include "graphics/Color.h"
#include "util/Vec3.h"

struct Triangle
{
  Vec3 a;
  Vec3 b;
  Vec3 c;
};

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Program> createLeaveShaderTest();

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Image> createLeavesImg();

// ----------------------------------------------------------------------------

void drawLeaf( const osg::ref_ptr<osg::Image>& img,
               const Vec2& pt1, const Vec2& pt2,
               const Vec2& pt3, const Vec2& pt4,
               const Color& color );

// ----------------------------------------------------------------------------

#endif
