#ifndef DNA_H
#define DNA_H

/// S = Start of value, N = 0, A = 1, .., D = 4, E = End of Gene
enum class Acid { S, N, A, B, C, D, E };

class Chromosome
{
public:
  Chromosome();
};

#endif
