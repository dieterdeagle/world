#ifndef TreeGen_Shaders_h
#define TreeGen_Shaders_h

#include <osg/Matrixf>
#include <osg/Program>
#include <osg/NodeCallback>
#include <osg/NodeVisitor>

#include <osgGA/CameraManipulator>

#include <iostream>

osg::Program* createGrassShader();

osg::Program* createTreeShader();

//-----------------------------------------------------

class updateWindShader : public osg::Uniform::Callback
{
public:
  virtual void operator()
     ( osg::Uniform* uniform, osg::NodeVisitor* nv )
  {
     float angle = nv->getFrameStamp()->getReferenceTime();
     float x = sinf(angle)*0.05;
     float y = cosf(1.2*angle)*.05f;
     osg::Vec2 windVec(x,y);
     uniform->set(windVec);
  }
};

//-----------------------------------------------------

class updateCameraPos : public osg::Uniform::Callback
{
public:
  void setCameraManipulator( osgGA::CameraManipulator* manipulator )
  {
    _manipulator = manipulator;
  }

  virtual void operator()
     ( osg::Uniform* uniform, osg::NodeVisitor* nv )
  {
    osg::Matrixd matrix = _manipulator->getMatrix();

    int i = 3;

    osg::Vec4 eyeInWorld = osg::Vec4(0.0,0.0,0.0,1.0);

    osg::Vec4 pos = _manipulator->getMatrix()*eyeInWorld;
    osg::Vec3 pos3d = osg::Vec3(pos.x(),pos.y(),pos.z());

    std::cout << pos.x() << "," << pos.y() << "," << pos.z() << std::endl;
    uniform->set(pos3d);

  }
private:
  osgGA::CameraManipulator* _manipulator;
};



//-----------------------------------------------------

class updateViewMatrix : public osg::Uniform::Callback
{
public:
  void setCameraManipulator( osg::Camera* camera )
  {
    _camera = camera;
  }

  virtual void operator()
     ( osg::Uniform* uniform, osg::NodeVisitor* nv )
  {
    osg::Vec4 eyeInWorld = osg::Vec4(0.0,0.0,0.0,1.0);
    osg::Vec4 cameraPos = eyeInWorld*_camera->getInverseViewMatrix();
    std::cout << cameraPos.x() << "," << cameraPos.y() << "," << cameraPos.z() << "," << cameraPos.w() << std::endl;
    uniform->set( _camera->getInverseViewMatrix() );
//    uniform->set( _camera->getProjectionMatrix()*_camera->getViewMatrix() );
  }
private:
  osg::Camera* _camera;
};


//--------------------------------------------------------------

static const char* vertGrass =
{
  "#version 330\n"
  "layout(location = 0) in vec3 position;\n"
//  "layout(location = 1) in vec3 direction;\n"
  "uniform sampler2D heightmap;\n"
  "uniform vec2 windVec;\n"
  "uniform mat4 osg_ProjectionMatrix;\n"
  "uniform mat4 osg_ModelViewMatrix;\n"
  "out float height;\n"
  "out vec2 v_windVec_out;\n"
  "out float sizeFactor;\n"
  "const float minHeight = 0.2;\n"
  "const float maxHeight = 2.8;\n"
  "const float factor = 1.0;\n"
  "void main(void)\n"
  "{\n"
  "  vec2 texCoord = gl_MultiTexCoord0;\n"
  "  height = (sin( texture2D(heightmap, texCoord).r )+1.0) / maxHeight * factor;"
  "  v_windVec_out = windVec;\n"
  "  sizeFactor = factor;\n"
  "  gl_Position = vec4(position,1.0);\n"
  "}\n"
};


static const char* geomGrass =
{
  "#version 330\n"
  "#extension GL_EXT_geometry_shader4 : enable\n"
  "in float height[];\n"
  "in float sizeFactor[];\n"
  "in vec2 v_windVec_out[];\n"
  "uniform mat4 osg_ProjectionMatrix;\n"
  "uniform mat4 osg_ModelViewMatrix;\n"
  "uniform mat4 aha;\n"
  "out float rand3;\n"
  "out vec4 Color;\n"
  "\n"
  "vec4 applyWind( vec4 pos, vec2 windVec )\n"
  "{\n"
  "   float windCoeff = pow(pos.z/height[0],1.5);\n"
  "   vec4 test = pos + vec4(windVec.x,windVec.y,0.0,0.0)*windCoeff*sizeFactor[0];\n"
  "   test.z = pos.z - length(windVec)*windCoeff*sizeFactor[0];\n"
  "   return test;\n"
  "}\n"
  "\n"
  "void main(void)\n"
  "{\n"
  "  vec4 v = gl_PositionIn[0];\n"
  "  mat4 viewMatrix = osg_ModelViewMatrix;\n"
  "  if( distance( vec2(gl_ModelViewMatrix*v), vec2(0.0,0.0)) < 10.f){\n"
  "    float thickness = 0.015;\n" // in cm
  "    float rand1 = sin(10.1*height[0]);\n"
  "    float rand2 = cos(80.41*height[0]);\n"
  "    rand3 = (1.0+rand1)/2.0;\n" // in [0,1]
  "    Color = vec4(0.0+rand3*0.1,0.3+rand3*0.25,0.05,1.0);\n"
  "    vec4 dirBase = normalize(vec4(rand1,rand2,0.0,0.0))*thickness;\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * (v - dirBase );  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * (v + dirBase );  EmitVertex();\n"
  "    rand1 = rand1*thickness;\n"
  "    rand2 = rand2*thickness;\n"
  "    float factor = 5.0;\n" //Schiefigkeit
  "\n"
  "   vec4 vert2 = (v - dirBase + vec4(0.0,0.0,height[0]*0.33,0.0) +vec4(rand1,rand2,0.0,0.0)*height[0]*factor );\n"
  "   vec4 w_vert2 = applyWind(vert2,v_windVec_out[0]);\n"
  "   gl_Position = gl_ModelViewProjectionMatrix * w_vert2;"
  "   EmitVertex();\n"
  "\n"
  "   vec4 vert3 = (v + dirBase + vec4(0.0,0.0,height[0]*0.33,0.0) +vec4(rand1,rand2,0.0,0.0)*height[0]*factor );\n"
  "   vec4 w_vert3 = applyWind(vert3,v_windVec_out[0]);\n"
  "   gl_Position = gl_ModelViewProjectionMatrix * w_vert3;"
  "   EmitVertex();\n"
  "\n"
  "   vec4 vert4 = (v - dirBase + vec4(0.0,0.0,height[0]*0.66,0.0)+vec4(rand1,rand2,0.0,0.0)*height[0]*2*factor );\n"
  "   vec4 w_vert4 = applyWind(vert4,v_windVec_out[0]);\n"
  "   gl_Position = gl_ModelViewProjectionMatrix * w_vert4;"
  "   EmitVertex();\n"
  "\n"
  "   vec4 vert5 = (v  + dirBase + vec4(0.0,0.0,height[0]*0.66,0.0)+vec4(rand1,rand2,0.0,0.0)*height[0]*2*factor );\n"
  "   vec4 w_vert5 = applyWind(vert5,v_windVec_out[0]);\n"
  "   gl_Position = gl_ModelViewProjectionMatrix * w_vert5;"
  "   EmitVertex();\n"
  "\n"
  "   vec4 vert6 = (v  + dirBase + vec4(0.0,0.0,height[0],0.0)+vec4(rand1,rand2,0.0,0.0)*height[0]*3*factor );\n"
  "   vec4 w_vert6 = applyWind(vert6,v_windVec_out[0]);\n"
  "   gl_Position = gl_ModelViewProjectionMatrix * w_vert6;"
  "   EmitVertex();\n"
  "\n"
  "   EndPrimitive();\n"
  "}}\n"
};


static const char* fragGrass =
{
  "#version 330\n"
  "in float rand3;\n"
  "in vec4 Color;\n"
  "out vec4 outColor;\n"
  "void main(void)\n"
  "{\n"
//  "    outColor = vec4(0.0+rand3*0.1,0.3+rand3*0.25,0.05,1.0);\n"
  "  outColor = Color;\n"
  "}\n"
};













///////////////////////////////////////////////////
///////////////////////////////////////////////////
static const char* fragSource = {
  "#version 120\n"
  "#extension GL_EXT_geometry_shader4 : enable\n"
  "varying vec4 v_color_out;\n"
  "in vec4 v_color_out;\n"
  "void main(void)\n"
  "{\n"
  "    gl_FragColor = v_color_out;\n"
  "}\n"
};



/// ///////////////////////
/// vertex to rectangle

static const char* vertSource = {
  "#version 120\n"
  "#extension GL_EXT_geometry_shader4 : enable\n"
  "varying out vec4 v_color;\n"
  "void main(void)\n"
  "{\n"
  "    v_color = gl_Color;\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n"
  "}\n"
};

static const char* geomSource = {
  "#version 120\n"
  "#extension GL_EXT_geometry_shader4 : enable\n"
  "varying in vec4 v_color[];\n"
  "varying out vec4 v_color_out;\n"
  "void main(void)\n"
  "{\n"
  "    vec4 v = gl_PositionIn[0];\n"
  "    v_color_out = v_color[0];\n"
//  "    v_color_out = v + v_color[0];\n"
  "\n"
  "    gl_Position = (v + vec4(-0.5,-0.5,0.,0.) );  EmitVertex();\n"
  "    gl_Position = (v + vec4(0.5,-0.5,0.,0.) );  EmitVertex();\n"
  "    gl_Position = (v + vec4(-0.5,0.5,0.,0.) );  EmitVertex();\n"
  "    gl_Position = (v + vec4(0.5,0.5,0.,0.) );  EmitVertex();\n"
  "    EndPrimitive();\n"
  "\n"
  "}\n"
};




/// ///////////////////////
/// shaders vertex to triangle showing direction

static const char* vertSourceTriangles = {
  "#version 120\n"
 // "#extension GL_EXT_geometry_shader4 : enable\n"
  "varying out vec4 v_color;\n"
  "varying out vec3 v_dir;\n"
  "void main(void)\n"
  "{\n"
  "    v_color = gl_Color;\n"
  "    v_dir = gl_Normal;\n"
  "    gl_Position = gl_Vertex;\n"
  "}\n"
};

static const char* geomSourceTriangles = {
  "#version 120\n"
  "#extension GL_EXT_geometry_shader4 : enable\n"
  "varying in vec4 v_color[];\n"
  "varying in vec3 v_dir[];\n"
  "varying out vec4 v_color_out;\n"
  "void main(void)\n"
  "{\n"
  "  vec4 v = gl_PositionIn[0];\n"
  "  v_color_out = v_color[0];\n"
  "  vec4 direction = vec4(v_dir[0],0.0);\n"
  // camera space direction
  "  vec4 dirEye = gl_ModelViewMatrix * direction;\n"
  // negative cameraSpace coordinates of a point are the vector between camera
  // and this point. crossproduct of two vectors produces orthogonal vector.
  // we use only the third coordinate to avoid getting different ortho vectors
  // when at center and when at edge of camera. as we only use the last coordinate
  // and normalize afterwards it is sufficient to simply use (0.0,0.0,-1.0).
  "  vec3 orthoV = normalize( cross( vec3(0.0,0.0,-1.0), dirEye.rgb ) );\n"
  "  vec4 orthoV4 = vec4( orthoV, 0.0);\n"
  "  direction = normalize(direction);\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * (v + 2*direction);  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * (v - direction) + orthoV4;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * (v - direction) - orthoV4;  EmitVertex();\n"
  "    EndPrimitive();\n"
  "\n"
  "}\n"
};




/// ///////////////////////
/// shaders vertex to tetraeder showing direction

static const char* vertSourceTetra = {
  "#version 120\n"
 // "#extension GL_EXT_geometry_shader4 : enable\n"
  "varying out vec4 v_color;\n"
  "varying out vec3 v_dir;\n"
  "void main(void)\n"
  "{\n"
  "    v_color = gl_Color;\n"
  "    v_dir = gl_Normal;\n"
  "    gl_Position = gl_Vertex;\n"
  "}\n"
};

static const char* geomSourceTetra = {
  "#version 120\n"
  "#extension GL_EXT_geometry_shader4 : enable\n"
  "varying in vec4 v_color[];\n"
  "varying in vec3 v_dir[];\n"
  "varying out vec4 v_color_out;\n"
  "void main(void)\n"
  "{\n"
  "  vec4 v = gl_PositionIn[0];\n"
  "  v_color_out = v_color[0];\n"
  "  vec4 direction = normalize(vec4(v_dir[0],0.0));\n"
  "  vec4 orthoV4 = normalize(vec4(-direction[1],direction[0],0.0,0.0));\n"
  "  direction = normalize(direction);\n"
  "  vec4 tip = v+3*direction;\n"
  "  vec4 centroidBaseTri = v-direction;\n"
  "  vec3 dirCrossProd = normalize(cross(direction.xyz, orthoV4.xyz));\n"
  "  vec4 dirCrossProdV4 = vec4(dirCrossProd,0.0);\n"
  "  vec4 baseTriA = (centroidBaseTri + 2*orthoV4);\n"
  "  vec4 baseTriB = (centroidBaseTri - orthoV4 + dirCrossProdV4);\n"
  "  vec4 baseTriC = (centroidBaseTri - orthoV4 - dirCrossProdV4);\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriA;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriB;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriC;  EmitVertex();\n"
  "    EndPrimitive();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriA;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriC;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * tip;  EmitVertex();\n"
  "    EndPrimitive();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriA;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriB;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * tip;  EmitVertex();\n"
  "    EndPrimitive();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriB;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * baseTriC;  EmitVertex();\n"
  "    gl_Position = gl_ModelViewProjectionMatrix * tip;  EmitVertex();\n"
  "    EndPrimitive();\n"
  "\n"
  "}\n"
};

#endif
