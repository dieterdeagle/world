#ifndef TreeGenerator_h
#define TreeGenerator_h

#include <deque>
#include <functional>

#include "geometry/LineSegment.h"

#include "util/Randomizer.h"

#include "Functors.h"
#include "Tree.h"
#include "ValuePicker.h"

#include <osg/Image>

// -------------------------------

using ValuePickerFunc = std::function<float(const std::shared_ptr<std::default_random_engine>& randomEngine,
                                            const std::shared_ptr<BranchSegmentAttributesPre>& attributes)>;

// -------------------------------

class TreeGeneratorDistributionBased
{
public:
  class Builder;

public:
  std::shared_ptr<Tree> generateTree( int seed, const Vec3& startingPos, const Vec3& up ) const;

  std::shared_ptr<Branch> growBranch( const Vec3& startingPos,
                                      const Vec3& direction,
                                      float thickness,
                                      float length,
                                      unsigned int branchDepth ) const;

  // creates a polygon that resembles a leaf
  std::vector<Vec2> createLeaf( const LineSegment& mainAxis, const Vec2& extent ) const;

  // FIXME: own image class to decouple from osg?
  /**
   * @brief createLeavesImage
   * @param width
   * @param height
   * @param pixelSize size of a img pixel in world size
   * @return
   */
  osg::ref_ptr<osg::Image> createLeavesImage( const Vec3& startingPosW,
                                              unsigned int branchDepth,
                                              double widthW = 0.001, double heightW = 0.002,
                                              unsigned int pixelPerMeter = 512 ) const;

  void drawBranches2d( const osg::ref_ptr<osg::Image>& img,
                                    const Vec2& startingPos,
                                    const Vec2& initDir,
                                    double thickness,
                                    double length,
                                    unsigned int branchDepth ) const;


private:
  TreeGeneratorDistributionBased( ValuePicker initialThickness,
                                  ValuePicker thicknessFactor,
                                  ValuePicker initialBranchLength,
                                  ValuePicker branchLengthFactor,
                                  ValuePicker nbBranches,
                                  ValuePicker nbSegments,
                                  unsigned int maxDepth,
                                  ValuePicker branchRotation,
                                  ValuePicker segmentRotation );

private:
  // FIXME: mutables.. not a particular nice design. try to remove..
  mutable ValuePickerFunc _initialThickness;
  mutable ValuePickerFunc _thicknessFactor;
  mutable ValuePickerFunc _initialBranchLength;
  mutable ValuePickerFunc _branchLengthFactor;
  mutable ValuePickerFunc _nbBranches;
  mutable ValuePickerFunc _nbSegments;
  mutable ValuePickerFunc _branchRotation;
  mutable ValuePickerFunc _segmentRotation;

  unsigned int _maxDepth;

private:
  std::shared_ptr<std::default_random_engine> _randomEngine = std::make_shared<std::default_random_engine>();

  int _currSeed;
  Vec3 _currStartingPos = Vec3( 0.f,0.f,0.f );
  Vec3 _currUp = Vec3( 0.f, 1.f, 0.f );
};

// ----------------

class TreeGeneratorDistributionBased::Builder
{
private:
  ValuePicker _initialThickness = ValuePicker( 0.1f,
                                               ValuePicker::DistributionType::Normal,
                                               0.01f );

  ValuePicker _thicknessFactor = ValuePicker( 0.6f,
                                              ValuePicker::DistributionType::Normal,
                                              0.1f );

  ValuePicker _initialBranchLength = ValuePicker( 4.f,
                                               ValuePicker::DistributionType::Normal,
                                               0.01f );

  ValuePicker _branchLengthFactor = ValuePicker( 0.4f,
                                                 ValuePicker::DistributionType::Normal,
                                                 0.1f );


  ValuePicker _nbBranches = ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                         std::make_shared<FunctorUnaryConstant>(2),
                                         ValuePicker::DistributionType::Uniform,
                                         0.5f );

  ValuePicker _nbSegments = ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                         std::make_shared<FunctorUnaryConstant>( 5 ),
                                         ValuePicker::DistributionType::Uniform,
                                         0.5f );

  ValuePicker _branchRotation = ValuePicker( 45.f,
                                             ValuePicker::DistributionType::Normal,
                                             5.f,
                                             std::make_shared<FunctorUnaryRandomSign>() );

  ValuePicker _segmentRotation = ValuePicker( 0.f,
                                              ValuePicker::DistributionType::Normal,
                                              10.f,
                                              std::make_shared<FunctorUnaryRandomSign>() );
  unsigned int _maxDepth = 4;

public:
  TreeGeneratorDistributionBased::Builder& setInitialThickness( const ValuePicker& initialThickness );
  TreeGeneratorDistributionBased::Builder& setThicknessFactor( const ValuePicker& thicknessFactor );
  TreeGeneratorDistributionBased::Builder& setInitialBranchLength( const ValuePicker& initialBranchLength );
  TreeGeneratorDistributionBased::Builder& setBranchLengthFactor( const ValuePicker& branchLengthFactor );
  TreeGeneratorDistributionBased::Builder& setNbBranches( const ValuePicker& nbBranches );
  TreeGeneratorDistributionBased::Builder& setNbSegments( const ValuePicker& nbSegments );
  TreeGeneratorDistributionBased::Builder& setBranchRotation( const ValuePicker& branchRotation );
  TreeGeneratorDistributionBased::Builder& setSegmentRotation( const ValuePicker& segmentRotation );
  TreeGeneratorDistributionBased::Builder& setMaxDepth( unsigned int maxDepth );


  TreeGeneratorDistributionBased build()
  {
    return TreeGeneratorDistributionBased( _initialThickness,
                                           _thicknessFactor,
                                           _initialBranchLength,
                                           _branchLengthFactor,
                                           _nbBranches,
                                           _nbSegments,
                                           _maxDepth,
                                           _branchRotation,
                                           _segmentRotation );
  }

};

// -------------------------------

namespace TreeGeneratorHelpers
{
  TreeGeneratorDistributionBased randomTreeGenerator( int seed = -1 );
}

// -------------------------------

#endif
