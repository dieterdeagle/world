
#include <iostream>

#include <osg/Image>
#include <osgDB/WriteFile>

#include "geometry/LineSegment.h"

#include "TreeGen/Leaves.h"

#include "TreeGen/Functors.h"
#include "TreeGen/Tree.h"
#include "TreeGen/TreeGenerator.h"

int main()
{
//  auto pgm = createLeaveShaderTest();
//  auto img = createLeavesImg();

//  LineSegment lineSeg1( Vec2(-1,-1), Vec2(1,1) );
//  LineSegment lineSeg2( Vec2(-2,-1), Vec2(-2,1) );
//  auto dist = LineUtility::distance( lineSeg1, lineSeg2 );

//  std::cout << "dist: " << dist << "\n";

//  std::cout << LineUtility::distance( lineSeg1, lineSeg2.p1() )
//               << "," << LineUtility::distance( lineSeg1, lineSeg2.p2() )
//                  << "," << LineUtility::distance( lineSeg2, lineSeg1.p1() )
//                     << "," << LineUtility::distance( lineSeg2, lineSeg1.p2() ) << "\n";

//  std::cout << "intersect: " << LineUtility::intersecting( LineSegment( Vec2(-1,-1), Vec2(1,1) ),
//                                                           LineSegment( Vec2(-1,1), Vec2(1,-1) ) )
//               << "\n not: " << LineUtility::intersecting( LineSegment( Vec2(-1,-1), Vec2(1,1) ),
//                                                           LineSegment( Vec2(-2,-11), Vec2(-2,1) ) )
//               << " obacht: " << LineUtility::intersecting( LineSegment( Vec2(-1,-1), Vec2(-1,1) ),
//                                                                    LineSegment( Vec2(-2,-1), Vec2(-2,1) ) )
//                  << "\n";

//  std::cout << "intersection: " << LineUtility::findLineIntersection( LineSegment( Vec2(-1,-1), Vec2(1,1) ),
//                                                                      LineSegment( Vec2(-1,1), Vec2(1,-1) ) )
//               << " oterh: " << LineUtility::findLineIntersection( LineSegment( Vec2(-1,-1), Vec2(1,1) ),
//                                                                   LineSegment( Vec2(-2,-11), Vec2(-2,1) ) )
//               << " obacht: " << LineUtility::findLineIntersection( LineSegment( Vec2(-1,-1), Vec2(-1,1) ),
//                                                                    LineSegment( Vec2(-2,-1), Vec2(-2,1) ) )
//               << "\n";




  //-----------------------
  // leaves test

  TreeGeneratorDistributionBased::Builder builder;
  builder.setNbBranches( ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                      std::make_shared<FunctorUnaryConstant>(2),
                                      ValuePicker::DistributionType::Uniform,
                                      0.5f,
                                      std::make_shared<FunctorUnarySetToZero>(0.1f) ) )
         .setInitialThickness( ValuePicker( 0.00005f,
                                            ValuePicker::DistributionType::Normal,
                                            0.000002f ) )
         .setThicknessFactor( ValuePicker( 0.45f,
                                           ValuePicker::DistributionType::Normal,
                                           0.15f ) )
         .setInitialBranchLength( ValuePicker( 0.001f,
                                               ValuePicker::DistributionType::Normal,
                                               0.00001f ) )
         .setBranchLengthFactor( ValuePicker( 0.4f,
                                              ValuePicker::DistributionType::Normal,
                                              0.01f ) )
         .setBranchRotation( ValuePicker( 40.f,
                                          ValuePicker::DistributionType::Uniform,
                                          15.f,
                                          std::make_shared<FunctorUnaryRandomSign>() ) )
         .setNbSegments( ValuePicker( ValuePicker::AttributeType::BranchDepth,
                                      std::make_shared<FunctorLinear>( std::make_pair(0.f,10.f), std::make_pair(1.f,4.f) ),
                                      ValuePicker::DistributionType::Uniform,
                                      0.5f ) );
  TreeGeneratorDistributionBased generator = builder.setMaxDepth(3).build();

  auto leavesImg = generator.createLeavesImage( Vec3(0.0,0.0,0.0), 3 );
  osgDB::writeImageFile( *leavesImg, "/tmp/leavesTest.png" );

  // leaves test end
  //-----------------------


  return 0;
}
