#ifndef PlaneTerrain_h
#define PlaneTerrain_h

#include <functional>

#include "util/Field.h"
#include "util/Vec2.h"
#include "util/Vec3.h"

// ----------------------------------------------------------------------------

struct PlaneTerrainProperties
{
  PlaneTerrainProperties( double heightScaling )
    : _heightScaling(heightScaling)
  {}
  double _heightScaling = 4.0;
};

// ----------------------------------------------------------------------------

class PlaneTerrain
{
public:
  PlaneTerrain( const Vec2& minPos, const Vec2& maxPos,
                double heightScaling,
                int seed = 0 );

  /**
   * @brief getHeightAt returns height at world pos.
   * @param pos
   * @return
   */
  double getHeightAt( const Vec2& posW ) const;
  Vec3 getNormalAt( const Vec2& posW ) const;
  Vec2 getGradAt( const Vec2& posW ) const;

  void setUp( const Vec3& up ){ _up = up; }
  Vec3 getUp() const { return _up; }

  double getEPSILON() const { return _EPSILON; }

  double getWidth() const { return _maxPos.getX() -  _minPos.getX(); }
  double getDepth() const { return _maxPos.getY() -  _minPos.getY(); }

private:
  Vec2 _minPos;
  Vec2 _maxPos;

  Field<double> _propertiesField;
  Field<double> _groundHeightField;

  double _heightScaling;

  std::function<double(const Vec2&)> _heightWorldScale;
  std::function<double(const Vec2&)> _heightContinentScale;
  std::function<double(const Vec2&)> _heightCountryScale;
  std::function<double(const Vec2&)> _heightRegionScale;
  std::function<double(const Vec2&)> _heightParcScale;
  std::function<double(const Vec2&)> _heightSiedlungScale;
  std::function<double(const Vec2&)> _heightGardenScale;
  std::function<double(const Vec2&)> _heightDirtScale;

  std::function<double(const Vec2&)> _heightFunctor1;
  std::function<double(const Vec2&)> _heightFunctor2;
  std::function<double(const Vec2&)> _heightFunctor3;

  Vec3 _up{ 0.0,0.0,1.0 };

  static const double _EPSILON;
};

// ----------------------------------------------------------------------------

#endif
