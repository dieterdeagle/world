#include "PlaneTerrain.h"

#include <iostream>
#include <limits>
#include <random>
#include <utility>

#include "util/noise/FractionalBrownianMotion.h"
#include "util/noise/PerlinNoise.h"

// ----------------------------------------------------------------------------

const double PlaneTerrain::_EPSILON = 0.0001;

// ----------------------------------------------------------------------------

PlaneTerrain::PlaneTerrain( const Vec2& minPos, const Vec2& maxPos,
                            double heightScaling,
                            int seed )
  : _minPos(minPos), _maxPos(maxPos), _heightScaling(heightScaling)
{

  std::uniform_int_distribution<int> distUniInt;
  std::normal_distribution<double> distGroundHeights( 0.5,3.0 );
  std::gamma_distribution<double> distGamma(2.0,1.0);
//  std::lognormal_distribution<double> dist( 0.0, 1.0 );
//  std::normal_distribution<double> dist( heightScaling, 3.5 );
  std::mt19937 randomEngine( seed );

  auto size = (maxPos - minPos);
  auto maxSizeComp = std::ceil( std::max( size.getX(), size.getY() ) );
  std::cout << "maxsizecomp: " << maxSizeComp << "\n";



  int TEST_OCT = 3;

  // world scale
  int NB_GLOBAL_FEATURES = 5;
  double detailWorld = static_cast<double>(NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmWorldScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailWorld, 0.5, 2.0 );
  _heightWorldScale = fbmWorldScale;

  // "continent" scale
  int NB_CONTINENT_FEATURES = 10;
  double detailContinent = static_cast<double>(NB_CONTINENT_FEATURES * NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmContinentScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailContinent, 0.5, 2.0 );
  _heightContinentScale = fbmContinentScale;

  // "country" scale
  int NB_COUNTRY_FEATURES = 15;
  double detailCountry = static_cast<double>(NB_COUNTRY_FEATURES * NB_CONTINENT_FEATURES * NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmCountryScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailCountry, 0.5, 2.0 );
  _heightCountryScale = fbmCountryScale;

  // region scale
  int NB_REGION_FEATURES = 12;
  double detailRegion = static_cast<double>(NB_REGION_FEATURES * NB_COUNTRY_FEATURES * NB_CONTINENT_FEATURES * NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmRegionScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailRegion, 0.5, 2.0 );
  _heightRegionScale = fbmRegionScale;

  int NB_PARC_FEATURES = 8;
  double detailParc = static_cast<double>(NB_PARC_FEATURES * NB_REGION_FEATURES * NB_COUNTRY_FEATURES * NB_CONTINENT_FEATURES * NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmParcScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailParc, 0.5, 2.0 );
  _heightParcScale = fbmParcScale;

  int NB_SIEDLUNG_FEATURES = 8;
  double detailSiedlung = static_cast<double>(NB_SIEDLUNG_FEATURES * NB_PARC_FEATURES * NB_REGION_FEATURES * NB_COUNTRY_FEATURES * NB_CONTINENT_FEATURES * NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmSiedlungScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailSiedlung, 0.5, 2.0 );
  _heightSiedlungScale = fbmSiedlungScale;

  int NB_GARDEN_FEATURES = 8;
  double detailGarden = static_cast<double>(NB_GARDEN_FEATURES * NB_SIEDLUNG_FEATURES * NB_PARC_FEATURES * NB_REGION_FEATURES * NB_COUNTRY_FEATURES * NB_CONTINENT_FEATURES * NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmGardenScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailGarden, 0.5, 2.0 );
  _heightGardenScale = fbmGardenScale;

  int NB_DIRT_FEATURES = 75;
  double detailDirt = static_cast<double>(NB_DIRT_FEATURES * NB_GARDEN_FEATURES * NB_SIEDLUNG_FEATURES * NB_PARC_FEATURES * NB_REGION_FEATURES * NB_COUNTRY_FEATURES * NB_CONTINENT_FEATURES * NB_GLOBAL_FEATURES) / maxSizeComp;
  FbmFunctor<Vec2> fbmDirtScale( distUniInt(randomEngine), maxSizeComp, maxSizeComp, TEST_OCT, detailDirt, 0.5, 2.0 );
  _heightDirtScale = fbmDirtScale;



  int NB_FIELD_POSITIONS_PER_DIM = 100;
  std::vector<double> coordsX;
  coordsX.reserve(NB_FIELD_POSITIONS_PER_DIM);
  std::vector<double> coordsY;
  coordsY.reserve(NB_FIELD_POSITIONS_PER_DIM);
  for( int i = 0; i < NB_FIELD_POSITIONS_PER_DIM; ++i )
  {
    coordsX.push_back( _minPos.getX() + (_maxPos.getX()-_minPos.getX()) * static_cast<double>(i) / (NB_FIELD_POSITIONS_PER_DIM-1) );
    coordsY.push_back( _minPos.getY() + (_maxPos.getY()-_minPos.getY()) * static_cast<double>(i) / (NB_FIELD_POSITIONS_PER_DIM-1) );
  }
  std::vector<double> fieldData;
  std::vector<double> groundHeightFieldData;
  fieldData.reserve( NB_FIELD_POSITIONS_PER_DIM * NB_FIELD_POSITIONS_PER_DIM );
  for( int i = 0; i < NB_FIELD_POSITIONS_PER_DIM * NB_FIELD_POSITIONS_PER_DIM; ++i )
  {
    auto val = distGamma( randomEngine );
    fieldData.push_back( val );
    groundHeightFieldData.push_back( distGroundHeights(randomEngine) );

//    std::cout << "field val: " << val << "\n";
  }
  _propertiesField = Field<double>( fieldData.begin(), fieldData.end(),
                                    coordsX, coordsY );
  _groundHeightField = Field<double>( groundHeightFieldData.begin(), groundHeightFieldData.end(),
                                      coordsX, coordsY );

}

// ----------------------------------------------------------------------------

double PlaneTerrain::getHeightAt( const Vec2& posW ) const
{
//  double MOUNTAIN_SCALE = 3.5;
//  double MOUNTAIN_FACTOR = 1/5.0;
//  double HILL_SCALE = 0.5;
//  double HILL_FACTOR = 1/50.0;

//  auto groundHeight = _groundHeightField.interpolate( posW );
//  auto groundHeight = _heightFunctor1( posW );
//  return groundHeight;


  return _heightWorldScale( posW ) * 18.0 // 18km
      + _heightContinentScale( posW ) * 4.0 // 4km
      + _heightCountryScale( posW ) * 0.6 // 600m
      + _heightRegionScale( posW ) * 0.08 // 80m
      + _heightParcScale( posW ) * 0.02 // 20m
      + _heightSiedlungScale( posW ) * 0.002 // 2m
      + _heightGardenScale( posW ) * 0.0005 // 50cm
      + _heightDirtScale( posW ) * 0.00005; // 5cm

}

// ----------------------------------------------------------------------------

Vec3 PlaneTerrain::getNormalAt( const Vec2& posW ) const
{
  Vec2 posXMinus( posW.getX() - _EPSILON, posW.getY() );
  Vec2 posXPlus( posW.getX() + _EPSILON, posW.getY() );
  auto heightPosXMinus = this->getHeightAt( posXMinus );
  auto heightPosXPlus = this->getHeightAt( posXPlus );
  Vec3 gradX = Vec3( posXPlus.getX(), posXPlus.getY(), heightPosXPlus )
      - Vec3( posXMinus.getX(), posXMinus.getY(), heightPosXMinus );

  Vec2 posYMinus( posW.getX(), posW.getY() - _EPSILON );
  Vec2 posYPlus( posW.getX(), posW.getY() + _EPSILON );
  auto heightPosYMinus = this->getHeightAt( posYMinus );
  auto heightPosYPlus = this->getHeightAt( posYPlus );
  Vec3 gradY = Vec3( posYPlus.getX(), posYPlus.getY(), heightPosYPlus )
      - Vec3( posYMinus.getX(), posYMinus.getY(), heightPosYMinus );

  auto normal = gradX.cross( gradY );

  return normal.normalized();
}

// ----------------------------------------------------------------------------

Vec2 PlaneTerrain::getGradAt( const Vec2& posW ) const
{
  Vec2 posXMinus( posW.getX() - _EPSILON, posW.getY() );
  Vec2 posXPlus( posW.getX() + _EPSILON, posW.getY() );
  auto heightPosXMinus = this->getHeightAt( posXMinus );
  auto heightPosXPlus = this->getHeightAt( posXPlus );
  Vec3 gradX = Vec3( posXPlus.getX(), posXPlus.getY(), heightPosXPlus )
      - Vec3( posXMinus.getX(), posXMinus.getY(), heightPosXMinus );

  Vec2 posYMinus( posW.getX(), posW.getY() - _EPSILON );
  Vec2 posYPlus( posW.getX(), posW.getY() + _EPSILON );
  auto heightPosYMinus = this->getHeightAt( posYMinus );
  auto heightPosYPlus = this->getHeightAt( posYPlus );
  Vec3 gradY = Vec3( posYPlus.getX(), posYPlus.getY(), heightPosYPlus )
      - Vec3( posYMinus.getX(), posYMinus.getY(), heightPosYMinus );

  return Vec2( std::abs(heightPosXPlus-heightPosXMinus), std::abs(heightPosYPlus-heightPosYMinus) );
}

// ----------------------------------------------------------------------------
