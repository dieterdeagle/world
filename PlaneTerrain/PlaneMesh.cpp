#include "PlaneMesh.h"

#include <iostream>
#include <limits>

// ----------------------------------------------------------------------------

PlaneMesh::PlaneMesh( const std::vector<std::vector<Vec3> >& mesh,
                      const std::vector<std::vector<Vec3> >& normals )
  : _mesh(mesh), _normals(normals)
{
  std::cout << "creating PlaneMesh\n";

  _minPos = Vec2( std::numeric_limits<double>::max(),
                  std::numeric_limits<double>::max() );
  _maxPos = Vec2( std::numeric_limits<double>::lowest(),
                  std::numeric_limits<double>::lowest() );
  for( const auto& row : _mesh )
  {
    for( const auto& pos : row )
    {
      _minPos[0] = std::min( _minPos[0], pos[0] );
      _minPos[1] = std::min( _minPos[1], pos[1] );

      _maxPos[0] = std::max( _maxPos[0], pos[0] );
      _maxPos[1] = std::max( _maxPos[1], pos[1] );
    }
  }

  std::cout << "... " << _minPos << ", " << _maxPos << "\n";
}

// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------

//double PlaneMesh::getHeightAt( const Vec2& pos )
//{

//}

// ----------------------------------------------------------------------------
