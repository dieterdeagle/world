#include "PlaneTerrainMeshCreator.h"

#include <iostream>
#include <limits>

// ----------------------------------------------------------------------------

PlaneTerrainMeshCreator::PlaneTerrainMeshCreator( unsigned int nbSamplesPerUnit )
  : _nbSamplesPerUnit(nbSamplesPerUnit)
{

}

// ----------------------------------------------------------------------------

PlaneMesh PlaneTerrainMeshCreator::createPatchMesh( const PlaneTerrain& terrain,
                                                    const Vec2& minPos,
                                                    const Vec2& maxPos ) const
{
  auto mesh = this->createPatch( terrain, minPos, maxPos );
  auto normals = this->createPatchNormals( terrain, minPos, maxPos );

  return PlaneMesh( mesh, normals );
}

// ----------------------------------------------------------------------------

std::vector<std::vector<Vec3> > PlaneTerrainMeshCreator::createPatch( const PlaneTerrain& terrain,
                                                                      const Vec2& minPos,
                                                                      const Vec2& maxPos ) const
{

  /// FIXME: find more elegant solution than +0.00001 to avoid numerical problems
  std::size_t nbSamplesX = ( (maxPos.getX() - minPos.getX()) +0.00001 ) * _nbSamplesPerUnit;
  std::size_t nbSamplesY = ( (maxPos.getY() - minPos.getY()) +0.00001 ) * _nbSamplesPerUnit;

  std::cout << "samples: " << nbSamplesX << "," << nbSamplesY << "\n";
  std::vector< std::vector<Vec3> > patch{ nbSamplesX,
        std::vector<Vec3>{ nbSamplesY, Vec3(0,0,0) } };

  double stepsizeX = ( maxPos.getX() - minPos.getX() ) / (nbSamplesX - 1);
  double stepsizeY = ( maxPos.getY() - minPos.getY() ) / (nbSamplesY - 1);

  Vec2 currPos = minPos;
  for( unsigned int i = 0; i < nbSamplesX; ++i )
  {
    currPos[0] = minPos.getX() + stepsizeX * i;
    for( unsigned int j = 0; j < nbSamplesY; ++j )
    {
//      std::cout << i << "," << j << "\n";
      currPos[1] = minPos.getY() + stepsizeY * j;
      auto currHeight = terrain.getHeightAt( currPos );
      patch[i][j] = Vec3( currPos.getX(), currPos.getY(), currHeight );
    }
  }

  return patch;
}

// ----------------------------------------------------------------------------

std::vector<std::vector<Vec3> > PlaneTerrainMeshCreator::createPatchNormals( const PlaneTerrain& terrain,
                                                                             const Vec2& minPos,
                                                                             const Vec2& maxPos ) const
{
  std::size_t nbSamplesX = ( maxPos.getX() - minPos.getX() +0.00001 ) * _nbSamplesPerUnit;
  std::size_t nbSamplesY = ( maxPos.getY() - minPos.getY() +0.00001 ) * _nbSamplesPerUnit;

  std::vector< std::vector<Vec3> > patch{ nbSamplesX,
        std::vector<Vec3>{ nbSamplesY, Vec3(0,0,0) } };

  double stepsizeX = ( maxPos.getX() - minPos.getX() ) / (nbSamplesX - 1);
  double stepsizeY = ( maxPos.getY() - minPos.getY() ) / (nbSamplesY - 1);

  Vec2 currPos = minPos;
  for( unsigned int i = 0; i < nbSamplesX; ++i )
  {
    currPos[0] = minPos.getX() + stepsizeX * i;
    for( unsigned int j = 0; j < nbSamplesY; ++j )
    {
      currPos[1] = minPos.getY() + stepsizeY * j;
      auto currNormal = terrain.getNormalAt( currPos );
      patch[i][j] = currNormal;
    }
  }

  return patch;
}

// ----------------------------------------------------------------------------

std::vector<std::vector<Vec3> > PlaneTerrainMeshCreator::createPatchNormalMap( const PlaneTerrain& terrain,
                                                                               const Vec2& minPos,
                                                                               const Vec2& maxPos,
                                                                               double sampleFactor ) const
{
  std::size_t nbSamplesX = ( maxPos.getX() - minPos.getX() ) * _nbSamplesPerUnit * sampleFactor;
  std::size_t nbSamplesY = ( maxPos.getY() - minPos.getY() ) * _nbSamplesPerUnit * sampleFactor;

  std::vector< std::vector<Vec3> > patch{ nbSamplesX,
        std::vector<Vec3>{ nbSamplesY, Vec3(0,0,0) } };

  double stepsizeX = ( maxPos.getX() - minPos.getX() ) / (nbSamplesX - 1);
  double stepsizeY = ( maxPos.getY() - minPos.getY() ) / (nbSamplesY - 1);

  Vec2 currPos = minPos;
  for( unsigned int i = 0; i < nbSamplesX; ++i )
  {
    currPos[0] = minPos.getX() + stepsizeX * i;
    for( unsigned int j = 0; j < nbSamplesY; ++j )
    {
      currPos[1] = minPos.getY() + stepsizeY * j;
      auto currNormal = terrain.getNormalAt( currPos );
      patch[i][j] = currNormal;
    }
  }

  return patch;
}

// ----------------------------------------------------------------------------
