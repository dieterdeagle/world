#ifndef PlaneTerrain_PlaneMesh_h
#define PlaneTerrain_PlaneMesh_h

#include <iostream>
#include <vector>

#include "util/Vec2.h"
#include "util/Vec3.h"

/**
 * @brief The PlaneMesh class handles meshes of PlaneTerrain
 */
class PlaneMesh
{
public:
  PlaneMesh( const std::vector< std::vector<Vec3> >& mesh,
             const std::vector< std::vector<Vec3> >& normals );

  /// TODO
//  double getHeightAt( const Vec2& pos );

  std::vector< std::vector<Vec3> > getMesh() const { return _mesh; }
  std::vector< std::vector<Vec3> > getNormals() const { return _normals; }

  Vec2 getMinPos() const { return _minPos; }
  Vec2 getMaxPos() const { return _maxPos; }

private:
  std::vector< std::vector<Vec3> > _mesh;
  std::vector< std::vector<Vec3> > _normals;

  Vec2 _minPos{0,0};
  Vec2 _maxPos{0,0};
};

#endif
