#include "Patch.h"

// ----------------------------------------------------------------------------

Patch::Patch( unsigned int id,
              const PlaneMesh& mesh,
              const std::vector<std::shared_ptr<Tree> >& plants )
  : _id(id), _mesh(mesh), _plants(plants)
{
}

// ----------------------------------------------------------------------------
