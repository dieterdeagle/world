#ifndef PlaneTerrain_PlaneMeshGenerator_h
#define PlaneTerrain_PlaneMeshGenerator_h

#include <vector>

#include <util/Vec2.h>
#include <util/Vec3.h>

#include "PlaneTerrain.h"
#include "PlaneMesh.h"

class PlaneTerrainMeshCreator
{
public:
  PlaneTerrainMeshCreator( unsigned int nbSamplesPerUnit );

  PlaneMesh createPatchMesh( const PlaneTerrain& terrain,
                             const Vec2& minPos,
                             const Vec2& maxPos ) const;

private:
  /**
   * @brief createPatch
   * @param terrain
   * @param minPos, maxPos bounds of the patch to be created in terrain-space.
   * @param nbSamplesPerAxis number of samples used to sample terrain heights.
   * @return 2d array of 3d positions.
   */
  std::vector< std::vector<Vec3> > createPatch( const PlaneTerrain& terrain,
                                                const Vec2& minPos,
                                                const Vec2& maxPos ) const;

  std::vector< std::vector<Vec3> > createPatchNormals( const PlaneTerrain& terrain,
                                                       const Vec2& minPos,
                                                       const Vec2& maxPos ) const;

  std::vector< std::vector<Vec3> > createPatchNormalMap( const PlaneTerrain& terrain,
                                                         const Vec2& minPos,
                                                         const Vec2& maxPos,
                                                         double sampleFactor = 4.0 ) const;

private:
  unsigned int _nbSamplesPerUnit;
};

#endif
