#ifndef PlaneTerrain_Patch_h
#define PlaneTerrain_Patch_h

#include "TreeGen/Tree.h"

#include "PlaneMesh.h"

class Patch
{
public:
  Patch( unsigned int id,
         const PlaneMesh& mesh,
         const std::vector< std::shared_ptr<Tree> >& plants );

  unsigned int getId() const { return _id; }

  PlaneMesh getMesh() const { return _mesh; }
  std::vector< std::shared_ptr<Tree> > getPlants() const { return _plants; }

  Vec2 getMinPos() const { return _mesh.getMinPos(); }
  Vec2 getMaxPos() const { return _mesh.getMaxPos(); }

private:
  unsigned int _id;

  PlaneMesh _mesh;
  std::vector< std::shared_ptr<Tree> > _plants;

};

#endif
