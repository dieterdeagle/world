
#include <osg/Geode>
#include <osg/StateSet>
#include <osgViewer/Viewer>
#include <osgViewer/View>
#include <osgViewer/ViewerEventHandlers>

#include "graphics/RenderUtil.h"

#include "TreeGen/Functors.h"
#include "TreeGen/Tree.h"
#include "TreeGen/TreeGenerator.h"


#include "util/testsUtil.h"

#include <osg/Geode>
#include <osgViewer/Viewer>

// ----------------

//osg::ref_ptr<osg::Geode> createDemoTreeGeode( unsigned int nbRows, float spaceBetweenTrees = 1.f )
//{
//  std::vector<TreeGeneratorSimple> treeGenerators;
//  float branchiness = 0.8;
//  float directionalDeviation = 0.6;
//  float branchDev = 0.2;
//  float branchLengthFactor = 0.5;
//  uint maxDepth = 4;
//  TreeGeneratorSimple treeGen0( branchiness, directionalDeviation, branchLengthFactor, branchDev, maxDepth );
//  treeGenerators.push_back( treeGen0 );

//  TreeGeneratorSimple treeGen1( std::make_shared<AdapterUnary>( std::make_shared<FunctorLinear>( std::make_pair(0.f,0.1f), std::make_pair(1.f,1.f) ),
//                                                          std::make_shared<ExtractBranchDepth>() ),
//                          std::make_shared<AdapterUnary>( std::make_shared<FunctorLinear>( std::make_pair(0.f,0.1f), std::make_pair(1.f,1.f) ),
//                                                          std::make_shared<ExtractBranchDepth>() ),
//                          std::make_shared<AdapterUnary>( std::make_shared<FunctorLinear>( std::make_pair(0.f,0.1f), std::make_pair(1.f,1.f) ),
//                                                          std::make_shared<ExtractBranchDepth>() ),
//                          std::make_shared<AdapterUnary>( std::make_shared<FunctorLinear>( std::make_pair(0.f,0.1f), std::make_pair(1.f,1.f) ),
//                                                          std::make_shared<ExtractBranchDepth>() ),
//                          maxDepth );

//  treeGenerators.push_back( treeGen1 );


//  std::vector< std::shared_ptr<Tree> > forest;


//  float distanceBetweenTrees = 2.f;
//  for( std::size_t i = 0; i < treeGenerators.size(); ++i )
//  {
//    for( std::size_t j = 0; j < nbRows; ++j )
//    {
//      std::cout << "seed: " << j+i*treeGenerators.size() << ", at: " << i << "," << j << std::endl;
//      forest.push_back( treeGenerators[i].generateTree( vec3f(i*spaceBetweenTrees,j*spaceBetweenTrees,0.0)*distanceBetweenTrees, 1+j+i*nbRows ) );
//    }
//  }

//  osg::ref_ptr<osg::Geode> forestGeode = getForestGeode( forest );

//  return forestGeode;
//}

// ----------------------------------------------------------------------------

osg::ref_ptr<osg::Geode> createDemoTreeGeodeDistributionBased( int nbRows = 2,
                                                               float spaceBetweenTrees = 4.f,
                                                               int seed = -1 )
{

  //  TreeGeneratorDistributionBased::Builder builder;
//  builder.setNbBranches( ValuePicker( ValuePicker::AttributeType::BranchDepth,
//                                      std::make_shared<FunctorUnaryConstant>(1),
//                                      ValuePicker::DistributionType::Uniform,
//                                      0.5f,
//                                      std::make_shared<FunctorUnarySetToZero>(0.1f) ) )
//         .setInitialThickness( ValuePicker( 0.00005f,
//                                            ValuePicker::DistributionType::Normal,
//                                            0.000002f ) )
//         .setThicknessFactor( ValuePicker( 0.45f,
//                                           ValuePicker::DistributionType::Normal,
//                                           0.15f ) )
//         .setInitialBranchLength( ValuePicker( 0.001f,
//                                               ValuePicker::DistributionType::Normal,
//                                               0.00001f ) )
//         .setBranchLengthFactor( ValuePicker( 0.4f,
//                                              ValuePicker::DistributionType::Normal,
//                                              0.01f ) )
//         .setBranchRotation( ValuePicker( 40.f,
//                                          ValuePicker::DistributionType::Uniform,
//                                          15.f,
//                                          std::make_shared<FunctorUnaryRandomSign>() ) )
//         .setNbSegments( ValuePicker( ValuePicker::AttributeType::BranchDepth,
//                                      std::make_shared<FunctorLinear>( std::make_pair(0.f,7.5f), std::make_pair(1.f,3.f) ),
//                                      ValuePicker::DistributionType::Uniform,
//                                      0.5f ) );
//  TreeGeneratorDistributionBased generator = builder.setMaxDepth(5).build();

  auto generator = TreeGeneratorHelpers::randomTreeGenerator( seed );

  std::vector< std::shared_ptr<Tree> > forest;

  float distanceBetweenTrees = 2.f;

  for( int j = 0; j < nbRows; ++j )
  {
    int seed = j;
    std::cout << "seed: " << seed << ", at: " << j << std::endl;
    forest.push_back( generator.generateTree( seed,
                                              Vec3(0.f,j*spaceBetweenTrees,0.0)*distanceBetweenTrees,
                                              Vec3(0.f,0.f,1.f) ) );
  }

  osg::ref_ptr<osg::Geode> forestGeode = getForestGeode( forest );



  return forestGeode;
}

// ----------------------------------------------------------------------------

int main( int argc, char* argv[] )
{

  osgViewer::Viewer viewer;
  std::vector<osgViewer::View*> views;
  viewer.getViews(views);
  for( auto view : views )
  {
    view->addEventHandler( new osgViewer::StatsHandler );

  }

  viewer.setUpViewInWindow( 100, 100, 1024, 512 );
  viewer.getCamera()->setClearColor( osg::Vec4(1.0,1.0,1.0,1.0) );


  int seed = -1;
  if( argc > 1 )
    seed = std::atoi( argv[1] );

  auto spaceBetweenTrees = 0.01f;
  if( argc > 2 )
    spaceBetweenTrees = std::atof( argv[2] );

//  osg::ref_ptr<osg::Geode> forestGeode = createDemoTreeGeode( 4,4.f );
  osg::ref_ptr<osg::Geode> forestGeode = createDemoTreeGeodeDistributionBased( 20,spaceBetweenTrees, seed );
  viewer.setSceneData( forestGeode );

  viewer.run();

//  UtilTests::matrixTests();
//  UtilTests::rotationTests();

  return 0;
}

// ----------------------------------------------------------------------------
