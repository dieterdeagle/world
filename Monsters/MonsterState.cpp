#include "MonsterState.h"

// ----------------------------------------------------------------------------
// --- Stunned ----------------------------------------------------------------

Monster::State::Stunned::Stunned( int nbTurns )
  : _nbRemainingTurns(nbTurns)
{

}

// -------------------------------------------------
void Monster::State::Stunned::update( int nbTurnsPassed )
{
  _nbRemainingTurns -= nbTurnsPassed;
}
