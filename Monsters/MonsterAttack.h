#ifndef Monsters_MonsterAttack_h
#define Monsters_MonsterAttack_h

#include <random>
#include <string>

//#include "MonsterBase.h"
//#include "GlobalVars.h"


namespace AttackDetail
{
static unsigned int NEXT_ID = 0;

static const unsigned int ID_STRENGTH = NEXT_ID++;
static const unsigned int ID_AGILITY = NEXT_ID++;

static const unsigned int ID_MAXHEALTHPTS = NEXT_ID++;

static const unsigned int ID_RESISTANCE_GENERAL = NEXT_ID++;

static const unsigned int ID_RESISTANCE_FIRE = NEXT_ID++;
static const unsigned int ID_RESISTANCE_WATER = NEXT_ID++;

//std::map<unsigned int, std::string> idNameMap();

}

// --------------------------------------------------

namespace Monster
{

// fwd decl
class MonsterBase;


class Attack
{
public:
  class Damage
  {
  public:
    Damage( double neutral, double fire = 0.0, double water = 0.0 );

    double getNeutral() const { return _neutral; }
    double getFire() const { return _fire; }
    double getWater() const { return _water; }

  private:
    double _neutral;
    double _fire;
    double _water;
  };

public:
  Attack( const std::string& name,
          double baseDamage, double strengthFactor,
          double agilityFactor, double luckFactor );

  std::string getName() const { return _name; }

  double getBaseDamage() const { return _baseDamage; }
  double getFactorStrength() const { return _factorsAttributes[AttackDetail::ID_STRENGTH]; }
  double getFactorAgility() const { return _factorsAttributes[AttackDetail::ID_AGILITY]; }

  // Fixme: Fixed ids
  double getFactorNeutral() const { return _factorsTypes[0]; }
  double getFactorFire() const { return _factorsTypes[1]; }
  double getFactorWater() const { return _factorsTypes[2]; }

private:
  std::string _name; //= "Tackle";

  double _costs = 1.0;

  double _baseDamage;// = 1.0;

  // contains strengthFactor, agilityFactor etc that describe the influence of these parameters
  std::vector<double> _factorsAttributes = std::vector<double>( AttackDetail::NEXT_ID, 1.0 );
//  double _strengthFactor = 0.0;
//  double _agilityFactor = 0.0;

  // Todo: build normal distribution around dmg based on this factor
//  double _luckFactor = 1.0;

  // Todo: Fixed ids. (Create types class or something similar)
  // atm: neutral=0, fire=1, water=2
  std::vector<double> _factorsTypes{ 1.0, 0.0, 0.0 };
//  double _neutralFactor = 1.0;
//  double _fireFactor = 0.0;
//  double _waterFactor = 0.0;
};

Attack createTackle();

}

#endif
