#ifndef Monsters_MonsterGenerator_h
#define Monsters_MonsterGenerator_h

#include <algorithm>
#include <chrono>
#include <functional>
#include <iostream>
#include <memory>
#include <random>

#include "MonsterBase.h"

template <typename Distribution>
class MonsterGenerator
{
public:
  MonsterGenerator( double baseValue,
                    double width,
                    std::function<std::string()> nameGenerator,
                    int seed = -1 );

  // -----------------

  std::shared_ptr<MonsterBase> generate() const
  {
    auto m = MonsterDetail::idNameMap();

    std::vector<double> attributes; //, _valueGenerator );
    for( std::size_t i = 0; i < m.size(); ++i )
    {
      auto val = _valueGenerator();
      attributes.push_back( val );
    }

    //    std::generate( attributes.begin(), attributes.end(), _valueGenerator );

    return std::make_shared<MonsterBase>( _nameGenerator(), attributes  /*todo: attacks*/ );
  }

private:
  void init( int seed )
  {
    std::cout << "init for: " << seed << "\n";
    if( seed < 0 )
      _twister.seed( std::time(0) );
    else
      _twister.seed( seed );

    _valueGenerator = std::bind( _distr, _twister );
  }

private:
  double _baseValue;

  std::mt19937 _twister;
  Distribution _distr;

  std::function<std::string()> _nameGenerator;
  std::function<double()> _valueGenerator;

};

#endif
