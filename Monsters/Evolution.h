#ifndef Monsters_Evolution_h
#define Monsters_Evolution_h

#include <functional>
#include <random>

#include "MonsterBase.h"
#include "FightCoordinator.h"

class Evolution
{
public:
  Evolution( std::function<std::string(void)> nameGen, int seed = -1 );

  std::vector< std::shared_ptr<MonsterBase> > evolve( const std::vector< std::shared_ptr<MonsterBase> >& initPopulation,
                                                      unsigned int nbTurns );

  std::shared_ptr<MonsterBase> mate( const std::shared_ptr<MonsterBase>& mon1, const std::shared_ptr<MonsterBase>& mon2 );

private:
  std::vector< std::shared_ptr<MonsterBase> > _population;
  FightCoordinator _fightCoordinator;

  std::function<std::string(void)> _nameGen;

  std::mt19937 _twister;
  std::uniform_int_distribution<std::size_t> _distr;
  std::normal_distribution<double> _distrNoise{ 0.0, 0.1 };
};

#endif
