#include "NameGenerator.h"

#include <chrono>
#include <iostream>
#include <locale>
#include <map>
#include <sstream>

// ----------------------------------------------------------------------------

NameGenerator::NameGenerator( int seed )
{
  if( seed < 0 )
    _twister.seed( std::time(0) );
  else
    _twister.seed( seed );
}

// ----------------------------------------------------------------------------

NameGenerator::NameGenerator( const std::vector<std::string>& initWords,
                              std::string delim,
                              int seed )
  : NameGenerator( seed )
{


  std::vector<std::string> tokens;
  for( const auto& str : initWords )
  {
    auto posStart = 0;
    auto posFind = str.find( delim );
    while( posFind < str.size() )
    {
      auto substr = str.substr( posStart, posFind - posStart );
      tokens.push_back( substr );
      posStart = posFind + 1;
      posFind = str.find( delim, posStart );
    }
    tokens.push_back( str.substr( posStart, str.size() - posStart ));
  }

  // count letter appearances
  std::map< std::string, unsigned int > occVowels;
  std::map< std::string, unsigned int > occConsonants;
  std::map< unsigned int, unsigned int > occWordLenghts;
  std::map< std::string, unsigned int > occCombis;

  std::string VOWELS( "aeiouy" );

  std::size_t counterOverall     = 0;
  std::size_t counterVowels      = 0;
  std::size_t counterConsonants  = 0;
  for( const auto& t : tokens )
  {
    std::cout << "t: " << t << "|\n";
    occWordLenghts[t.size()]++;

    // count character occurrences
    for( const auto& c : t )
    {
      auto cLower = std::tolower(c);
      counterOverall++;
      if( VOWELS.find(cLower) != std::string::npos )
      {
        occVowels[ std::string( 1, cLower ) ]++;
        counterVowels++;
      }
      else
      {
        occConsonants[ std::string( 1, cLower ) ]++;
        counterConsonants++;
      }
    }
  }

  std::vector<double> wordLengthsProb;
  wordLengthsProb.reserve( occWordLenghts.size() );
  _wordLengths.reserve( occWordLenghts.size() );
  for( const auto& oc : occWordLenghts )
  {
    std::cout << oc.first << ": " << oc.second << "\n";
    _wordLengths.push_back( oc.first );
    auto prob = oc.second / static_cast<double>( tokens.size() );
    wordLengthsProb.push_back( prob );
    std::cout << "  -> " << prob << "\n";
  }
  std::cout << "\n";
  _distrLengths = std::discrete_distribution<int>( wordLengthsProb.begin(), wordLengthsProb.end() );

  std::vector<double> vowelsProbs;
  vowelsProbs.reserve( occVowels.size() );
  _vowels.reserve( occVowels.size() );
  for( const auto& oc : occVowels )
  {
    std::cout << oc.first << ": " << oc.second << "\n";
    _vowels.push_back( oc.first );
    vowelsProbs.push_back( oc.second / static_cast<double>(counterVowels) );
    std::cout << "  -> " << vowelsProbs.back() << "\n";
  }
  std::cout << "\n";
  _distrVowels = std::discrete_distribution<int>( vowelsProbs.begin(), vowelsProbs.end() );

  std::vector<double> consonantsProbs;
  consonantsProbs.reserve( occConsonants.size() );
  _consonants.reserve( occConsonants.size() );
  for( const auto& oc : occConsonants )
  {
    std::cout << oc.first << ": " << oc.second << "\n";
    _consonants.push_back( oc.first );
    consonantsProbs.push_back( oc.second / static_cast<double>(counterConsonants) );
    std::cout << "  -> " << consonantsProbs.back() << "\n";
  }
  _distrConsonants = std::discrete_distribution<int>( consonantsProbs.begin(), consonantsProbs.end() );

}

// ----------------------------------------------------------------------------

std::string NameGenerator::generateName()
{
  std::string out;

  std::uniform_int_distribution<int> distr;
  unsigned int NB_CHARACTERS = _wordLengths[_distrLengths(_twister)];

  std::cout << "nb chars: " << NB_CHARACTERS << "\n";

  auto randFirst = distr(_twister);

  if( randFirst % 2 == 0 ) // start with vowel
  {
    auto currRand = _distrVowels(_twister);
    out += std::toupper( _vowels.at( currRand % _vowels.size() )[0] );
    _lastType = LastType::Vowel;
  }
  else // start with consonant
  {
    auto currRand = _distrConsonants(_twister);
    out += std::toupper( _consonants.at( currRand % _consonants.size() )[0] );
    _lastType = LastType::Consonant;
  }

  for( int i = 1; i < NB_CHARACTERS; ++i )
  {
    switch( _lastType )
    {
    case LastType::Consonant:
    {
      auto currRand = _distrVowels(_twister);
      out += _vowels.at( currRand % _vowels.size() );
      _lastType = LastType::Vowel;
      break;
    }
    case LastType::Vowel:
    {
      auto currRand = _distrConsonants(_twister);
      out += _consonants.at( currRand % _consonants.size() );
      _lastType = LastType::Consonant;
      break;
    }
    }
  }

  return out;
}

// ----------------------------------------------------------------------------

std::string NameGenerator::createSimpleName()
{


  std::discrete_distribution<int> distNameLength{ 0.0, 0.0, 0.01, 0.3, 0.3, 0.3, 0.3, 0.02, 0.01, 0.01, 0.01, 0.01, 0.01 };
  unsigned int NB_CHARACTERS = distNameLength(_twister);

  std::uniform_int_distribution<int> dist;

  //  "bcdfghjklmnpqrstvwxz" );
  std::discrete_distribution<int> distConsonants{ 1, 1, 1, 1, 1, 1.5, 0.5, 0.8, 1, 1, 1, 0.8, 0.2, 1, 1, 1, 0.4, 0.4, 0.1, 0.1 };

  // static const std::string vowels( "aeiouy" );
  std::discrete_distribution<int> distVowels{ 1,1,1,1,1,0.3 };

  std::string out;

  auto randFirst = dist(_twister);

  if( randFirst % 2 == 0 ) // start with vowel
  {
    auto currRand = distVowels(_twister);
    out += std::toupper( vowels.at( currRand % vowels.size() ) );
    _lastType = LastType::Vowel;
  }
  else // start with consonant
  {
    auto currRand = distConsonants(_twister);
    out += std::toupper( consonants.at( currRand % consonants.size() ) );
    _lastType = LastType::Consonant;
  }

  for( int i = 1; i < NB_CHARACTERS; ++i )
  {

    switch( _lastType )
    {
    case LastType::Consonant:
    {
      auto currRand = distVowels(_twister);
      out += vowels.at( currRand % vowels.size() );
      _lastType = LastType::Vowel;
      break;
    }
    case LastType::Vowel:
    {
      auto currRand = distConsonants(_twister);
      out += consonants.at( currRand % consonants.size() );
      _lastType = LastType::Consonant;
      break;
    }
    }
  }


  return out;
}

// ----------------------------------------------------------------------------
