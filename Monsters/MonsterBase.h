#ifndef Monsters_MonsterBase_h
#define Monsters_MonsterBase_h

#include <map>
#include <ostream>
#include <random>
#include <string>

#include "DNA.hh"
//#include "GlobalVars.h"
#include "MonsterAttack.h"


namespace MonsterDetail
{
static unsigned int NEXT_ID = 0;

static const unsigned int ID_STRENGTH = NEXT_ID++;
static const unsigned int ID_AGILITY = NEXT_ID++;

static const unsigned int ID_MAXHEALTHPTS = NEXT_ID++;

static const unsigned int ID_RESISTANCE_GENERAL = NEXT_ID++;

static const unsigned int ID_RESISTANCE_FIRE = NEXT_ID++;
static const unsigned int ID_RESISTANCE_WATER = NEXT_ID++;

std::map<unsigned int, std::string> idNameMap();

//static int COUNTER_INSTANCES = 0;

}

// -------------------------

namespace Monster
{
class Attack;
Attack createTackle();
}

// -------------------------

class MonsterBase
{
public:
//  MonsterBase( const std::string& name, double strength, double agility, double maxHealthPts );
  MonsterBase( const std::string& name,
               const std::vector<double>& attributes,
               const std::vector<Monster::Attack>& attacks = std::vector<Monster::Attack>{ Monster::createTackle() } );

  MonsterBase( const DNA& dna,
               const std::vector<Monster::Attack>& attacks = std::vector<Monster::Attack>{ Monster::createTackle() },
               const std::string& name = "N.N." );


  std::string getName() const { return _name; }

  /**
    * Strength: max strength, schnellkraft, kraftausdauer
    * - Maxstrength: musclemass, muscle vernetzung, muscle koordination
    * - schnellkraft: muscle vernetzung, muscle durchblutung/versorgung, -fatness, muscle koordination
    * - kraftausdauer: muscle durchblutung/versorgung, - musclemass, regeneration, energiereserve
    *
    * Regeneration: Metabolic rate at rest, durchblutung, umwandlungseffizienz
    * Intelligence: min( skull size, brain size ), neuronenvernetzung, neuronendichte, durchblutung, energiereserve,
    *               konzentrationsfähigkeit
    *
    * Geschicklichkeit: muscle koordination, neuronenvernetzung, neuronen dichte, konzentrationsfähigkeit
    *
    * Schnelligkeit: Schnellkraft, -body mass, windschnittigkeit (körperform), muscle koordination
    *
    * Ausdauer: kraftausdauer, energiereserve, umwandlungseffizienz, -mass
    *
    * Gewicht: body size/mass, skullsize, fatness, musclemass, knochenmasse/dichte
    *
    * Giftanfälligkeit: durchblutung,
    *
    * Energiereserve: fatness, umwandlungseffizienz
    *

   * Intelligence
   * Strength
   * Agility - Gewandtheit
   * Dexterity - Geschicklichkeit
   * Max speed
   * Acceleration
   * Regeneration
   * Vitality
   * Max health pts
   * Constitution?
   *
   * Weight -> Size * (Hardness of skin + muscularness) etc
   * metabolicRateAtRest
   * endurance
   * aktivitätszeit / tag vs nacht oder zeitpunkt
   *
   * Size
   * Muscularness
   * Fatness
   * Heavyness
   * Brain size
   * Leg length
   * Furryness
   * Hardness of Skin
   * Flexibility
   * eye color
   * seeing ability
   * hearing ability
   *
   * fire resistance
   * water resistance
   * earth/stone resistance
   * firyness - feuer bonus
   * wateryness - water bonus
   * stonyness - stone bonus
   */

  double getStrength() const;
  double getAgility() const;
  double getMaxHealthPts() const;

  double getCurrHealthPts() const { return _currHealthPts; }
  int getLevel() const { return _level; }

  std::vector<double> getAttributes() const { return _attributes; }

  Monster::Attack getAttack( int i ) const { return _attacks[i]; }
  std::vector<Monster::Attack> getAttacks() const { return _attacks; }

  void addAttack( const Monster::Attack& attack ){ _attacks.push_back(attack); }

  double roll() const;

  void applyDamage( double dmg );
  /**
   * @brief regenerate
   * @param nbTurns already plugged in for later use in combination with a regeneration attribute.
   */
  void regenerate( int nbTurns = -1 );


private:
  std::string _name;

  std::vector<double> _attributes; //= std::vector<double>( MonsterDetail::NEXT_ID, 0.0 );

  DNA _dna;

  std::vector<Monster::Attack> _attacks;

  double _currHealthPts;

  int _level = 1;

  std::mt19937 _twister;
};

std::ostream& operator<<( std::ostream& o, const MonsterBase& monster );

namespace DetailPrint
{

// Obacht!
std::size_t getNbLines();

std::string printLine( unsigned int i, const MonsterBase& monster );

std::string printMonsterSideBySide( const MonsterBase& monster1, const MonsterBase& monster2 );

}

#endif
