#include "MonsterAttack.h"

#include <cmath>

// ----------------------------------------------------------------------------

Monster::Attack::Attack( const std::string& name,
                double baseDamage, double strengthFactor,
                double agilityFactor, double luckFactor )
  : _name(name),
    _baseDamage(baseDamage)/*, _strengthFactor(strengthFactor),
    _agilityFactor(agilityFactor), _luckFactor(luckFactor)*/
{
  _factorsAttributes[AttackDetail::ID_STRENGTH] = strengthFactor;
  _factorsAttributes[AttackDetail::ID_AGILITY] = agilityFactor;
// Todo: Luck
}

// ----------------------------------------------------------------------------
// --- Tackle -----------------------------------------------------------------

Monster::Attack Monster::createTackle()
{
  return Attack( "Tackle", 1.0, 0.8, 0.2, 0.01 );
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// --- Damage

Monster::Attack::Damage::Damage( double neutral, double fire, double water )
  : _neutral(neutral), _fire(fire), _water(water)
{

}

// ----------------------------------------------------------------------------
