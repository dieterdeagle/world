#ifndef Monsters_DNA_hh
#define Monsters_DNA_hh

#include <random>
#include <vector>

class DNA
{
public:
  DNA();

  DNA( const std::vector<double>& gameteFather,
       const std::vector<double>& gameteMother );

  double getInfoFather( std::size_t pos ) const;
  double getInfoMother( std::size_t pos ) const;

  std::vector<double> getGameteFather() const { return _gameteFather; }
  std::vector<double> getGameteMother() const { return _gameteMother; }

  std::vector<double> produceRandomGamete() const;

private:
  std::vector<double> _gameteFather;
  std::vector<double> _gameteMother;

  mutable std::mt19937 _twister;
  mutable std::uniform_int_distribution<unsigned int> _distr{ 0, 1 };
};

#endif
