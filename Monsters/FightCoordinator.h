#ifndef Monsters_FightCoordinator_h
#define Monsters_FightCoordinator_h

#include <memory>
#include <random>

#include "DecisionMaker.h"
#include "MonsterAttack.h"
#include "MonsterBase.h"

class FightCoordinator
{
public:
  FightCoordinator( int seed = -1 );

  /**
   * @brief fight Obacht! Monster werden verändert!
   * @param monster1
   * @param monster2
   */
  void fight( const std::shared_ptr<MonsterBase>& monster1,
              const std::shared_ptr</*Monster::*/DecisionMaker>& decider1,
              const std::shared_ptr<MonsterBase>& monster2,
              const std::shared_ptr</*Monster::*/DecisionMaker>& decider2 );

  /**
   * @brief calcDamage calculates damage done by an attack
   * @param attack
   * @param aggressor
   * @param defender
   * @return
   */
  double calcDamage( const Monster::Attack& attack,
                     const MonsterBase& aggressor,
                     const MonsterBase& defender );

  std::mt19937 _twister;
  std::uniform_int_distribution<unsigned int> _distrStart{0,1};
};

#endif

