#ifndef Monsters_MonsterState_h
#define Monsters_MonsterState_h

#include <string>

namespace Monster
{

class StateBase
{
public:
  virtual std::string getName() const = 0;

  virtual void update( int nbTurnsPassed ) = 0;
};

// -------------------------------

namespace State
{

class Stunned : public StateBase
{
  Stunned( int nbTurns );

  virtual std::string getName() const { return "Stunned"; }

  virtual void update( int nbTurnsPassed );

private:
  int _nbRemainingTurns;
};

}

}

#endif
