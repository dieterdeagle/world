#include "Evolution.h"

#include <chrono>

// ----------------------------------------------------------------------------

Evolution::Evolution( std::function<std::string(void)> nameGen, int seed )
  : _nameGen(nameGen)
{
  if( seed < 0 )
    _twister.seed( std::time(0) );
  else
    _twister.seed( seed );
}

// ----------------------------------------------------------------------------

std::vector<std::shared_ptr<MonsterBase> > Evolution::evolve( const std::vector<std::shared_ptr<MonsterBase> >& initPopulation,
                                                              unsigned int nbTurns )
{
  _population = initPopulation;

  auto deciderRand = std::make_shared<AIRandom>();

  std::size_t NB_RESULTS = 10;

  std::size_t NB_MATINGS_PER_ROUND = 100;

  for( unsigned int i = 0; i < nbTurns; ++i )
  {
    auto NB_FIGHTS_PER_ROUND = _population.size() - NB_RESULTS;
    for( unsigned int j = 0; j < NB_FIGHTS_PER_ROUND; ++j )
    {
      // choose random fight
      auto id1 = _distr(_twister) % _population.size();
      auto id2 = _distr(_twister) % _population.size();
      while( id2 == id1 )
        id2 = _distr(_twister) % _population.size();

      _fightCoordinator.fight( _population[id1], deciderRand, _population[id2], deciderRand );
      if( _population[id1]->getCurrHealthPts() < 0.0 )
      {
        _population[id2]->regenerate();
        _population.erase( _population.begin() + id1 );
      }
      else
      {
        _population[id1]->regenerate();
        _population.erase( _population.begin() + id2 );
      }
    }

    std::vector< std::shared_ptr<MonsterBase> > offspring;
    for( std::size_t j = 0; j < NB_MATINGS_PER_ROUND; ++j )
    {
      auto id1 = _distr(_twister) % _population.size();
      auto id2 = _distr(_twister) % _population.size();
      offspring.push_back( this->mate( _population[id1], _population[id2] ) );
    }
    if( i < nbTurns - 1 )
      _population = offspring;
  }

  return _population;
}

// ----------------------------------------------------------------------------

std::shared_ptr<MonsterBase> Evolution::mate( const std::shared_ptr<MonsterBase>& mon1,
                                              const std::shared_ptr<MonsterBase>& mon2 )
{
  auto attribs1 = mon1->getAttributes();
  auto attribs2 = mon2->getAttributes();

  std::vector<double> attribsNew;
  attribsNew.reserve( attribs1.size() );
  for( std::size_t i = 0; i < attribs1.size(); ++i )
  {
    double currAttrib = attribs2[i];
    auto rand = _distr(_twister);
    if( rand % 2 == 0 )
      currAttrib = attribs1[i];
    currAttrib += _distrNoise(_twister);
    attribsNew.push_back( currAttrib );
  }

  auto monNew = std::make_shared<MonsterBase>( _nameGen(), attribsNew, mon1->getAttacks() );

  return monNew;


  // Todo: Attacks


}

// ----------------------------------------------------------------------------
