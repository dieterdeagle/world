#include "DecisionMaker.h"

#include <chrono>

// ----------------------------------------------

/*Monster::*/AIRandom::AIRandom( int seed )
{
  if( seed < 0 )
    _twister.seed( std::time(0) );
  else
  {
    _twister.seed( seed );
  }

  _rand = std::bind( _distr, _twister );
}

// ----------------------------------------------

int /*Monster::*/AIRandom::chooseAction( const MonsterBase& monsterOwn, const MonsterBase& monsterOpponent ) const
{
  return _rand() % monsterOwn.getAttacks().size();
}
