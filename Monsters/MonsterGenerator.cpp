#include "MonsterGenerator.h"


// ----------------------------------------------------------------------------

template <>
MonsterGenerator< std::normal_distribution<double> >::MonsterGenerator( double baseValue, double width,
                                                                        std::function<std::string()> nameGenerator,
                                                                        int seed )
  : _baseValue(baseValue), _nameGenerator(nameGenerator)
{
  _distr = std::normal_distribution<double>( baseValue, width/3.0 );
  this->init( seed );
}

// ----------------------------------------------------------------------------

template <>
MonsterGenerator< std::uniform_real_distribution<double> >::MonsterGenerator( double baseValue, double width,
                                                                              std::function<std::string()> nameGenerator,
                                                                              int seed )
  : _baseValue(baseValue), _nameGenerator(nameGenerator)
{
  _distr = std::uniform_real_distribution<double>( baseValue-width, baseValue+width );
  this->init( seed );
}

// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------
