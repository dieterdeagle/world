#include "MonsterBase.h"

#include <chrono>
#include <iomanip>
#include <iostream>
#include <sstream>

// ----------------------------------------------------------------------------

MonsterBase::MonsterBase( const std::string& name,
                          const std::vector<double>& attributes,
                          const std::vector<Monster::Attack>& attacks )
  : _name(name), _attributes(attributes)
{

  _attacks.insert( _attacks.end(), attacks.begin(), attacks.end() );

  // Using name as hashSeed
  auto hashSeed = std::hash<std::string>{}(name);
  _twister.seed( hashSeed );

  _currHealthPts = _attributes[MonsterDetail::ID_MAXHEALTHPTS];

}

// ----------------------------------------------------------------------------

MonsterBase::MonsterBase( const DNA& dna,
                          const std::vector<Monster::Attack>& attacks,
                          const std::string& name )
  : _dna(dna), _attacks(attacks), _name(name)
{

}

// ----------------------------------------------------------------------------

double MonsterBase::getStrength() const
{
  return _attributes[MonsterDetail::ID_STRENGTH];
}

// ----------------------------------------------------------------------------

double MonsterBase::getAgility() const
{
  return _attributes[MonsterDetail::ID_AGILITY];
}

// ----------------------------------------------------------------------------

double MonsterBase::getMaxHealthPts() const
{
  return _attributes[MonsterDetail::ID_MAXHEALTHPTS];
}

// ----------------------------------------------------------------------------

double MonsterBase::roll() const
{
  return 0.0;
}

// ----------------------------------------------------------------------------

void MonsterBase::applyDamage( double dmg )
{
  _currHealthPts -= dmg;
}

// ----------------------------------------------------------------------------

void MonsterBase::regenerate( int nbTurns )
{
  if( nbTurns < 0 )
  {
    // regen full
    _currHealthPts = this->getMaxHealthPts();
  }
  else
  {
    // Todo: something like: regenFactor()*nbTurns
  }
}

// ----------------------------------------------------------------------------

std::ostream& operator<<( std::ostream& o,
                          const MonsterBase& monster )
{
  for( unsigned int i = 0; i < DetailPrint::getNbLines(); ++i )
  {
    o << DetailPrint::printLine(i, monster) << "\n";
  }

  return o;

//  o <<  monster.getName() << " (Lvl: " << monster.getLevel() << ")"
//    << "\n | " << "Strength: " << monster.getStrength()
//    << "\n | " << "Agilitiy: " << monster.getAgility()
//    << "\n | " << "Health:   " << monster.getCurrHealthPts() << "/" << monster.getMaxHealthPts()
//    << "\n";
//  return o;
}

// ----------------------------------------------------------------------------

std::string DetailPrint::printLine( unsigned int i, const MonsterBase& monster )
{
//  std::cout << "CALLING PRINT LINE FOR: " << monster.getName() << "\n";
  switch(i)
  {
  case 0:
  {
    return std::string( monster.getName() + " (Lvl: " + std::to_string(monster.getLevel()) + ")" );
    break;
  }
  case 1:
  {
    return std::string( "Health: " + std::to_string(monster.getCurrHealthPts()) + "/" + std::to_string(monster.getMaxHealthPts() ) );
    break;
  }
  default:
  {
    auto idNameMap = MonsterDetail::idNameMap();
    if( i - 2 < idNameMap.size() )
    {
      auto attribs = monster.getAttributes();
      auto iter = idNameMap.begin();
      for( std::size_t j = 0; j < i-2; ++j )
        ++iter;
      return std::string( iter->second + ": " + std::to_string( attribs[iter->first] ) );
    }
    else
      return "";
    break;
  }
  }

  return "";
}

// ----------------------------------------------------------------------------

std::size_t DetailPrint::getNbLines()
{
  return 5;
}

// ----------------------------------------------------------------------------

std::string DetailPrint::printMonsterSideBySide( const MonsterBase& monster1, const MonsterBase& monster2 )
{

  std::string result;

  std::size_t w = 50;

  for( std::size_t i = 0; i < getNbLines(); ++i )
  {
    {
      std::stringstream format1;
      format1 << std::setw(w) << std::left << printLine( i , monster1 );
//      format1  << "";

      result += format1.str();
    }

//    result += "             ";

    {
      std::stringstream format2;
      format2 << std::setw( w ) << std::left;
      format2 << printLine( i, monster2 );

      result += format2.str();
      result += "\n";
    }
  }

  return result;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

std::map<unsigned int, std::string> MonsterDetail::idNameMap()
{
  std::map<unsigned int, std::string> map;
  map.insert( std::make_pair(ID_STRENGTH, "Strength") );
  map.insert( std::make_pair(ID_AGILITY, "Agility") );
  map.insert( std::make_pair(ID_MAXHEALTHPTS, "Max Health Pts") );

  // Todo: insert all attributes

  return map;
}
