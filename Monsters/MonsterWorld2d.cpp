#include "MonsterWorld2d.h"

#include <algorithm>
#include <iostream>

// ----------------------------------------------------------------------------

MonsterWorld::MonsterWorld( unsigned int width,
                            unsigned int height,
                            const std::vector<MonsterWorld::TileType>& tiles )
  : _width(width), _height(height), _tiles(tiles)
{

}

// ----------------------------------------------------------------------------

MonsterWorld::TileType MonsterWorld::getTileType( unsigned int x, unsigned int y ) const
{
  return _tiles[ x + y*_width ];
}

// ----------------------------------------------------------------------------

void MonsterWorld::addGameObject( const std::shared_ptr<GameObject>& obj )
{
  _gameObjects.push_back( obj );
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// --- Generator --------------------------------------------------------------
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

MonsterWorldUtility::Generator::Generator( const std::vector<std::pair<MonsterWorld::TileType, double> >& seedingProbabilities,
                                           unsigned int nbSeeds,
                                           unsigned int nbTowns )
  : _nbSeeds(nbSeeds), _nbTowns(nbTowns)
{

  _tileCandidates.reserve( seedingProbabilities.size() );
  std::vector<double> probs;
  probs.reserve( seedingProbabilities.size() );
  for( auto it : seedingProbabilities )
  {
    _tileCandidates.push_back( it.first );
    probs.push_back( it.second );
  }

  _distrSeeding = std::discrete_distribution<int>( probs.begin(), probs.end() );

}

// ----------------------------------------------------------------------------

std::shared_ptr<MonsterWorld> MonsterWorldUtility::Generator::generate( unsigned int w, unsigned int h,
                                                                              int seed )
{
  using Type = MonsterWorld::TileType;

  std::vector<MonsterWorld::TileType> result( w*h, MonsterWorld::TileType::EMPTY );

  if( seed < 0 )
  {
    _twister.seed( std::time(0) );
  }
  else
  {
    _twister.seed( seed );
  }

  std::vector<unsigned int> idsColored;
  idsColored.reserve( w*h );

  // add tile seeds to the map
  std::uniform_int_distribution<int> distrUniformPositions( 0, result.size()-1 );
  for( unsigned int i = 0; i < _nbSeeds; ++i )
  {
    // draw random pos id
    auto posId = distrUniformPositions(_twister);
    while( result[posId] != MonsterWorld::TileType::EMPTY )
    {
      posId = distrUniformPositions(_twister);
    }
    idsColored.push_back( posId );

    // draw seed tile type
    auto tile = _tileCandidates[ _distrSeeding(_twister) ];
    result[posId] = tile;
  }

  // fill tiles around seeds
  std::uniform_int_distribution<unsigned int> distrUniformIds;
  while( !idsColored.empty() )
  {
//    std::cout << "size: " << idsColored.size() << "\n";
    // pick random
    auto idInIdsColored = distrUniformIds(_twister) % idsColored.size();
    auto idRand = idsColored[ idInIdsColored ];

    auto neighbours = Detail::getNeighbouringIds( idRand, w, h );

    std::shuffle( neighbours.begin(), neighbours.end(), _twister );
    auto findIter = std::find_if( neighbours.begin(), neighbours.end(), [&result](unsigned int id){ return result[id] == Type::EMPTY; } );


    // check if empty neighbours
    //   if: color neighbour, add neighbour to idsColored
    //   else: remove from idsColored
    if( findIter != neighbours.end() )
    {
//      std::cout << " find: " << *findIter << " -> " << "\n";
      result[*findIter] = result[idRand];
      idsColored.push_back(*findIter);
    }
    else
    {
      idsColored.erase( idsColored.begin() + idInIdsColored );
    }
  }

  auto world = std::make_shared<MonsterWorld>( w, h, result );


  return world;
}

// ----------------------------------------------------------------------------

void MonsterWorldUtility::Generator::addTowns( const std::shared_ptr<MonsterWorld>& world,
                                               unsigned int nbTowns )
{

  for( unsigned int i = 0; i < nbTowns; ++i )
  {
    // Todo: Rules where to place towns.
  }
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// --- Detail -----------------------------------------------------------------
// ----------------------------------------------------------------------------

std::vector<unsigned int> MonsterWorldUtility::Detail::getNeighbouringIds( unsigned int id, unsigned int w, unsigned int h )
{
  // id = x + w*y
  // -> x = id - w*y
  // -> y = (id-x) / w
  auto x = id % w;
  auto y = (id-x) / w;

  std::vector<unsigned int> neighs;
  neighs.reserve(4);

  // left
  if( static_cast<int>(x)-1 >= 0 )
  {
    neighs.push_back( x-1 + w*y );
  }
  if( x + 1 < w )
  {
    neighs.push_back( x+1 + w*y );
  }
  if( static_cast<int>(y)-1 >= 0 )
  {
    neighs.push_back( x + w*(y-1) );
  }
  if( y + 1 < h )
  {
    neighs.push_back( x + w*(y+1) );
  }

//  std::cout << "| neighs for: " << id << "\n|";
//  for( auto n : neighs )
//  {
//    std::cout << n << ", ";
//  }
//  std::cout << "\n";

  return neighs;
}
