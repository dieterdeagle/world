#include "FightCoordinator.h"

#include <chrono>
#include <iostream>

// ----------------------------------------------------------------------------

FightCoordinator::FightCoordinator( int seed )
{
  if( seed < 0 )
    _twister.seed( std::time(0) );
  else
    _twister.seed( seed );
}

// ----------------------------------------------------------------------------

void FightCoordinator::fight( const std::shared_ptr<MonsterBase>& monster1,
                              const std::shared_ptr</*Monster::*/DecisionMaker>& decider1,
                              const std::shared_ptr<MonsterBase>& monster2,
                              const std::shared_ptr</*Monster::*/DecisionMaker>& decider2  )
{
  // Fixme: MAX_ROUNDS as para?
  unsigned int MAX_ROUNDS = 100;
  unsigned int counterRounds = 0;

  // Random starter or is the higher lvl allowed to start?
  auto starterId = _distrStart(_twister);


  bool PRINT = false;

  if( PRINT )
    std::cout << "Fight!\n" << DetailPrint::printMonsterSideBySide( *monster1, *monster2 );

  while( monster1->getCurrHealthPts() > 0
         && monster2->getCurrHealthPts() > 0
         && counterRounds <= MAX_ROUNDS )
  {
    counterRounds++;
    if( PRINT )
    {
      std::cout << "------------------\n";
      std::cout << "Starting round " << counterRounds << "\n";
    }

    std::shared_ptr<MonsterBase> attackerMonster = monster2;
    std::shared_ptr<DecisionMaker> attackerDecider = decider2;
    std::shared_ptr<MonsterBase> defenderMonster = monster1;
    std::shared_ptr<DecisionMaker> defenderDecider = decider1;

//    std::cout << "starter id: " << starterId << "\n";
    if( counterRounds % 2 == starterId )
    {
      if( PRINT )
        std::cout << "#### HEY!\n";
      attackerMonster = monster1;
      attackerDecider = decider1;
      defenderMonster = monster2;
      defenderDecider = decider2;
    }

    auto action = attackerDecider->chooseAction( *attackerMonster, *defenderMonster );
    if( PRINT )
      std::cout << " action nb: " << action << "\n";

    if( action >= 0 )
    {
      auto dmg = this->calcDamage( attackerMonster->getAttack(action), *attackerMonster, *defenderMonster );
      if( PRINT )
        std::cout << attackerMonster->getName() << " uses " << attackerMonster->getAttack(action).getName() << " and causes " << dmg << " damage.\n";
      defenderMonster->applyDamage( dmg );
    }
    else
    {
      // do nothing
      if( PRINT )
        std::cout << attackerMonster->getName() << " waits..\n";
    }


    if( PRINT )
    {
      std::cout << "\nEnd of round:\n";
      std::cout << DetailPrint::printMonsterSideBySide( *monster1, *monster2 );
      std::cout << "\n";
    }


  }

}

// ----------------------------------------------------------------------------

double FightCoordinator::calcDamage( const Monster::Attack& attack,
                                     const MonsterBase& aggressor,
                                     const MonsterBase& defender )
{
  auto dmg = attack.getBaseDamage() * std::pow( aggressor.getLevel(), 1/3.0 )
      + attack.getFactorStrength() * aggressor.getStrength() / 10.0
      + attack.getFactorAgility() * aggressor.getAgility() / 10.0;

  // Todo: take defender resistances into account

  return dmg;
//  Monster::Attack::Damage( dmg * attack.getFactorNeutral(),
//          dmg * attack.getFactorFire(),
//          dmg * attack.getFactorWater() );

  return dmg;
}

// ----------------------------------------------------------------------------
