#ifndef Monsters_NameGenerator_h
#define Monsters_NameGenerator_h

#include <random>
#include <string>
#include <vector>

static const std::string consonants( "bcdfghjklmnpqrstvwxz" );
static const std::string vowels( "aeiouy" );
static const std::string sonder( "-'" );

class NameGenerator
{
public:
  NameGenerator( int seed = -1 );

  NameGenerator( const std::vector<std::string>& initWords,
                 std::string delim = " ",
                 int seed = -1 );

  std::string generateName();

  std::string createSimpleName();

  enum class LastType { Vowel, Consonant, Sonder };
  LastType _lastType;

private:
  std::mt19937 _twister;

  std::vector<unsigned int> _wordLengths;
  std::discrete_distribution<int> _distrLengths;

  std::vector<std::string> _vowels;
  std::discrete_distribution<int> _distrVowels;

  std::vector<std::string> _consonants;
  std::discrete_distribution<int> _distrConsonants;
};

#endif
