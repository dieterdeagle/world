#ifndef Monsters_MonsterWorld2d_h
#define Monsters_MonsterWorld2d_h

#include <memory>
#include <utility>
#include <vector>

#include "GameObjects/GameObject.h"

class MonsterWorld
{
public:
  enum class TileType { Grass, Forest, Fire, Water, Stone, EMPTY };

  MonsterWorld( unsigned int width,
                unsigned int height,
                const std::vector<TileType>& tiles );

  TileType getTileType( unsigned int x, unsigned int y ) const;

  unsigned int getWidth() const { return _width; }
  unsigned int getHeight() const { return _height; }

  void addGameObject( const std::shared_ptr<GameObject>& obj );

private:
  unsigned int _width;
  unsigned int _height;
  std::vector<TileType> _tiles;

  std::vector< std::shared_ptr<GameObject> > _gameObjects;
};

// ------------------------------------

#include <chrono>
#include <random>

namespace MonsterWorldUtility
{
  class Generator
  {
  public:
    Generator( const std::vector< std::pair<MonsterWorld::TileType,double> >& seedingProbabilities,
               unsigned int nbSeeds,
               unsigned int nbTowns = 0 );

    std::shared_ptr<MonsterWorld> generate( unsigned int w, unsigned int h, int seed = -1 );

  private:
    void addTowns( const std::shared_ptr<MonsterWorld>& world, unsigned int nbTowns );

  private:
    std::mt19937 _twister;
    std::discrete_distribution<int> _distrSeeding;

    std::vector<MonsterWorld::TileType> _tileCandidates;

    unsigned int _nbSeeds;
    unsigned int _nbTowns;

  };

  namespace Detail
  {
    std::vector<unsigned int> getNeighbouringIds( unsigned int id, unsigned int w, unsigned int h );
  }
}

#endif
