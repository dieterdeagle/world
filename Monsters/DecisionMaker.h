#ifndef Monster_DecisionMaker_h
#define Monster_DecisionMaker_h

#include <functional>
#include <memory>
#include <random>

#include "MonsterBase.h"

//namespace Monster
//{

class DecisionMaker
{
public:
  virtual int chooseAction( const MonsterBase& monsterOwn,
                            const MonsterBase& monsterOpponent ) const = 0;
};

// --------------------------------

class AIRandom : public DecisionMaker
{
public:
  AIRandom( int seed = -1 );

  virtual int chooseAction( const MonsterBase& monsterOwn,
                            const MonsterBase& monsterOpponent ) const;

private:
  std::mt19937 _twister;
  std::uniform_int_distribution<int> _distr;

  std::function<int(void)> _rand;
};

//} // namespace Monster end

#endif
