#include "Town.h"

// ----------------------------------------------------------------------------

Town::Town(const std::string& name, const Vec2& pos, double w, double h  )
  : _name(name), _pos(pos)
{
  _bBox = BoundingBox<Vec2>( Vec2(_pos.x() - w/2.0, _pos.y() - h/2.0),
                             Vec2(_pos.x() + w/2.0, _pos.y() + h/2.0) );
}

// ----------------------------------------------------------------------------

void Town::doStuff()
{
  // Todo
}

// ----------------------------------------------------------------------------
