#ifndef Monsters_GameObjects_GameObject_h
#define Monsters_GameObjects_GameObject_h

#include "geometry/BoundingBox.h"

#include "util/Vec2.h"

/**
 * @brief The GameObject class
 *   GameObjects: Player, Towns, Schatzkisten
 */
class GameObject
{
public:

  /**
   * @brief doStuff implement actions here.
   */
  virtual void doStuff(/*time*/) = 0;

  virtual void updateBoundingbox() = 0;

  BoundingBox<Vec2> getBoundingBox() const { return _bBox; }

protected:
  BoundingBox<Vec2> _bBox{ Vec2(0,0),Vec2(0,0) };
};

#endif
