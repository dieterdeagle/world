#ifndef Monsters_Town_h
#define Monsters_Town_h

#include "util/Vec2.h"

#include "GameObject.h"

class Town : public GameObject
{
public:
  Town( const std::string& name,
        const Vec2& pos, double w, double h );

  virtual void doStuff();
  virtual void updateBoundingbox() const;

private:
  std::string _name;

  Vec2 _pos;
};

#endif
