#include "DNA.hh"

#include <chrono>

// ----------------------------------------------------------------------------

DNA::DNA()
{
  _gameteMother = std::vector<double>{ 0.0, 0.0, 0.0 };
  _gameteFather = std::vector<double>{ 0.0, 0.0, 0.0 };
}

// ----------------------------------------------------------------------------

DNA::DNA( const std::vector<double>& gameteFather,
          const std::vector<double>& gameteMother )
  :  _gameteFather(gameteFather), _gameteMother(gameteMother)
{
  _twister.seed( std::time(0) );
}

// ----------------------------------------------------------------------------

double DNA::getInfoFather( std::size_t pos ) const
{
  return _gameteFather[ pos % _gameteFather.size() ];
}

// ----------------------------------------------------------------------------

double DNA::getInfoMother( std::size_t pos ) const
{
  return _gameteMother[ pos % _gameteMother.size() ];
}

// ----------------------------------------------------------------------------

std::vector<double> DNA::produceRandomGamete() const
{
  std::vector<double> gamete;

  auto size = _gameteMother.size();

  if( _distr(_twister) == 1 )
    size = _gameteFather.size();
  gamete.reserve( size );

  for( std::size_t i = 0; i < size; ++i )
  {
    if( _distr(_twister) == 0 )
      gamete.push_back( _gameteMother[ i % _gameteMother.size() ] );
    else
      gamete.push_back( _gameteFather[ i % _gameteFather.size() ] );
  }

  return gamete;
}

// ----------------------------------------------------------------------------
